# knockout-front
### a React forum front-end

![https://img.shields.io/badge/javascript-yes-yellow](https://img.shields.io/badge/javascript-yes-yellow) ![https://img.shields.io/badge/tests-lol%20no-critical](https://img.shields.io/badge/tests-lol%20no-critical)

This is the forums software developed (most hurriedly) for the Knockout forums.  
It's written in React and (sadly) uses Redux for (some) state management
You really, really shouldn't use this as anything but a toy. It's incomplete, messy, and slow. At best it's a lesson in why "I'll write it quick first and then fix it" doesn't ever work.  
Aqui há dragões.

# License

See LICENSE file.

# Running this project

Requirements:

* Node v10.14.2 or later
* yarn

Instructions:

1. Clone the repo
1. Navigate to `/knockout-front`
1. Run `yarn`
1. Start your server
    * If you have a local API server, run `yarn start` to start up your development server
    * Otherwise, run `NODE_ENV=qa yarn start` to start up your development server with data from the QA API
1. The application should automatically open in your browser in localhost:8080
    * If the forums look like they are constantly loading, your API server is not returning data. Check
    the browser console for any errors; you may need to clear your browser cache. If you are stuck, feel
    free to reach out to other developers and we will gladly help out!

# Contributing to knockout-front

1. Make a branch based on `qa`
1. Commit and push your changes
1. Write tests
1. Run `yarn test` to ensure tests pass on your branch
1. Create a pull request
1. Ensure any CI checks pass on your branch
1. Notify the #knockout-software channel on [Discord](https://discord.gg/wjWpapC) so other developers can review your code!

## Styling Guidelines

- Components should be flat, with no rounded edges
- Components should not have borders
- Similar elements should be the same size for consistency

When designing, please consider:
- Light and Dark styles
- Mobile layout
- Accessibility
  - Is the component tab accessible?
  - Does the component and its content have enough contrast against the background?

## Code Structure Guidelines

### Components

When designing or adding to components, your work should be done in the `src/componentsNew` folder.

Each Component should live in an `index.jsx` file under a folder corresponding to the Component's name.
For example, the `Avatar` component lives under `src/componentsNew/Avatar/index.jsx`.

Components should be based around a singular React Component, with additional styling being done
within the same `index.jsx` folder using regular CSS classes under the main Component class.
For example, a Component named Foo which has a div inside that must float left should be structured as such:

```css
const Foo = styled.div`
  // base Foo styles...

  div.sub-component {
    float: left;
  }
```

Notice that we do not add a new `SubComponent` component here, we simply add a class.

### Views

When working with or adding a new View (page), your work should be done in the `src/views` folder.

Each View is sturctured similarly to a Component, in that each View has an `index.jsx` file under a folder
corresponding to the View's name.
For example, the `HomePage` view lives under `src/views/HomePage/index.jsx`.

Views also follow the same code and style structuring principles of Components.

### Themes

When utilizing Theme colors and sizing, please reference `src/utils/ThemeNew.js`.

### Tests

Each merge request should have unit tests corresponding to the fix, feature, or enhancement
that was made.

At a minimum, your code should have unit tests in [spec syntax](https://jasmine.github.io/2.0/introduction.html)
using `it` and `describe` blocks. This format makes tests human-readable and easier to understand what kind of testing
is being done.

Tests should strive to cover the main permutations that the system under test can be under.
For example, if a test was made for the Header component, the tests should cover cases where:

- The User is not logged in
- The User is logged in
- The User is logged in and banned
- The User is logged in and a moderator

Snapshot tests can be used for smaller components, but should be avoided for larger components as they are
more difficult to parse and to keep up to date.

# Feedback procedure

1. Comments, suggestions, additions and criticism* should be posted as comments on the merge request, or in the #knockout-software Discord channel.
1. Comments, suggestions, additions and criticism* are always welcome. thanks for your help!
