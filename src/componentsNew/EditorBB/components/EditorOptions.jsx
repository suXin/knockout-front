import React from 'react';
import PropTypes from 'prop-types';

const EditorOptions = ({
  allowExtraScripting,
  setAllowExtraScripting,
  sendCountryInfo,
  setSendCountryInfo,
  handleOptionsClose,
}) => (
  <div className="options">
    <button type="button" className="close-button" onClick={handleOptionsClose}>
      <i className="fas fa-times" />
    </button>
    <h3>Options</h3>

    <label htmlFor="toggle-display-country-info">
      Show country information in post: &nbsp;
      <i className={`options-icon far fa${sendCountryInfo ? '-check' : ''}-square`} />
      <input
        id="toggle-display-country-info"
        type="checkbox"
        onChange={() => setSendCountryInfo(!sendCountryInfo)}
        checked={sendCountryInfo}
      />
    </label>

    <label htmlFor="allow-extra-scripting">
      Allow for extra scripting (disable if you have issues on mobile): &nbsp;
      <i className={`options-icon far fa${allowExtraScripting ? '-check' : ''}-square`} />
      <input
        id="allow-extra-scripting"
        name="allow-extra-scripting"
        type="checkbox"
        onChange={() => setAllowExtraScripting(!allowExtraScripting)}
        checked={allowExtraScripting}
      />
    </label>
  </div>
);

EditorOptions.propTypes = {
  allowExtraScripting: PropTypes.bool.isRequired,
  setAllowExtraScripting: PropTypes.func.isRequired,
  sendCountryInfo: PropTypes.bool.isRequired,
  setSendCountryInfo: PropTypes.func.isRequired,
  handleOptionsClose: PropTypes.func.isRequired,
};

export default EditorOptions;
