import React from 'react';
import PropTypes from 'prop-types';

import Tooltip from '../../Tooltip';
import { insertTag } from '../helpers';

const ToolbarButton = ({ icon, tooltipText, iconFamily, handleButtonClick }) => (
  <button type="button" onClick={handleButtonClick} aria-label={tooltipText}>
    <Tooltip text={tooltipText}>
      <i className={`${iconFamily} fa-${icon}`} />
    </Tooltip>
  </button>
);

ToolbarButton.propTypes = {
  icon: PropTypes.string.isRequired,
  tooltipText: PropTypes.string.isRequired,
  handleButtonClick: PropTypes.func.isRequired,
  iconFamily: PropTypes.string,
};

ToolbarButton.defaultProps = {
  iconFamily: 'fas',
};

const EditorToolbar = ({ content, setContent, selectionRange, setSelectionRange, input }) => {
  const handleButtonClick = (type) => {
    insertTag(type, content, setContent, selectionRange, setSelectionRange, input);
  };

  return (
    <div className="toolbar">
      <>
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('b')}
          icon="bold"
          tooltipText="Bold"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('i')}
          icon="italic"
          tooltipText="Italic"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('u')}
          icon="underline"
          tooltipText="Underlined"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('s')}
          icon="strikethrough"
          tooltipText="Strikethrough"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('code')}
          icon="code"
          tooltipText="Code"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('spoiler')}
          icon="eye-slash"
          tooltipText="Spoiler"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('h1')}
          icon="heading"
          tooltipText="Very Large Text"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('h2')}
          icon="font"
          tooltipText="Large Text"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('blockquote')}
          icon="quote-right"
          tooltipText="Quote"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('li')}
          icon="list-ul"
          tooltipText="Unordered List"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('img')}
          icon="image"
          tooltipText="Image"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('youtube')}
          icon="youtube"
          tooltipText="Youtube"
          iconFamily="fab"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('twitter')}
          icon="twitter"
          tooltipText="Twitter"
          iconFamily="fab"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('strawpoll')}
          icon="poll"
          tooltipText="Straw Poll"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('video')}
          icon="video"
          tooltipText="Video (webm/mp4)"
        />
        <ToolbarButton
          handleButtonClick={() => handleButtonClick('noparse')}
          icon="terminal"
          tooltipText="Noparse (do not parse BBCode)"
        />
      </>
    </div>
  );
};

EditorToolbar.propTypes = {
  content: PropTypes.string.isRequired,
  setContent: PropTypes.func.isRequired,
  selectionRange: PropTypes.arrayOf(PropTypes.number).isRequired,
  setSelectionRange: PropTypes.func.isRequired,
  input: PropTypes.instanceOf(Element).isRequired,
};

export default EditorToolbar;
