import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import {
  ThemeBackgroundLighter,
  ThemeFontFamily,
  ThemeFontSizeLarge,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';

export const FieldLabel = styled.div`
  margin-bottom: 10px;
  font-size: ${ThemeFontSizeLarge};
  font-weight: bold;
`;

export const FieldLabelSmall = styled.div`
  margin-bottom: 10px;
  font-size: ${ThemeFontSizeSmall};
  opacity: 0.7;
  line-height: normal;

  a {
    color: #3facff;
  }
`;

export const TextField = styled.input`
  display: block;
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  color: ${ThemeTextColor};
  font-size: ${ThemeFontSizeMedium};
  font-family: ${ThemeFontFamily};
  line-height: 1.1;
  margin-bottom: 20px;
  border: none;
  width: 100%;
  box-sizing: border-box;
  background: ${ThemeBackgroundLighter};
  resize: none;
`;

export const TextFieldLarge = ({ value, placeholder, onChange, maxLength }) => {
  return (
    <StyledTextFieldLarge>
      <span className="char-count">{maxLength - value.length}</span>
      <TextField
        as="textarea"
        value={value}
        placeholder={placeholder}
        onChange={onChange}
        maxLength={maxLength}
      />
    </StyledTextFieldLarge>
  );
};

TextFieldLarge.propTypes = {
  value: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  maxLength: PropTypes.number.isRequired,
};

export const StyledTextFieldLarge = styled.div`
  position: relative;
  margin-bottom: 25px;

  ${TextField} {
    height: 100px;
  }

  .char-count {
    position: absolute;
    right: ${ThemeHorizontalPadding};
    bottom: ${ThemeVerticalPadding};
    font-size: ${ThemeFontSizeSmall};
    opacity: 0.5;
  }
`;
