import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tooltip from '../../Tooltip';
import ratingList from '../../../utils/ratingList.json';

const RatingItem = ({ rating, count, ratedByCurrentUser, postByCurrentUser, onClick }) => {
  const ratingObject = ratingList[rating];

  let tooltipText = `${ratingObject.name} - `;
  let suffix = 'Click to rate';
  if (ratedByCurrentUser) {
    suffix = 'Rated by you';
  } else if (postByCurrentUser) {
    suffix = 'Cannot rate yourself';
  }
  tooltipText += suffix;

  return (
    <StyledRatingItem
      ratingDisabled={ratedByCurrentUser || postByCurrentUser || onClick === undefined}
      ratedByCurrentUser={ratedByCurrentUser}
      className="rating-bar-item rating-item"
    >
      <div className="rating-icon">
        <Tooltip text={tooltipText} top right>
          <button onClick={onClick} type="button" title={tooltipText}>
            <img src={ratingObject.url} alt={ratingObject.name} />
            <span className="count">{`${count}`}</span>
          </button>
        </Tooltip>
      </div>
    </StyledRatingItem>
  );
};

RatingItem.propTypes = {
  rating: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
  ratedByCurrentUser: PropTypes.bool.isRequired,
  postByCurrentUser: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
};

const StyledRatingItem = styled.div`
  ${(props) => props.ratedByCurrentUser && 'filter: drop-shadow(0px 0px 3px #ffcc00);'}
  ${(props) => props.ratingDisabled && 'button { pointer-events: none; }'}
`;
export default RatingItem;
