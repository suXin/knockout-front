import dayjs from 'dayjs';
import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import relativeTime from 'dayjs/plugin/relativeTime';
import styled from 'styled-components';
import { getIcon } from '../../services/icons';
import ratingList from '../../utils/ratingList.json';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeHighlightWeaker,
  ThemeFontSizeLarge,
} from '../../utils/ThemeNew';
import UserRoleWrapper from '../UserRoleWrapper';
import Pagination from '../Pagination';
import UserAvatar from '../Avatar';
import MarkUnreadButton from '../MarkUnreadButton';
import { POSTS_PER_PAGE, unreadPostPage } from '../../utils/postsPerPage';
import minimalDateFormat from '../../utils/minimalDateFormat';
import { loadHideRatingsFromStorageBoolean } from '../../services/theme';
import { MobileMediaQuery } from '../SharedStyles';

dayjs.extend(relativeTime);

const ThreadItem = ({
  id,
  createdAt,
  deleted,
  iconId,
  lastPost,
  locked,
  pinned,
  postCount,
  title,
  unreadPostCount,
  user,
  showTopRating,
  firstPostTopRating,
  firstUnreadId,
  tags,
  backgroundUrl,
  backgroundType,
  threadIsRead,
  threadOpacity,
  markUnreadAction,
  markUnreadText,
  viewers,
  minimal,
}) => {
  const lastPage = Math.ceil(postCount / POSTS_PER_PAGE);

  const icon = getIcon(iconId);
  const threadTag = tags && tags.length ? Object.values(tags[0])[0] : null;
  const topRating = firstPostTopRating && ratingList[firstPostTopRating.rating];
  const unreadPostId = firstUnreadId ? `#post-${firstUnreadId}` : '';
  const unreadPostPageNum = unreadPostPage(unreadPostCount, postCount);
  const lastPostAvatar =
    lastPost && lastPost.user && lastPost.user.avatarUrl ? lastPost.user.avatarUrl : '';

  const formatDate = (date) => {
    if (!minimal) {
      if (date === 'a few seconds ago') {
        return 'just now';
      }
      return date.replace('ago', 'old');
    }

    return minimalDateFormat(date);
  };
  const lastPostDate = lastPost && dayjs(lastPost.createdAt).fromNow();
  const threadDate = formatDate(dayjs(createdAt).fromNow(minimal));

  const threadStatuses = [locked && 'Locked', pinned && 'Pinned', deleted && 'Deleted']
    .filter((status) => !!status)
    .concat('Thread')
    .join(' | ');

  const hasAvatar =
    lastPost &&
    lastPost.user &&
    lastPost.user.avatarUrl &&
    !lastPost.user.avatarUrl.includes('none.webp');

  return (
    <StyledThreadItem
      threadOpacity={threadOpacity}
      minimal={minimal}
      showTopRating={showTopRating && !loadHideRatingsFromStorageBoolean()}
      hasAvatar={hasAvatar}
    >
      <div className="image thread-icon">
        <Link to={`/thread/${id}`}>
          <div className="thread-icon-inner">
            <img src={icon.url} alt={icon.description} />
          </div>
        </Link>
      </div>
      <div className="content">
        <div className="first-row">
          <Link to={`/thread/${id}`} className="thread-title" title={threadStatuses}>
            {locked && (
              <>
                <i className="locked fas fa-lock" />
                &nbsp;
              </>
            )}
            {pinned && (
              <>
                <i className="pinned fas fa-sticky-note" />
                &nbsp;
              </>
            )}
            {deleted && (
              <>
                <i className="deleted fas fa-trash" />
                &nbsp;
              </>
            )}
            {title}
          </Link>
          {threadIsRead && unreadPostCount && unreadPostCount > 0 ? (
            <Link to={`/thread/${id}/${unreadPostPageNum}${unreadPostId}`} className="unread-posts">
              {`${unreadPostCount} new ${unreadPostCount === 1 ? 'post' : 'posts'}`}
            </Link>
          ) : null}
        </div>
        {!minimal && (
          <div className="second-row">
            {threadTag && (
              <>
                <span className="thread-tag">{threadTag}</span>
                <span> • </span>
              </>
            )}
            <span> by </span>
            <Link to={`/user/${user.id}`}>
              <UserRoleWrapper className="thread-user" user={user}>
                {user.username}
              </UserRoleWrapper>
            </Link>
            {postCount && postCount > POSTS_PER_PAGE ? (
              <>
                <span className="spacer-bead">•</span>
                <Pagination
                  className="thread-item-pagination"
                  pagePath={`/thread/${id}/`}
                  totalPosts={postCount}
                  small
                />
              </>
            ) : null}
            {viewers?.memberCount + viewers?.guestCount > 0 && (
              <>
                <span className="spacer-bead">•</span>
                <span className="viewer-count">
                  <i className="fas fa-user-friends viewer-icon" />
                  {` ${viewers?.memberCount + viewers?.guestCount}`}
                  <span className="reading-now-text">{` reading now`}</span>
                </span>
              </>
            )}
            {threadIsRead && markUnreadAction ? (
              <>
                <span className="spacer-bead">•</span>
                <MarkUnreadButton markUnread={markUnreadAction}>{markUnreadText}</MarkUnreadButton>
              </>
            ) : null}
          </div>
        )}
      </div>
      {showTopRating && !loadHideRatingsFromStorageBoolean() && (
        <div className="thread-ratings">
          {firstPostTopRating && (
            <>
              <img
                className="top-rating"
                src={topRating.url}
                alt={`Top rating: ${topRating.name}`}
              />
              <span className="rating-count">{firstPostTopRating.count}</span>
            </>
          )}
        </div>
      )}
      <Link
        className="info"
        title="Go to latest post"
        to={`/thread/${id}/${lastPage}#post-${lastPost ? lastPost.id : 1}`}
      >
        {!minimal ? (
          <div className="stats-container">
            <span>
              <i className="fas fa-comments stats-container-icon" />
              &nbsp;
              {postCount}
              &nbsp;
              {postCount === 1 ? 'post' : 'posts'}
            </span>
            <span title={createdAt}>
              <i className="fas fa-clock stats-container-icon" />
              &nbsp;
              {threadDate}
            </span>
          </div>
        ) : (
          <div className="stats-container-minimal">
            <div className="stats-container-item">
              <i className="fas fa-comments stats-container-icon" />
              <span className="stats-container-text">{postCount}</span>
            </div>
            <div className="stats-container-item">
              <i className="fas fa-clock stats-container-icon" />
              <span className="stats-container-text">{threadDate}</span>
            </div>
          </div>
        )}
        {lastPost && lastPost.user && (
          <div className="latest-post">
            <div className="post-info">
              <span>
                <i className="fas fa-angle-double-right" />
                <span> </span>
                {lastPostDate}
              </span>
              <span className="post-author">
                <span>by </span>
                <UserRoleWrapper user={lastPost.user}>{lastPost.user.username}</UserRoleWrapper>
              </span>
            </div>
            <UserAvatar
              className="threaditem-user-avatar"
              src={lastPostAvatar}
              alt={`${lastPost.user.username}'s avatar`}
            />
          </div>
        )}
      </Link>

      {backgroundUrl && (
        <div
          className="background-image"
          style={{
            backgroundImage: `url(${backgroundUrl})`,
            backgroundSize: backgroundType === 'cover' ? 'cover' : null,
            backgroundPosition: 'center center',
            backgroundRepeat: backgroundType === 'tiled' ? 'repeat' : 'no-repeat',
          }}
        />
      )}
    </StyledThreadItem>
  );
};

ThreadItem.propTypes = {
  id: PropTypes.number.isRequired,
  createdAt: PropTypes.string.isRequired,
  user: PropTypes.shape({
    avatarUrl: PropTypes.string,
    username: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  deleted: PropTypes.bool,
  iconId: PropTypes.number.isRequired,
  lastPost: PropTypes.shape({
    id: PropTypes.number.isRequired,
    createdAt: PropTypes.string.isRequired,
    user: PropTypes.shape({
      avatarUrl: PropTypes.string,
      username: PropTypes.string.isRequired,
    }).isRequired,
  }),
  locked: PropTypes.bool,
  pinned: PropTypes.bool,
  postCount: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  unreadPostCount: PropTypes.number,
  showTopRating: PropTypes.bool,
  firstPostTopRating: PropTypes.shape({
    count: PropTypes.number.isRequired,
    rating: PropTypes.string.isRequired,
  }),
  firstUnreadId: PropTypes.number,
  tags: PropTypes.arrayOf(PropTypes.object),
  backgroundUrl: PropTypes.string,
  backgroundType: PropTypes.string,
  threadIsRead: PropTypes.bool,
  threadOpacity: PropTypes.string,
  markUnreadAction: PropTypes.func,
  markUnreadText: PropTypes.string,
  viewers: PropTypes.shape({
    memberCount: PropTypes.number.isRequired,
    guestCount: PropTypes.number.isRequired,
  }),
  minimal: PropTypes.bool,
};

ThreadItem.defaultProps = {
  deleted: false,
  locked: false,
  pinned: false,
  unreadPostCount: 0,
  showTopRating: false,
  firstPostTopRating: null,
  firstUnreadId: 0,
  tags: [{}],
  backgroundUrl: '',
  backgroundType: '',
  threadIsRead: true,
  threadOpacity: undefined,
  markUnreadText: 'Mark unread',
  lastPost: undefined,
  markUnreadAction: undefined,
  viewers: undefined,
  minimal: false,
};

export default ThreadItem;

export const StyledThreadItem = styled.div`
  background: ${ThemeBackgroundDarker};
  margin-bottom: calc(${ThemeVerticalPadding} / 2);
  font-size: ${ThemeFontSizeMedium};
  opacity: ${(props) => props.threadOpacity};
  position: relative;
  min-height: 60px;

  display: grid;
  ${(props) =>
    props.showTopRating
      ? `
      grid-template-columns: 55px 1fr 70px ${props.minimal ? 140 : 310}px;`
      : `
      grid-template-columns: 55px 1fr ${props.minimal ? 140 : 320}px;
  `}

  align-items: center;

  .thread-title {
    ${(props) => props.minimal && `font-size: ${ThemeFontSizeLarge};`}
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    ${(props) => props.minimal && `font-size: ${ThemeFontSizeLarge};`}
  }

  .image {
    text-align: center;
    img {
      width: 100%;
    }
  }

  .image,
  .info {
    height: 100%;
    padding: 0 ${ThemeVerticalPadding} 0
      ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundLighter};
    box-sizing: border-box;
  }

  .thread-icon {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .thread-icon-inner {
    width: 40px;
    height: 40px;
  }

  .content {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
  }

  .locked {
    color: #ffcb00;
  }
  .pinned {
    color: #acff49;
  }
  .deleted {
    color: #ff3535;
  }

  .first-row {
    align-items: center;
  }

  .unread-posts {
    background: ${ThemeHighlightWeaker};
    padding: 5px;
    margin-bottom:  0;
    margin-left: 8px;
    display: inline-block;
    line-height: initial;
    font-size: ${ThemeFontSizeSmall};
    color: white;
    transition: 0.2s;
    white-space: nowrap;

    &:hover {
      opacity: 1;
    }
  }

  .second-row {
    font-size: ${ThemeFontSizeSmall};
    text-overflow: ellipsis;
    padding-top: ${ThemeVerticalPadding};

    .thread-tag {
      padding: 1px 2px;
      background: ${ThemeBackgroundLighter};
    }
  }

  .thread-user {
    margin-right: 6px;
  }

  .viewer-count {
    margin: 0 6px;
  }

  ${MobileMediaQuery} {
    .reading-now-text {
      display: none;
    }
  }
 
  .thread-ratings {
    padding: 0;
    font-size: ${ThemeFontSizeSmall};
    text-align: center;
  }

  .top-rating {
    max-width: 21px;
    height: auto;
    vertical-align: sub;
  }

  .rating-count {
    display: block;
    margin-top: calc(${ThemeVerticalPadding} / 2);
  }

  .info {
    display: flex;
    justify-content: space-between;
    align-items: center;
    font-size: ${ThemeFontSizeSmall};
  }

  .stats-container {
    display: flex;
    width: 140px;
    height: 88%;
    flex-direction: column;
    justify-content: space-around;

    .stats-container-icon {
      margin-right: 3px;
    }
  }

  .stats-container-minimal {
    display: flex;
    height: 100%;
    align-items: center;
    flex-grow: 1;
    justify-content: space-around;

    .stats-container-item {
      display: flex;
      align-items: center;
    }
    .stats-container-text {
      font-size: ${ThemeFontSizeMedium};
    }

    .stats-container-icon {
      font-size: ${ThemeFontSizeLarge};
      margin-right: 6px;
    }
  }

  .latest-post {
    display: flex;
    height: 88%;
    flex: 1;
    margin: 0 ${ThemeHorizontalPadding};
    justify-content: space-between;
    padding-left: ${ThemeHorizontalPadding};

    img {
      width: 40px;
      height: auto;
      margin: auto;
      margin-right: 0;
      max-height: 40px;
    }

    .post-info {
      display: flex;
      width: 115px;
      height: 100%;
      flex-direction: column;
      justify-content: space-around;
      line-height: initial;
      margin-right: ${ThemeHorizontalPadding};
    }

    .post-author {
      display: flex;
      width: 80%;
      white-space: pre;

      ${(props) => `
        > * {
          overflow: ${props.hasAvatar ? 'hidden;' : 'visible;'}
        }
      `}
    }
  }

  &:not(.background-image) {
    z-index: 1;
  }

  .background-image {
    width: 100%;
    height: 100%;
    display: block;
    position: absolute;
    z-index: -1;
    opacity: 0.075;
  }

  @media (max-width: 900px) {
    grid-template-columns: 56px 1fr;

    .thread-icon {
      padding: ${ThemeVerticalPadding} 0;
    }

    .thread-ratings,
    .info {
      display: none;
    }

    .spacer-bead {
      display: none;
    }

    .thread-item-pagination {
      display: inline;
      margin-top: ${ThemeVerticalPadding};

      .pagination-item:first-child {
        margin-left: 0;
      }

      .pagination-item.small {
        display: inline;
      }

      .pagination-item,
      .pagination-spacer {
        display: inline-block;
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding}!important;
      }
    }
  }
`;

StyledThreadItem.propTypes = {
  threadOpacity: PropTypes.string,
};

StyledThreadItem.defaultProps = {
  threadOpacity: '1.0',
};
