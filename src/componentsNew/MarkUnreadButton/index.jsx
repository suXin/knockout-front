import React from 'react';
import PropTypes from 'prop-types';

import LoggedInOnly from '../LoggedInOnly';
import { TextButton } from '../Buttons';

const MarkUnreadButton = ({ markUnread, children }) => (
  <LoggedInOnly>
    <TextButton small onClick={markUnread}>
      {children}
    </TextButton>
  </LoggedInOnly>
);

MarkUnreadButton.propTypes = {
  markUnread: PropTypes.func,
  children: PropTypes.string.isRequired,
};
MarkUnreadButton.defaultProps = {
  markUnread: undefined,
};

export default MarkUnreadButton;
