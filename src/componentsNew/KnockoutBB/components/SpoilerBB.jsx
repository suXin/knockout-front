import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import { ThemeTextColor } from '../../../utils/ThemeNew';

const StyledSpoiler = styled.span`
  background: ${ThemeTextColor};
  color: ${ThemeTextColor};

  a {
    color: ${ThemeTextColor};
  }

  &:hover {
    background: transparent;

    a {
      color: anime;
    }
  }
`;

const SpoilerBB = ({ children }) => <StyledSpoiler>{children}</StyledSpoiler>;

SpoilerBB.propTypes = {
  children: PropTypes.node.isRequired,
};

export default SpoilerBB;
