import React from 'react';

export const getSpotifyId = (src) => {
  try {
    const url = new URL(src);
    if (url.hostname !== 'open.spotify.com') throw new Error();
    const path = url.pathname.split('/');
    return path[path.length - 1];
  } catch (error) {
    return null;
  }
};

const SpotifyBB = ({ href, children }) => {
  try {
    const url = href || children.join('');
    const spotifyId = getSpotifyId(url);

    const path = new URL(url).pathname;

    return (
      <iframe
        src={`https://open.spotify.com/embed${path}`}
        width="300"
        height="380"
        frameBorder="0"
        allowtransparency="true"
        allow="encrypted-media"
        title={`spotify-${spotifyId}`}
      />
    );
  } catch (error) {
    return '[Bad Spotify embed.]';
  }
};

export default SpotifyBB;
