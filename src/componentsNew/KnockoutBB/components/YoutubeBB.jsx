/* eslint-disable no-useless-escape */
/* eslint-disable prefer-destructuring */
import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import VisibilitySensor from 'react-visibility-sensor';

/**
 * A styled image block component.
 *
 * @type {Component}
 */
const StyledYoutubeWrapper = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  ${(props) => (props.small ? 'max-width: 600px;' : 'max-width: 960px;')}

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    content: '';
    display: table;
    clear: both;
  }

  ${(props) => props.selected && `box-shadow: 0px 0px 0 1px #2900ff;`}
`;
const StyledYoutube = styled.iframe`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`;
const StyledYoutubePlaceholder = styled.div`
  display: block;
  margin: 15px 0;

  position: relative;
  width: 100%;
  max-width: 960px;

  img {
    width: 100%;
    height: 100%;
  }

  &:before {
    content: '';
    width: 1px;
    margin-left: -1px;
    float: left;
    height: 0;
    padding-top: calc(540 / 960 * 100%);
  }
  &:after {
    display: table;
    clear: both;
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);

    content: '\f167';
    font-family: 'Font Awesome 5 Brands', sans-serif;
    font-size: 100px;
  }

  &:hover {
    img {
      filter: brightness(1.1);
    }
    &:after {
      opacity: 0.2;
    }
  }
`;

export const getYoutubeId = (url, rawId = false) => {
  let youtubeId = '';
  const ytOnly = /^(http(s)?:\/\/)?((w){3}.)?youtu(be|.be)?(\.com)?\/.+/;
  const isYoutubeUrl = ytOnly.exec(url);

  if (!isYoutubeUrl || !isYoutubeUrl[0]) {
    return null;
  }

  const youtubeIdRegex = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|shorts\/|watch\?v=|\&v=)([^#\&\?]*)(?:(\?t|\&t|&start)=(\d+))?.*/;
  const filteredUrl = youtubeIdRegex.exec(url);

  if (!filteredUrl || !filteredUrl[2]) {
    return null;
  }

  if (filteredUrl[2] !== undefined) {
    // eslint-disable-next-line no-useless-escape
    youtubeId = filteredUrl[2].split(/[^0-9a-z_\-]/i)[0];
  } else {
    youtubeId = filteredUrl;
  }

  if (rawId) {
    return youtubeId;
  }

  if (filteredUrl[4]) {
    youtubeId = `${youtubeId}?start=${filteredUrl[4]}`;
  }

  // display autoplay
  let firstParamCharacter = '?';
  if (youtubeId.includes('?')) {
    firstParamCharacter = '&';
  }
  youtubeId += `${firstParamCharacter}autoplay=0&rel=0`;

  return youtubeId;
};

const YoutubeBB = ({ children, href, small }) => {
  const [visible, setVisible] = useState(false);
  const onChange = (isVisible) => isVisible && setVisible(isVisible);

  try {
    const youtubeUrl = href || children.join('');

    return visible ? (
      <StyledYoutubeWrapper small={small}>
        <StyledYoutube
          title={getYoutubeId(youtubeUrl)}
          type="text/html"
          width="1280"
          height="720"
          src={`https://www.youtube.com/embed/${getYoutubeId(youtubeUrl)}`}
          frameBorder="0"
          allowFullScreen="allowfullscreen"
        />
      </StyledYoutubeWrapper>
    ) : (
      <VisibilitySensor partialVisibility minTopValue={150} intervalDelay="250" onChange={onChange}>
        <StyledYoutubePlaceholder onClick={() => onChange(true)} title="Play YouTube video">
          <img
            src={`https://img.youtube.com/vi/${getYoutubeId(youtubeUrl, true)}/hqdefault.jpg`}
            alt="Thumbnail for Youtube video"
          />
        </StyledYoutubePlaceholder>
      </VisibilitySensor>
    );
  } catch (error) {
    return '[Bad Twitter embed.]';
  }
};

YoutubeBB.propTypes = {
  children: PropTypes.arrayOf(PropTypes.string),
  href: PropTypes.string,
};

YoutubeBB.defaultProps = {
  children: [],
  href: '',
};

export default YoutubeBB;
