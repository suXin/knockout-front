import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const StyledVocaroo = styled.iframe`
  width: 100%;
  max-width: 350px;
  height: 80px;
`;

export const getVocarooId = (url) => {
  const regex = /(https:\/\/(?:www\.|(?!www))(vocaroo\.com|voca\.ro)\/([\d\w]+))/;

  try {
    const result = regex.exec(url);

    if (!result[3]) {
      return null;
    }

    return result[3];
  } catch (error) {
    return null;
  }
};

const VocarooBB = ({ content, href }) => {
  try {
    const vocarooUrl = href || `${content}`;
    const vocarooId = getVocarooId(vocarooUrl);

    return (
      <StyledVocaroo
        title={vocarooId}
        type="text/html"
        width="100%"
        height="auto"
        src={`https://vocaroo.com/embed/${vocarooId}`}
        frameBorder="0"
        allowFullScreen="allowfullscreen"
      />
    );
  } catch (error) {
    return '[Bad Vocaroo embed.]';
  }
};

VocarooBB.propTypes = {
  src: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  // eslint-disable-next-line react/forbid-prop-types
  attributes: PropTypes.any,
};

VocarooBB.defaultProps = {
  attributes: undefined,
};

export default VocarooBB;
