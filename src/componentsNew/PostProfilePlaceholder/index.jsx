import React from 'react';
import Placeholder from '../Placeholder';
import { StyledPost } from '../Post';

const PostProfilePlaceholder = () => (
  <StyledPost hideUserWrapper profileView>
    <div className="post-body">
      <div className="post-toolbar">
        <div className="post-details">
          <Placeholder width={500} marginBottom={0} />
          <Placeholder width={100} marginBottom={0} />
        </div>
      </div>

      <div className="post-content">
        <Placeholder width={800} />
        <Placeholder width={400} />
      </div>
    </div>
  </StyledPost>
);

export default PostProfilePlaceholder;
