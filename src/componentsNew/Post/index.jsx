/* eslint-disable react/forbid-prop-types */
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { TextNode } from 'shortcode-tree';
import PostEditor from '../PostEditor';
import { MobileMediaQuery } from '../SharedStyles';
import { getPost } from '../../services/posts';
import {
  loadRatingsXrayFromStorageBoolean,
  loadHideRatingsFromStorageBoolean,
} from '../../services/theme';
import humanizeDuration from '../../utils/humanizeDuration';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHighlightStronger,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemePostLineHeight,
  ThemeBannedUserColor,
} from '../../utils/ThemeNew';
import getSelectedText from '../../utils/getSelectedText';
import RatingBar from '../Rating/RatingBar';
import PostControls from './components/PostControls';
import ParseBB, { bbToTree } from '../KnockoutBB/Parser';
import ErrorBoundary from '../ErrorBoundary';
import UserInfo from './components/UserInfo';
import FlagIcon from './components/FlagIcon';
import minimalDateFormat from '../../utils/minimalDateFormat';

dayjs.extend(relativeTime);

const Post = ({
  postBody,
  postDate,
  postThreadPostNumber,
  postId,
  user,
  byCurrentUser,
  bans,
  threadId,
  threadLocked,
  postPage,
  isUnread,
  hideUserWrapper,
  hideControls,
  subforumName,
  countryName,
  countryCode,
  isLinkedPost,
  ratings,
  threadPage,
  subforumId,
  handleReplyClick,
  handleReportClick,
  postSubmitFn,
  profileView,
  threadInfo,
  canRate,
}) => {
  const ratingsXray = loadRatingsXrayFromStorageBoolean();

  const [showBBCode, setShowBBCode] = useState(false);
  const [ratingsState, setRatingsState] = useState(ratings);
  const [editing, setEditing] = useState(false);

  const refreshPost = async (id) => {
    const updatedPost = (await getPost(id)).data;

    setRatingsState(updatedPost.ratings);
    setEditing(false);
  };

  useEffect(() => {
    setRatingsState(ratings);
  }, [ratings]);

  const toggleShowBBCode = () => setShowBBCode(!showBBCode);

  const replyToPost = () => {
    // if text is highlighted within the post,
    // only include that text in the quote
    let quotePyramidsRemovedString = null;
    const postBodyString = postBody.toString();
    const selectedText = getSelectedText();
    if (selectedText !== '' && postBodyString.includes(selectedText)) {
      quotePyramidsRemovedString = selectedText;
    } else {
      quotePyramidsRemovedString = postBodyString;
    }

    const nodeTree = bbToTree(quotePyramidsRemovedString);
    const extractQuotes = (tree) => {
      if (tree.shortcode === null && tree.children && tree.children.length > 0) {
        return tree.children.map(extractQuotes).join('');
      }

      if (tree instanceof TextNode || tree.shortcode === null) {
        return tree.text;
      }

      if (tree.shortcode.name !== 'quote') {
        return tree.shortcode.codeText;
      }

      return '';
    };

    try {
      quotePyramidsRemovedString = extractQuotes(nodeTree);
    } catch (error) {
      console.log(error);
      console.error('Could not remove quotes.');

      quotePyramidsRemovedString = postBody;
    }
    const content = `[quote mentionsUser="${
      user.id
    }" postId="${postId}" threadPage="${threadPage}" threadId="${threadId}" username="${
      user.username
    }"]${quotePyramidsRemovedString.trim()}[/quote]`;

    handleReplyClick(content);

    window.scrollTo(0, document.body.scrollHeight);
  };

  const postDateHuman = dayjs().isBefore(dayjs(postDate)) ? 'Now' : dayjs().to(dayjs(postDate));

  let postHeaderTitle = new Intl.DateTimeFormat(undefined, {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
  }).format(new Date(postDate));

  const postDateHumanMinimal =
    postDateHuman !== 'Now' ? minimalDateFormat(dayjs(postDate).fromNow(true), false) : 'Now';

  if (countryName) postHeaderTitle += ` from ${countryName}`;

  const postIndicatorIcons = (() => {
    return (
      <>
        {byCurrentUser && !profileView && (
          <span className="post-indicator is-self" title="You">
            <i className="fas fa-box-open" />
            <span className="indicator-text"> You</span>
          </span>
        )}
        {isLinkedPost && (
          <span className="post-indicator is-linked" title="Linked">
            <i className="fas fa-hashtag" />
            <span className="indicator-text"> Linked</span>
          </span>
        )}
        {isUnread && (
          <span className="post-indicator is-unread" title="New">
            <i className="fas fa-asterisk" />
            <span className="indicator-text"> New</span>
          </span>
        )}
      </>
    );
  })();

  const postCountryFlag = (() => {
    return (
      <>
        {countryName && countryCode && (
          <>
            <span className="from-text">{' from'}</span>
            <FlagIcon code={countryCode.toLowerCase()} />
          </>
        )}
      </>
    );
  })();

  return (
    <StyledPost
      id={`post-${postId}`}
      hideUserWrapper={hideUserWrapper}
      canRate
      profileView={profileView}
    >
      {!hideUserWrapper && <UserInfo user={user} />}
      <div className="post-body">
        <div className="post-toolbar">
          <Link
            className="post-details"
            to={`${threadId ? `/thread/${threadId}/${postPage}` : ''}#post-${postId}`}
            title={postHeaderTitle}
          >
            {postIndicatorIcons}
            {profileView && threadInfo && (
              <span className="thread-info">
                <span className="thread-info-title" title={threadInfo.title}>
                  {threadInfo.title}
                </span>
                <span>
                  in&nbsp;
                  {threadInfo.subforum ? threadInfo.subforum.name : 'Subforum'}
                </span>
              </span>
            )}
            <span className="post-date-human">{postDateHuman}</span>
            <span className="post-date-human-minimal">{postDateHumanMinimal}</span>
            {postCountryFlag}
            {postThreadPostNumber > 0 && (
              <span className="thread-post-number">{`#${postThreadPostNumber}`}</span>
            )}
          </Link>
          {!hideControls && (
            <PostControls
              postId={postId}
              user={user}
              toggleEditable={() => setEditing((value) => !value)}
              showBBCode={toggleShowBBCode}
              replyToPost={replyToPost}
              reportPost={handleReportClick}
              threadLocked={threadLocked}
              byCurrentUser={byCurrentUser}
            />
          )}
        </div>

        <div className="post-content">
          {editing || showBBCode ? (
            <PostEditor
              subforumId={subforumId}
              threadId={threadId}
              initialContent={postBody}
              type={editing ? 'edit' : 'code'}
              postId={postId}
              postSubmitFn={postSubmitFn}
            />
          ) : (
            <ErrorBoundary errorMessage="Invalid KnockoutBB code.">
              <ParseBB content={postBody} smallEmbeds={profileView} />
            </ErrorBoundary>
          )}
        </div>

        {!loadHideRatingsFromStorageBoolean() && (
          <RatingBar
            postId={postId}
            ratings={ratingsState}
            userId={user.id}
            refreshPost={refreshPost}
            xrayEnabled={ratingsXray}
            ratingDisabled={byCurrentUser || !canRate}
            byCurrentUser={byCurrentUser}
            subforumName={subforumName}
          />
        )}

        {bans &&
          bans.length > 0 &&
          bans.map((ban) => {
            const { banReason, createdAt, expiresAt } = ban;
            const banLength = humanizeDuration(createdAt, expiresAt);
            const banBannedBy = ban.banBannedBy || ban.bannedBy.username;
            return (
              <div key={banReason + createdAt} className="ban-item">
                <img src="/static/icons/police-badge.png" alt="Police Badge" />
                <span>
                  {`User was banned for this post by ${banBannedBy}`}
                  &nbsp;with reason &quot;
                  <strong>{banReason}</strong>
                  &quot; for&nbsp;
                  {banLength}
                </span>
              </div>
            );
          })}
      </div>
    </StyledPost>
  );
};

Post.propTypes = {
  postBody: PropTypes.string.isRequired,
  postDate: PropTypes.string.isRequired,
  postThreadPostNumber: PropTypes.number,
  postId: PropTypes.number.isRequired,
  user: PropTypes.shape({
    id: PropTypes.number,
    username: PropTypes.string,
    usergroup: PropTypes.number,
    isBanned: PropTypes.bool,
    avatarUrl: PropTypes.string,
    backgroundUrl: PropTypes.string,
  }),
  byCurrentUser: PropTypes.bool,
  bans: PropTypes.array,
  threadId: PropTypes.number.isRequired,
  threadLocked: PropTypes.bool,
  postPage: PropTypes.number,
  isUnread: PropTypes.bool,
  hideUserWrapper: PropTypes.bool,
  hideControls: PropTypes.bool,
  subforumName: PropTypes.string,
  countryName: PropTypes.string,
  countryCode: PropTypes.string,
  isLinkedPost: PropTypes.bool,
  ratings: PropTypes.array,
  threadPage: PropTypes.number,
  subforumId: PropTypes.number,
  handleReplyClick: PropTypes.func,
  handleReportClick: PropTypes.func,
  postSubmitFn: PropTypes.func,
  profileView: PropTypes.bool,
  threadInfo: PropTypes.shape({
    title: PropTypes.string.isRequired,
    subforum: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  }),
  canRate: PropTypes.bool,
};

Post.defaultProps = {
  postThreadPostNumber: 0,
  byCurrentUser: false,
  bans: [],
  threadLocked: false,
  postPage: 1,
  isUnread: false,
  hideUserWrapper: false,
  hideControls: false,
  subforumName: 'Subforum',
  countryName: '',
  countryCode: '',
  isLinkedPost: false,
  ratings: [],
  threadPage: 1,
  subforumId: undefined,
  handleReplyClick: () => {},
  handleReportClick: () => {},
  postSubmitFn: () => {},
  profileView: false,
  threadInfo: undefined,
  user: {},
  canRate: true,
};

export default Post;

export const StyledPost = styled.div`
  display: grid;
  ${(props) => !props.hideUserWrapper && 'grid-template-columns: 230px 1fr;'}
  min-height: ${(props) => (props.profileView ? 60 : 210)}px;
  overflow: hidden;
  margin-bottom: calc(${ThemeVerticalPadding} / 2);
  font-size: ${ThemeFontSizeMedium};
  ${(props) => props.anchor && `outline: 1px solid ${ThemeHighlightWeaker};`}

  .post-body {
    display: grid;
    grid-template-rows: 30px 1fr;
    background-color: ${ThemeBackgroundDarker};
    ${(props) => !props.canRate && `padding-bottom: calc(${ThemeVerticalPadding(props)} * 3);`}
    white-space: break-spaces;
    .bb-post {
      padding: ${ThemeVerticalPadding} 0;
      line-height: ${ThemePostLineHeight}%;
    }
  }

  .post-toolbar {
    background: ${ThemeBackgroundLighter};
    line-height: 30px;
    padding: 0 ${ThemeHorizontalPadding};
    ${(props) => !props.profileView && 'padding-right: 0;'}
    font-size: ${ThemeFontSizeMedium};
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .post-details {
    text-overflow: ellipsis;
    overflow: hidden;
    white-space: nowrap;
    min-width: 0;
    width: 0;
    flex: 1;
    padding-left: calc(${ThemeHorizontalPadding} / 2);
    ${(props) =>
      props.profileView &&
      `
      display: flex;
      justify-content: space-between;
    `}

    @media (max-width: 700px) {
      .from-text {
        display: none;
      }

      .post-date-human {
        display: none;
      }
    }

    @media (min-width: 700px) {
      .post-date-human-minimal {
        display: none;
      }
    }
  }

  .thread-info {
    display: flex;
    align-items: center;
    min-width: 0;
    margin-right: 10px;
  }

  .thread-info-title {
    font-weight: bold;
    margin-right: 5px;
    display: inline-block;
    text-overflow: ellipsis;
    overflow: hidden;
  }

  .thread-post-number {
    margin-left: ${ThemeHorizontalPadding};
    opacity: 0.5;
  }

  .post-content {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} calc(${ThemeHorizontalPadding} * 1.5);
    font-size: ${ThemeFontSizeMedium};
    line-height: 1.5;
    white-space: pre-wrap;
    overflow: hidden;
    overflow-wrap: break-word;
    min-height: ${(props) => (props.profileView ? 90 : 128)}px;
  }

  .ban-item {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeBannedUserColor};
    display: flex;
    align-items: center;

    img {
      margin-right: ${ThemeHorizontalPadding};
    }
  }

  .post-indicator {
    font-size: ${ThemeFontSizeSmall};
    text-transform: uppercase;
    margin-right: ${ThemeHorizontalPadding};
    padding: 0.1875rem ${ThemeHorizontalPadding} calc(0.1875rem - 1px); /*i hate css*/
    transition: opacity 150ms ease-in-out;
    font-weight: bold;
  }
  .is-linked {
    background: ${ThemeBannedUserColor};
    color: white;
  }
  .is-self {
    background: ${ThemeHighlightStronger};
  }
  .is-unread {
    background: ${ThemeHighlightWeaker};
    color: white;
  }

  ${MobileMediaQuery} {
    .indicator-text {
      display: none;
    }
  }

  .user-info {
    display: contents;
    margin-bottom: ${ThemeVerticalPadding};

    .user-title {
      z-index: 2;
      margin-bottom: ${ThemeVerticalPadding};
      font-size: ${ThemeFontSizeSmall};
      transition: opacity ease-in-out 500ms;
    }
  }

  @media (max-width: 900px) {
    grid-template-columns: unset;
    ${(props) => !props.profileView && 'grid-template-rows: 5rem 1fr;'}

    .post-toolbar {
      a {
        font-size: ${ThemeFontSizeSmall};
      }
    }
  }

  .flag-icon {
    margin-left: ${ThemeHorizontalPadding}
  }
`;
