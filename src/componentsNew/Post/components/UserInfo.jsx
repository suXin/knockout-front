/* eslint-disable react/forbid-prop-types */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import dayjs from 'dayjs';
import styled from 'styled-components';

import { Link } from 'react-router-dom';
import config from '../../../../config';
import UserRoleWrapper from '../../UserRoleWrapper';
import { DesktopMediaQuery } from '../../SharedStyles';
import {
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
} from '../../../utils/ThemeNew';
import UserProfileTopRating, {
  UserProfileTopRatingWrapper,
} from '../../../views/UserProfile/components/UserProfileTopRating';
import { USER_GROUPS } from '../../../utils/userGroups';
import { loadHideRatingsFromStorageBoolean } from '../../../services/theme';

const UserInfo = ({ user, profileView, topRatings }) => {
  const [viewBackground, setViewBackground] = useState(false);

  const hasAvatar =
    user.avatarUrl && user.avatarUrl.length !== 0 && !user.avatarUrl.includes('none.webp');
  let url = `${config.cdnHost}/image/${user.avatarUrl}`;
  let title = `${user.username}'s avatar`;
  const bgUrl = user.backgroundUrl ? `${config.cdnHost}/image/${user.backgroundUrl}` : undefined;
  const userJoinDateShort = dayjs(user.createdAt).format('MMM YYYY');
  const userJoinDateLong = dayjs(user.createdAt).format('DD/MM/YYYY');
  const userCakeDay = dayjs(user.createdAt).format('DD/MM') === dayjs(new Date()).format('DD/MM');

  if (!hasAvatar) {
    url = `${config.cdnHost}/image/none.webp`;
  }
  if (user.isBanned) {
    url = 'https://img.icons8.com/color/80/000000/minus.png';
    title = `${user.username} is banned!`;
  }

  const isDeletedUser = user.username === 'DELETED_USER';

  let linkTo = `/user/${user.id}`;
  if (isDeletedUser) {
    linkTo = '#';
  }

  const bgTitleText = `${user.username}'s Background Avatar`;

  return (
    <UserInfoWrapper
      $profileView={profileView}
      $hasBackground={bgUrl !== undefined}
      usergroup={user.usergroup}
      viewBackground={viewBackground}
      className={`user-wrapper ${isDeletedUser ? 'deleted-user' : ''}`}
    >
      <Link className="profile-link" title={`${user.username}'s profile`} to={linkTo}>
        <img className="user-avatar" alt={title} src={url} />
        <div className="user-info">
          <UserRoleWrapper user={user} className={bgUrl && 'user-info-name'}>
            {user.username}
          </UserRoleWrapper>
          {user.usergroup === USER_GROUPS.MODERATOR_IN_TRAINING && (
            <div className="user-title">Moderator in training</div>
          )}
        </div>
      </Link>
      {!isDeletedUser && bgUrl && (
        <div
          className="user-background"
          onClick={() => setViewBackground((value) => !value)}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              setViewBackground((value) => !value);
            }
          }}
          role="button"
          tabIndex="0"
          title={bgTitleText}
        >
          <img
            className="user-background-image"
            alt=""
            title={bgTitleText}
            draggable="false"
            onDragStart={(e) => {
              e.preventDefault();
            }}
            src={bgUrl}
          />
        </div>
      )}
      {!isDeletedUser && (
        <span className="user-join-date" title={userJoinDateLong}>
          {userCakeDay && `🍰 `}
          {userJoinDateShort}
          {userCakeDay && ` 🎉`}
        </span>
      )}
      {!loadHideRatingsFromStorageBoolean() && profileView && (
        <UserProfileTopRating topRatings={topRatings} />
      )}
    </UserInfoWrapper>
  );
};

UserInfo.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    usergroup: PropTypes.number.isRequired,
    backgroundUrl: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
    createdAt: PropTypes.string.isRequired,
    isBanned: PropTypes.bool.isRequired,
  }).isRequired,
  profileView: PropTypes.bool,
  topRatings: PropTypes.arrayOf(PropTypes.object),
};

UserInfo.defaultProps = {
  profileView: false,
  topRatings: [],
};

export default UserInfo;

const UserInfoWrapper = styled.div`
  display: flex;
  justify-content: start;
  align-items: center;
  flex-direction: column;
  position: relative;
  ${(props) => (!props.$profileView ? 'min-height: 120px;' : 'min-height: 440px;')}
  background: rgba(255, 255, 255, 0.05);
  padding-top: 45px;

  transition: min-height 250ms ease-in-out;

  ${DesktopMediaQuery} {
    &:hover {
      .user-background {
        filter: none;
      }
    }
    ${(props) =>
      props.viewBackground &&
      `
      min-height: 440px;

      .user-background {
        filter: none;
      }

      .user-avatar,
      .user-join-date,
      .user-role-wrapper-component,
      .user-title,
      ${UserProfileTopRatingWrapper} {
        opacity: 0;
      }`}
  }

  .profile-link {
    z-index: 2;
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .user-info {
    display: contents;
    margin-bottom: ${ThemeVerticalPadding};

    .user-title {
      z-index: 2;
      margin-bottom: ${ThemeVerticalPadding};
      font-size: ${ThemeFontSizeSmall};
      transition: opacity ease-in-out 500ms;
    }

    .user-info-name {
      ${(props) =>
        props.$hasBackground && props.usergroup === 1 && 'filter: drop-shadow(0px 0px 2px black);'}
    }
  }

  .user-role-wrapper-component {
    font-size: ${ThemeFontSizeMedium};
    z-index: 3;
    top: 175px;
    left: 50%;
    font-weight: bold;
    height: 20px;
    transition: opacity ease-in-out 500ms;
  }

  .user-join-date {
    font-size: ${ThemeFontSizeSmall};
    z-index: 3;
    top: 195px;
    left: 50%;
    ${(props) => props.$hasBackground && 'color: white;'}
    transition: opacity ease-in-out 500ms;
  }

  .deleted-user {
    pointer-events: none;
  }

  .user-avatar {
    z-index: 2;
    margin-bottom: ${ThemeVerticalPadding};
    transition: opacity ease-in-out 500ms;
  }

  .user-background {
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    content: '';
    filter: ${(props) => (props.viewBackground ? 'none' : 'brightness(0.5) contrast(0.925)')};
    transition: filter ease-in-out 500ms;
    cursor: pointer;
    user-select: none;
  }

  .user-background-image {
    mask-image: linear-gradient(rgb(255, 255, 255) 0%, rgb(255, 255, 255) 85%, transparent 100%);
  }

  @media (max-width: 900px) {
    overflow: hidden;
    min-height: unset;
    height: 5rem;
    align-items: left;
    justify-content: left;
    padding: unset;
    flex-direction: row;
    padding-left: ${ThemeHorizontalPadding};

    .user-role-wrapper-component {
      position: initial;
      transform: none;
    }
    .profile-link {
      align-items: left;
      justify-content: left;
      flex-direction: row;
    }
    .user-avatar {
      position: initial;
      width: auto;
      transform: none;
      max-width: 70px;
      max-height: 70px;
      margin: 0 ${ThemeHorizontalPadding} 0 0;
    }
    .user-join-date {
      display: none;
    }

    .user-background-image {
      position: absolute;
      width: 100%;
      top: 50%;
      transform: translateY(-50%);
    }

    .user-info {
      display: block;
      z-index: 2;

      .user-title {
        margin-top: calc(${ThemeVerticalPadding} / 2);
      }
    }
  }
`;
