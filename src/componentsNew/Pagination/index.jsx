/* eslint-disable react/no-array-index-key */

import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import {
  ThemeBackgroundDarker,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeKnockoutRed,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import { POSTS_PER_PAGE } from '../../utils/postsPerPage';
import { MobileMediaQuery } from '../SharedStyles';

const scrollToTop = () => {
  document.documentElement.scrollTop = 0;
};

const getUnfocusedRange = (totalPages, maxSize) => {
  const output = [];
  const center = Math.ceil(maxSize / 2);

  // count up from page 1 to the center
  for (let i = 1; i <= center; i += 1) {
    output.push(i);
  }

  // include a deliniator
  output.push(null);

  // count down from the last page to the center
  for (let i = center; i > 0; i -= 1) {
    output.push(totalPages - (i - 1));
  }

  return output;
};

const getFocusedRange = (totalPages, maxSize, currentPage) => {
  const output = [];
  const center = Math.ceil(maxSize / 2);

  // if the user is on a page near the start of the thread
  if (currentPage <= center) {
    for (let i = 1; i <= maxSize; i += 1) {
      output.push(i);
    }
    output[maxSize - 2] = null;
    output[maxSize - 1] = totalPages;
  }

  // if the user is on a page near the middle of the thread
  if (currentPage > center && currentPage <= totalPages - center) {
    for (let i = center - 1; i > 0; i -= 1) {
      output.push(currentPage - i);
    }
    for (let i = 0; i < center; i += 1) {
      output.push(currentPage + i);
    }
    output[0] = 1;
    output[1] = null;
    output[maxSize - 2] = null;
    output[maxSize - 1] = totalPages;
  }

  // if the user is on a page near the end of the thread
  if (currentPage > totalPages - center) {
    for (let i = maxSize; i > 0; i -= 1) {
      output.push(totalPages - (i - 1));
    }
    output[0] = 1;
    output[1] = null;
  }

  return output;
};

const getFullRange = (totalPages) => {
  const output = [];

  // count up to the max Size
  for (let i = 1; i <= totalPages; i += 1) {
    output.push(i);
  }

  return output;
};

const handlePageChange = async (fn, page) => {
  await fn(page);
  scrollToTop();
};

const PaginationLink = ({ small, page, pagePath, active, useButtons, pageChangeFn, children }) => (
  <StyledPaginationLink
    className={`pagination-item ${active ? 'active' : ''} ${small ? 'small' : ''}`}
    key={`to-${pagePath}${page}`}
    to={`${pagePath}${page}`}
    onClick={useButtons ? () => handlePageChange(pageChangeFn, page) : scrollToTop}
    as={useButtons && 'button'}
  >
    {children}
  </StyledPaginationLink>
);

const StyledPaginationLink = styled(Link)``;

PaginationLink.propTypes = {
  small: PropTypes.bool.isRequired,
  page: PropTypes.number.isRequired,
  pagePath: PropTypes.string.isRequired,
  active: PropTypes.bool,
  useButtons: PropTypes.bool,
  pageChangeFn: PropTypes.func,
  children: PropTypes.node.isRequired,
};

PaginationLink.defaultProps = {
  active: false,
  useButtons: false,
  pageChangeFn: undefined,
};

const Pagination = ({
  pagePath,
  totalPosts,
  currentPage,
  pageSize = POSTS_PER_PAGE,
  small,
  showNext,
  className,
  pageChangeFn,
  useButtons,
  marginBottom,
}) => {
  const totalPages = Math.ceil(totalPosts / pageSize);

  if (totalPages < 2) {
    return <div />;
  }

  const maxSize = small ? 4 : 9;

  const currentPageInt = parseInt(currentPage, 10);

  const onLastPage = currentPageInt === totalPages;
  const nextPage = currentPageInt + 1;
  const smallClassName = small ? 'small' : '';
  let pages = [];

  if (totalPages > maxSize) {
    // more pages than we have space to show, so lets be a bit more relevent
    if (typeof currentPage === 'undefined') {
      // we dont have a current page set, so lets just show the extremes
      pages = getUnfocusedRange(totalPages, maxSize);
    } else {
      // we do have a current page set, so lets show around it and the extremes
      pages = getFocusedRange(totalPages, maxSize, currentPageInt);
    }
  } else {
    // less pages than we have space to show, so lets show everything
    pages = getFullRange(totalPages, maxSize, currentPageInt);
  }

  return (
    <PaginationWrapper className={className} marginBottom={marginBottom}>
      {pages.map((page, index) => {
        if (page == null) {
          return (
            <div className={`pagination-spacer ${smallClassName}`} key={`page-null-${index}`}>
              ...
            </div>
          );
        }
        if (page === currentPageInt) {
          return (
            <PaginationLink
              small={small}
              page={page}
              key={`page-${page}-active`}
              active
              pagePath={pagePath}
              pageChangeFn={pageChangeFn}
              useButtons={useButtons}
            >
              {page}
              <i className="fas fa-caret-up" />
            </PaginationLink>
          );
        }
        return (
          <PaginationLink
            small={small}
            page={page}
            key={`page-${page}`}
            pagePath={pagePath}
            pageChangeFn={pageChangeFn}
            useButtons={useButtons}
          >
            {page}
          </PaginationLink>
        );
      })}
      {showNext && !onLastPage && pages[0] && (
        <PaginationLink
          small={small}
          page={nextPage}
          pagePath={pagePath}
          pageChangeFn={pageChangeFn}
          useButtons={useButtons}
        >
          Next&nbsp;
          <i className="fas fa-angle-double-right" />
        </PaginationLink>
      )}
    </PaginationWrapper>
  );
};

export default Pagination;

Pagination.propTypes = {
  pagePath: PropTypes.string,
  totalPosts: PropTypes.number,
  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  small: PropTypes.bool,
  showNext: PropTypes.bool,
  className: PropTypes.string,
  useButtons: PropTypes.bool,
  marginBottom: PropTypes.bool,
  pageChangeFn: PropTypes.func,
};

Pagination.defaultProps = {
  pagePath: '/',
  pageSize: POSTS_PER_PAGE,
  small: false,
  showNext: false,
  className: '',
  currentPage: undefined,
  totalPosts: 0,
  useButtons: false,
  marginBottom: false,
  pageChangeFn: undefined,
};
const PaginationWrapper = styled.div`
  display: inline-block;
  ${(props) => props.marginBottom && `margin-bottom: ${ThemeVerticalPadding(props)};`}

  a,
  .pagination-spacer {
    display: inline-block;
  }

  .pagination-item,
  .pagination-spacer {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    margin: 0 calc(${ThemeHorizontalPadding} / 5);
    height: 30px;
    border: none;
    color: ${ThemeTextColor};
    box-sizing: border-box;

    ${MobileMediaQuery} {
      margin: 0;
    }

    &:first-child {
      margin-left: 0;
    }

    &:last-child {
      margin-right: 0;
    }

    &:hover {
      filter: brightness(1.2);
      text-decoration: underline;
    }

    &.small {
      background: none;
      display: inline;
      box-sizing: border-box;
      margin: 0;
      padding: ${ThemeVerticalPadding} calc(${ThemeHorizontalPadding} * 0.75);
    }

    &.active {
      position: relative;
      background: ${ThemeBackgroundLighter};
      pointer-events: none;

      i {
        color: ${ThemeKnockoutRed};
        position: absolute;
        left: 50%;
        bottom: -5px;
        transform: translateX(-50%);
      }
    }
  }
`;
