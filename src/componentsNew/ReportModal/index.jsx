import React, { useState } from 'react';
import PropTypes from 'prop-types';
import submitReport from '../../services/reports';
import { pushSmartNotification } from '../../utils/notification';
import Modal from '../Modals/Modal';
import { FieldLabelSmall, TextField } from '../FormControls';

const ReportModal = ({ isOpen, close, postId, subforumId }) => {
  const [reportReason, setReportReason] = useState('');

  const submit = async () => {
    if (postId === undefined) {
      return;
    }

    try {
      await submitReport({ postId, reportReason });
      close();
      setReportReason('');
    } catch (err) {
      pushSmartNotification({ error: err.message });
    }
  };

  return (
    <Modal
      iconUrl="/static/icons/siren.png"
      title="Report post"
      cancelFn={() => {
        close();
        setReportReason('');
      }}
      submitFn={submit}
      isOpen={isOpen}
    >
      <FieldLabelSmall>
        {`Which of the `}
        <a href="/rules" target="_blank">
          site rules
        </a>
        {subforumId && ` or `}
        {subforumId && (
          <a href={`/subforumRules/${subforumId}`} target="_blank">
            subforum rules
          </a>
        )}
        {` did this post break?`}
      </FieldLabelSmall>
      <TextField value={reportReason} onChange={(e) => setReportReason(e.target.value)} />
    </Modal>
  );
};

ReportModal.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  close: PropTypes.func.isRequired,
  postId: PropTypes.number,
  subforumId: PropTypes.number,
};

ReportModal.defaultProps = {
  postId: undefined,
  subforumId: undefined,
};

export default ReportModal;
