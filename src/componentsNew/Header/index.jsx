import React, { useState, useEffect, useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLocation, useHistory, Link } from 'react-router-dom';
import loadable from 'loadable-components';

import { StyledHeader, HeaderLink } from './components/style';
import UserControls from './components/UserControls';
import BannedHeaderMessage from './components/BannedHeaderMessage';
import UserGroupRestricted from '../UserGroupRestricted';
import { scrollToTop } from '../../utils/pageScroll';
import LoggedInOnly from '../LoggedInOnly';

import { updateMentions } from '../../state/mentions';
import { loadBannedMessageFromStorage } from '../../utils/bannedStorage';
import { checkLoginStatus, isLoggedIn } from '../../utils/user';
import { MODERATOR_GROUPS } from '../../utils/userGroups';
import StyleableLogo from './components/StyleableLogo';
import SubscriptionsMenu from './components/SubscriptionsMenu';
import RepliesMenu from './components/RepliesMenu';
import updateSubscriptions from '../../utils/subscriptions';
import { getEventHeaderLogo, getEventText } from '../../utils/eventDates';

import MessageOfTheDay from './components/MessageOfTheDay';
import { loadPunchyLabsFromStorageBoolean } from '../../services/theme';
import { getConversations } from '../../services/messages';
import updateMessages from '../../utils/messages';

const LogoQuotes = loadable(() => import('./components/LogoQuotes'));

const Header = () => {
  const [bannedInformation, setBannedInformation] = useState(undefined);
  const [openReports, setOpenReports] = useState(0);
  const [isOnTopOfPage, setisOnTopOfPage] = useState(true);

  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();

  const stickyHeader = useSelector((state) => state.style.stickyHeader);

  const punchyLabsEnabled = loadPunchyLabsFromStorageBoolean();

  const updateHeader = async ({ mentions, subscriptions, reports, id }) => {
    if (subscriptions && subscriptions[0]) {
      updateSubscriptions(dispatch, subscriptions);
    }

    if (reports) {
      setOpenReports(reports || 0);
    }

    if (mentions.length > 0) {
      dispatch(updateMentions(mentions));
    }

    const conversations = await getConversations();
    updateMessages(id, dispatch, conversations);
  };

  useEffect(() => {
    checkLoginStatus(location.pathname, (user) => updateHeader({ ...user }), history);

    if (!isLoggedIn()) {
      setBannedInformation(loadBannedMessageFromStorage());
    }
  }, []);

  useLayoutEffect(() => {
    const handleScroll = () => {
      setisOnTopOfPage(window.scrollY <= 5);
    };

    // Add scroll listener on mount
    window.addEventListener('scroll', handleScroll, { capture: true, passive: true });

    // Remove scroll listener on unmount
    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return (
    <>
      {bannedInformation && (
        <BannedHeaderMessage
          banMessage={bannedInformation.banMessage}
          threadId={bannedInformation.threadId}
        />
      )}

      <StyledHeader
        id="knockout-header"
        stickyHeader={stickyHeader}
        $isOnTopOfPage={isOnTopOfPage}
        labs={punchyLabsEnabled}
      >
        <div id="header-content">
          <div className="branding">
            <Link to="/" className="brand">
              {getEventHeaderLogo() ? (
                <img src={getEventHeaderLogo()} className="header-logo" alt="Knockout logo" />
              ) : (
                <StyleableLogo className="header-logo" />
              )}
              <div className="title-container">
                <div className="title">{`${getEventText()}!`}</div>
                <LogoQuotes isOnTopOfPage={isOnTopOfPage} />
              </div>
            </Link>

            <div id="nav-items">
              <Link to="/rules" className={`link ${isLoggedIn() && 'no-mobile'}`}>
                <i className="fas fa-atlas" />
                <span className="nav-title">Rules</span>
              </Link>

              <Link className="link" to="/search">
                <i className="fas fa-search" />
                <span className="nav-title">Search</span>
              </Link>

              <LoggedInOnly>
                <Link to="/events" className="link" onClick={scrollToTop}>
                  <i className="fas fa-bullhorn" />
                  <span className="nav-title">Events</span>
                </Link>
              </LoggedInOnly>

              <LoggedInOnly>
                <SubscriptionsMenu />

                <RepliesMenu />

                <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
                  <HeaderLink
                    to="/moderate"
                    className="link"
                    onClick={() => {
                      setOpenReports(0);
                      scrollToTop();
                    }}
                  >
                    <i className="fas fa-shield-alt" />
                    <span className="nav-title">Moderation</span>
                    {openReports > 0 && <div className="link-notification">{openReports}</div>}
                  </HeaderLink>
                </UserGroupRestricted>
              </LoggedInOnly>
            </div>
          </div>
          <UserControls />

          <div className="env-tag">{punchyLabsEnabled ? 'LABS' : 'BETA'}</div>
        </div>

        <MessageOfTheDay />

        <div className="backdrop-filter" />
      </StyledHeader>
    </>
  );
};

export default Header;
