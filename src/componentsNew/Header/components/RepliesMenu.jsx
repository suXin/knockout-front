import React, { useRef } from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { darken, lighten } from 'polished';

import useDropdownMenu, {
  DropdownMenuButton,
  DropdownMenuEmpty,
  DropdownMenuItem,
  DropdownMenuOpened,
} from './DropdownMenu';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import UserRoleWrapper from '../../UserRoleWrapper';
import { updateMention } from '../../../services/mentions';
import { removeMentions } from '../../../state/mentions';
import UserAvatar from './UserAvatar';

const RepliesMenu = () => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);
  const mentions = useSelector((state) => state.mentions.mentions);
  const messages = useSelector((state) => state.messages.messages);
  const dispatch = useDispatch();

  async function handleMentionDelete(mention) {
    await updateMention(mention.postId);
    dispatch(removeMentions(mention.mentionId));
  }

  function parseMentionContent(mention) {
    return JSON.parse(mention.content)[0];
  }

  const totalReplies = messages.length + mentions.length;
  return (
    <RepliesMenuButton
      className="replies-menu"
      onClick={() => setOpen((value) => !value)}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          setOpen((value) => !value);
        }
      }}
      tabIndex="0"
      ref={buttonRef}
    >
      <div className="dropdown-menu-button-inner" title="Messages">
        <i className="fas fa-comment menu-icon" />
        {totalReplies > 0 && (
          <div className="link-notification">{totalReplies < 10 ? totalReplies : '9+'}</div>
        )}
      </div>
      {open && (
        <DropdownMenuOpened className="subscriptions-menu-dropdown" ref={menuRef}>
          <DropdownMenuItem className="dropdown-menu-header">
            <h1 className="dropdown-menu-header-text">Messages</h1>
          </DropdownMenuItem>
          <div className="dropdown-menu-body">
            {messages.map((message) => {
              const messageUser = message.user;
              return (
                <DropdownReplyItem key={message.id}>
                  <Link className="dropdown-reply-link" to={`/messages/${message.conversationId}`}>
                    <StyledUserAvatar
                      user={messageUser}
                      defaultUrl="https://img.icons8.com/color/48/000000/speech-bubble.png"
                    />
                    <div className="dropdown-reply-text">
                      <b>
                        <UserRoleWrapper user={messageUser}>{messageUser.username}</UserRoleWrapper>
                      </b>
                      {` sent you a message`}
                    </div>
                  </Link>
                </DropdownReplyItem>
              );
            })}
            {mentions.map((mention) => {
              const mentionUser = mention.mentionedBy;
              return (
                <DropdownReplyItem key={mention.mentionId}>
                  <Link
                    className="dropdown-reply-link"
                    to={`/thread/${mention.threadId}/${mention.threadPage}#post-${mention.postId}`}
                  >
                    <StyledUserAvatar
                      user={mentionUser}
                      defaultUrl="https://img.icons8.com/color/96/000000/chat.png"
                    />
                    {mentionUser ? (
                      <div className="dropdown-reply-text">
                        <b>
                          <UserRoleWrapper user={mentionUser}>
                            {mentionUser.username}
                          </UserRoleWrapper>
                        </b>
                        {` replied to your post in `}
                        <b>{`${mention.threadTitle}.`}</b>
                      </div>
                    ) : (
                      <div className="dropdown-reply-text">{parseMentionContent(mention)}</div>
                    )}
                  </Link>
                  <button
                    onClick={() => handleMentionDelete(mention)}
                    className="dropdown-reply-dismiss"
                    type="button"
                    title="Dismiss"
                  >
                    <i className="fas fa-times" />
                  </button>
                </DropdownReplyItem>
              );
            })}
            {totalReplies === 0 && (
              <DropdownMenuEmpty>
                <i className="fas fa-comment-dots dropdown-menu-empty-icon" />
                <h2 className="dropdown-menu-empty-header">No new messages</h2>
                <div className="dropdown-menu-empty-desc">
                  Messages from users and posts replies will appear here.
                </div>
              </DropdownMenuEmpty>
            )}
          </div>

          <Link
            to="/messages"
            className="dropdown-menu-footer"
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                window.location.href = '/messages';
              }
            }}
          >
            View messages
          </Link>
        </DropdownMenuOpened>
      )}
    </RepliesMenuButton>
  );
};

const RepliesMenuButton = styled(DropdownMenuButton)`
  padding: 0px 10px;
  margin-right: 10px;
  margin-left: -5px;
`;

const StyledUserAvatar = styled(UserAvatar)`
  width: 45px;
  margin-right: calc(${ThemeHorizontalPadding} * 1.5);
`;

const DropdownReplyItem = styled(DropdownMenuItem)`
  padding: calc(${ThemeVerticalPadding} * 1.5) calc(${ThemeHorizontalPadding} * 2);

  .dropdown-reply-link {
    display: flex;
    align-items: center;
  }

  .dropdown-reply-text {
    flex: 1;
    line-height: calc(${ThemeFontSizeMedium} * 1.2);
  }

  .dropdown-reply-dismiss {
    outline: none;
    border: none;
    background: none;
    cursor: pointer;
    color: ${(props) => {
      if (props.theme.mode === 'light') {
        return darken(0.1, ThemeBackgroundLighter(props));
      }
      return lighten(0.1, ThemeBackgroundLighter(props));
    }};
    padding: 0;
    font-size: 20px;

    &:hover {
      color: ${(props) => {
        if (props.theme.mode === 'light') {
          return darken(0.2, ThemeBackgroundLighter(props));
        }
        return lighten(0.2, ThemeBackgroundLighter(props));
      }};
    }
  }
`;
export default RepliesMenu;
