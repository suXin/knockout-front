/* eslint-disable no-nested-ternary */
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { darken } from 'polished';
import {
  ThemeBackgroundDarker,
  ThemeBodyWidth,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeFontSizeHuge,
  ThemeVerticalPadding,
  ThemeKnockoutRed,
} from '../../../utils/ThemeNew';

export const StyledHeader = styled.header`
  width: 100%;
  top: 0;
  left: 0;
  right: 0;
  position: ${(props) => (props.stickyHeader ? 'fixed' : 'absolute')};
  z-index: 1000;
  background: ${ThemeBackgroundLighter};
  transition: background 1s;

  /* makes for weird overflow issues. too soon for this world. :'( */
  @supports (backdrop-filter: blur(10px)) {
    .backdrop-filter {
      backdrop-filter: blur(10px);
      width: 100%;
      height: 100%;
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
      bottom: 0;
      z-index: 0;
    }

    & {
      background: ${ThemeBackgroundLighter}b8;
    }
  }

  #header-content {
    max-width: ${ThemeBodyWidth};

    display: flex;
    align-items: center;
    justify-content: center;
    margin: auto;
    box-sizing: border-box;
    padding: 0 ${ThemeHorizontalPadding};
    position: relative;
    height: 50px;
    z-index: 4;

    .user-menu-button,
    .replies-menu {
      height: 100%;
    }

    .replies-menu {
      order: 6;
    }

    .user-menu-button {
      order: 7;
    }

    .branding {
      display: flex;
      flex-grow: 1;
      justify-content: flex-start;
      align-items: center;
      height: 50px;
      min-width: auto;

      @media (max-width: 700px) {
        font-size: 14px;
      }

      .brand {
        display: flex;
        flex-direction: row;
        align-items: center;
        font-family: 'Arimo', sans-serif;
        margin-right: 30px;
        height: 100%;
      }

      .title-container {
        height: 100%;
        display: flex;
        flex-direction: column;
        justify-content: center;
        width: 130px;

        .title {
          margin-top: calc(
            ${ThemeVerticalPadding} * ${(props) => (props.$isOnTopOfPage ? 1 : 1.5)}
          );
          font-family: 'Arimo', sans-serif;

          transition: margin 200ms ease-in-out;

          @media (max-width: 900px) {
            display: none;
          }

          font-size: ${ThemeFontSizeHuge};
          font-weight: 600;
          color: ${ThemeTextColor};
          font-style: italic;
          margin-right: 4px;
          opacity: 0.925;
        }
      }
    }
  }

  #nav-items {
    display: flex;
    align-items: center;
    flex-grow: 1;
    height: 100%;

    a {
      color: ${ThemeTextColor};
      text-decoration: none;
      transition: opacity 150ms ease-in-out;
      opacity: 0.75;

      i {
        margin-right: 0.25rem;
      }

      &.link {
        margin-right: 30px;
        font-size: ${ThemeFontSizeMedium};

        span {
          @media (max-width: 900px) {
            display: none;
          }
        }
      }

      @media (max-width: 900px) {
        &.no-mobile {
          display: none;
        }
      }

      &:hover {
        opacity: 1;
      }
    }
  }

  .header-logo {
    width: auto;
    height: 40px;
    position: relative;
    top: 0;
    shape-rendering: geometricprecision;
    transition: transform 300ms ease-in-out;
  }

  .link-notification {
    background: #ec3737;
    height: 17px;
    width: 17px;
    border-radius: 50%;
    position: absolute;
    top: -7px;
    right: -15px;
    box-sizing: border-box;

    font-weight: bold;
    font-size: 9px;
    line-height: 17px;
    text-align: center;

    ${(props) =>
      props.left &&
      `
      right: unset;
      left: 0;
      transform: unset;
    `}
  }

  .link-notification.left {
    bottom: -4px;
    left: -4px;
    top: unset;
    right: unset;
  }

  @media (max-width: 900px) {
    margin-bottom: 50px;

    padding: unset;
    box-sizing: border-box;

    transition: transform 300ms ease-in-out;

    .link-notification {
      top: 1px;
      right: 2px;
    }

    #nav-items {
      position: fixed;
      left: 0;
      bottom: 0px;
      width: calc(100vw);
      justify-content: space-around;
      display: flex;
      background: ${ThemeBackgroundLighter};
      padding: 5px 0;
      box-sizing: border-box;
      border-top: 1px solid ${ThemeBackgroundDarker};
      height: unset;

      .subscriptions-menu-dropdown {
        top: unset;
        bottom: 60px;
      }

      a {
        &.link {
          margin-right: 0;
          padding: 10px;
        }
      }

      .nav-title {
        display: none;
      }
    }
  }

  .env-tag {
    position: fixed;
    width: 90px;
    text-align: center;
    top: 10px;
    right: -30px;
    transform: rotate(45deg);
    color: white;
    font-size: 10px;
    font-weight: bold;
    z-index: 900;
    text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.28);
    background: ${(props) =>
      props.labs
        ? 'linear-gradient(90deg, #14ff00, #14ff00 30%, #a300ff 61%, #a300ff)'
        : 'linear-gradient(90deg, #28cdff, #a100dd)'};
  }

  #nprogress .bar {
    background: linear-gradient(
      90deg,
      ${ThemeKnockoutRed} 0,
      ${(props) => darken(0.06, ThemeKnockoutRed(props))} 100%
    );
  }
`;

export const HeaderLink = styled(Link)`
  position: relative;
`;
