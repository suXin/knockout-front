import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import HeaderUserLoggedIn from './HeaderUserLoggedIn';
import { loadUserFromStorage } from '../../../services/user';
import { Button, TextButton } from '../../Buttons';

const UserControls = () => {
  const user = loadUserFromStorage();
  const username = user ? user.username : null;
  const { pathname } = useLocation();

  if (user == null) {
    return (
      <Link to="/login">
        <Button>Log in</Button>
      </Link>
    );
  }
  if (username == null) {
    return (
      <div>
        {pathname !== '/usersetup' && (
          <Link to="/usersetup">
            <Button>Create your account</Button>
          </Link>
        )}
        <Link to="/logout">
          <TextButton>Log out</TextButton>
        </Link>
      </div>
    );
  }
  return <HeaderUserLoggedIn user={{ ...user, avatarUrl: user.avatarUrl }} />;
};

export default UserControls;
