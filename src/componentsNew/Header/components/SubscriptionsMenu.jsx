import React, { useRef, useEffect, useState } from 'react';
import styled from 'styled-components';
import { HashLink as Link } from 'react-router-hash-link';
import { useDispatch, useSelector } from 'react-redux';
import dayjs from 'dayjs';
import { getIcon } from '../../../services/icons';
import useDropdownMenu, {
  DropdownMenuButton,
  DropdownMenuEmpty,
  DropdownMenuItem,
  DropdownMenuOpened,
} from './DropdownMenu';
import {
  ThemeFontSizeMedium,
  ThemeFontSizeSmall,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
} from '../../../utils/ThemeNew';
import { getAlerts } from '../../../services/alerts';
import updateSubscriptions from '../../../utils/subscriptions';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../../utils/postOptionsStorage';

const SubscriptionsMenu = () => {
  const menuRef = useRef();
  const buttonRef = useRef();
  const lastUpdated = useRef(dayjs().subtract(2, 'minute'));
  const [open, setOpen] = useDropdownMenu(menuRef, buttonRef);
  const [threads, setThreads] = useState([]);
  const subscriptions = useSelector((state) => state.subscriptions);
  const dispatch = useDispatch();
  const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

  useEffect(() => {
    const setSubscriptions = async () => {
      const { alerts } = await getAlerts(nsfwFilterEnabled);
      updateSubscriptions(dispatch, alerts);
      lastUpdated.current = dayjs();
    };
    const currentTime = dayjs();
    if (open && currentTime.diff(lastUpdated.current, 'seconds') >= 10) {
      setSubscriptions();
    }
  }, [open, dispatch]);

  useEffect(() => {
    const threadList = Object.keys(subscriptions.threads).map((item) => ({
      ...subscriptions.threads[item],
      id: item,
    }));
    setThreads(
      threadList.sort((threadA, threadB) => {
        if (threadA.count > threadB.count) return -1;
        if (threadA.count < threadB.count) return 1;
        return 0;
      })
    );
  }, [subscriptions.threads]);

  const checkRefresh = (thread, event) => {
    const link = `/thread/${thread.id}/${thread.page}`;
    if (link === window.location.pathname) {
      event.preventDefault();
      window.location.reload();
    }
  };

  return (
    <SubscriptionsMenuButton
      className="subscriptions-menu"
      onClick={(e) => {
        if (!e.ctrlKey) setOpen((value) => !value);
      }}
      onKeyDown={(e) => {
        if (e.key === 'Enter') {
          setOpen((value) => !value);
        }
      }}
      tabIndex="0"
      ref={buttonRef}
    >
      <div className="dropdown-menu-button-inner" title="Subscriptions">
        <i className="fas fa-newspaper menu-icon" />
        {threads.length > 0 && (
          <div className="link-notification">{threads.length < 10 ? threads.length : '9+'}</div>
        )}
      </div>
      {open && (
        <DropdownMenuOpened className="subscriptions-menu-dropdown" ref={menuRef}>
          <DropdownMenuItem className="dropdown-menu-header">
            <h1 className="dropdown-menu-header-text">Subscriptions</h1>
          </DropdownMenuItem>
          <div className="dropdown-menu-body">
            {threads.length > 0 ? (
              threads.map((thread) => {
                const icon = getIcon(thread.iconId);
                return (
                  <DropdownThreadItem
                    to={`/thread/${thread.id}/${thread.page}${
                      thread.postId ? `#post-${thread.postId}` : ``
                    }`}
                    onClick={(e) => checkRefresh(thread, e)}
                    key={thread.id}
                  >
                    <DropdownMenuItem>
                      <img className="dropdown-thread-icon" src={icon.url} alt={icon.description} />
                      <div className="dropdown-thread-title" title={thread.title}>
                        {thread.title}
                      </div>
                      <div className="dropdown-thread-count">{thread.count}</div>
                    </DropdownMenuItem>
                  </DropdownThreadItem>
                );
              })
            ) : (
              <DropdownMenuEmpty>
                <i className="fas fa-comments dropdown-menu-empty-icon" />
                <h2 className="dropdown-menu-empty-header">No new posts</h2>
                <div className="dropdown-menu-empty-desc">
                  Subscribe to a thread to be notified when it has new posts.
                </div>
              </DropdownMenuEmpty>
            )}
          </div>
          <Link
            to="/alerts/list"
            className="dropdown-menu-footer"
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                window.location.href = '/alerts/list';
              }
            }}
          >
            View all
          </Link>
        </DropdownMenuOpened>
      )}
    </SubscriptionsMenuButton>
  );
};

export const SubscriptionsMenuButton = styled(DropdownMenuButton)`
  @media (min-width: 900px) {
    order: 5;
    margin-left: auto;
    height: 100%;
    margin-right: 10px;
    padding: 0px 10px;
  }

  @media (max-width: 900px) {
    padding: 10px;
    .menu-icon {
      font-size: ${ThemeFontSizeMedium};
    }
  }
`;

const DropdownThreadItem = styled(Link)`
  .dropdown-thread-icon {
    width: 35px;
    margin-right: ${ThemeHorizontalPadding};
  }

  .dropdown-thread-title {
    font-size: ${ThemeFontSizeMedium};
    font-weight: 600;
    margin-right: ${ThemeHorizontalPadding};
    flex: 1;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    height: calc(${ThemeFontSizeMedium} * 1.1);
  }

  .dropdown-thread-count {
    border-radius: 50%;
    background: ${ThemeHighlightWeaker};
    padding: 6px;
    font-size: ${ThemeFontSizeSmall};
    font-weight: bold;
  }
`;

export default SubscriptionsMenu;
