import { darken, desaturate, lighten } from 'polished';
import { useEffect, useState } from 'react';
import styled from 'styled-components';
import { DesktopMediaQuery, MobileMediaQuery } from '../../SharedStyles';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontFamily,
  ThemeFontSizeMedium,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

export default function useDropdownMenu(menuRef, buttonRef) {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    function handleClickOutside(event) {
      if (
        menuRef.current &&
        !menuRef.current.contains(event.target) &&
        !buttonRef.current.contains(event.target)
      ) {
        setOpen((value) => !value);
      }
    }

    if (open) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      // Unbind the event listener on clean up
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [open, menuRef, buttonRef]);
  return [open, setOpen];
}

export const DropdownMenuButton = styled.div`
  display: flex;
  padding: 0;
  align-items: center;
  justify-content: center;
  position: relative;
  z-index: 50;

  .dropdown-menu-button-inner {
    position: relative;
    cursor: pointer;
    display: flex;
    align-items: center;
  }

  .menu-icon {
    opacity: 0.75;
    font-size: 1.2rem;
    transition: 0.2s;

    &:hover {
      opacity: 1;
    }
  }

  .link-notification {
    font-family: ${ThemeFontFamily};
    top: -8px;
    right: -12px;
    z-index: 51;
  }

  ${DesktopMediaQuery} {
    .dropdown-menu-button-inner:hover {
      filter: brightness(1.1);
    }
  }
`;

export const DropdownMenuItem = styled.div`
  display: flex;
  padding: ${ThemeVerticalPadding} calc(${ThemeHorizontalPadding} * 2);
  box-sizing: border-box;
  color: ${ThemeTextColor};
  font-size: ${ThemeFontSizeMedium};
  align-items: center;

  i {
    min-width: 20px;
  }

  &:hover {
    background-color: ${ThemeBackgroundDarker};
  }
`;

export const DropdownMenuEmpty = styled.div`
  text-align: center;
  padding: 10px calc(${ThemeHorizontalPadding} * 2) 20px calc(${ThemeHorizontalPadding} * 2);

  .dropdown-menu-empty-icon {
    font-size: 2.5rem;
    color: ${(props) => {
      if (props.theme.mode === 'light') {
        return darken(0.08, ThemeBackgroundLighter(props));
      }
      return lighten(0.08, ThemeBackgroundLighter(props));
    }};
  }

  .dropdown-menu-empty-desc {
    line-height: 1.1rem;
  }
`;

export const DropdownMenuOpened = styled.div`
  top: 45px;
  right: 0;
  z-index: 51;
  color: ${ThemeTextColor};

  min-width: 300px;
  max-width: 450px;
  max-height: 650px;
  position: absolute;

  margin-top: ${ThemeVerticalPadding};
  background: ${ThemeBackgroundLighter};
  border: none;
  box-shadow: 0 6px 14px rgba(0, 0, 0, 0.33);

  display: flex;
  flex-direction: column;
  overflow: hidden;

  font-size: ${ThemeFontSizeMedium};

  @media (max-width: 900px) {
    min-width: unset;
    max-width: unset;
    width: calc(100vw - 45px);
    position: fixed;
    left: 50%;
    transform: translateX(-50%);
  }

  .dropdown-menu-header-text {
    margin: 10px 0;
  }

  .dropdown-menu-body {
    max-height: 460px;
    scrollbar-width: thin;
    scrollbar-color: ${(props) => lighten(0.1, ThemeBackgroundLighter(props))}
      ${ThemeBackgroundLighter};
    overflow: auto;

    ${MobileMediaQuery} {
      max-height: 250px;
    }
  }

  .dropdown-menu-footer {
    padding: calc(${ThemeVerticalPadding} * 2) ${ThemeHorizontalPadding};
    text-align: center;
    border-top: 1px solid
      ${(props) => desaturate(0.1, lighten(0.15, ThemeBackgroundLighter(props)))};

    &:hover {
      background-color: ${ThemeBackgroundDarker};
    }
  }

  .dropdown-menu-header {
    pointer-events: none;
  }
`;
