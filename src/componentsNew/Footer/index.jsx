import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

import {
  ThemeFontSizeSmall,
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import StyleableLogo from '../Header/components/StyleableLogo';
import LoggedInOnly from '../LoggedInOnly';

const scrollToTop = () => {
  document.documentElement.scrollTop = 0;
};

const Footer = () => {
  return (
    <StyledFooter>
      <Link to="/">
        <StyleableLogo className="logo" />
      </Link>

      <div className="linkContainer">
        <button onClick={scrollToTop} type="button">
          <i className="fas fa-level-up-alt" />
          &nbsp;Top
        </button>

        <a href="https://icons8.com/" target="_blank" rel="noopener">
          <i className="fas fa-external-link-alt" />
          &nbsp;Icons by Icons8
        </a>

        <LoggedInOnly>
          <Link to="/alerts/list" className="no-mobile no-tablet">
            <i className="fas fa-newspaper" />
            &nbsp;Subscriptions
          </Link>
        </LoggedInOnly>

        <Link to="/rules">
          <i className="fas fa-atlas" />
          &nbsp;Rules
        </Link>

        <Link to="/search" className="no-mobile no-tablet">
          <i className="fas fa-search" />
          &nbsp;Search
        </Link>

        <a href="https://steamcommunity.com/groups/knockoutchat" target="_blank" rel="noopener">
          <i className="fab fa-steam" />
          &nbsp;Steam
        </a>

        <a href="https://discord.gg/wjWpapC" target="_blank" rel="noopener">
          <i className="fab fa-discord" />
          &nbsp;Discord
        </a>

        <a href="https://gitlab.com/knockout-community" target="_blank" rel="noopener">
          <i className="fas fa-code" />
          &nbsp;Source
        </a>

        <a href="mailto:admin@knockout.chat" target="_blank" rel="noopener">
          <i className="fas fa-envelope" />
          &nbsp;Contact
        </a>
      </div>
    </StyledFooter>
  );
};

const StyledFooter = styled.footer`
  position: relative;
  width: 100%;
  bottom: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;

  margin: 0;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalPadding};
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  font-size: ${ThemeFontSizeSmall};
  box-sizing: border-box;

  .logo {
    width: auto;
    height: 40px;
    margin-right: 10px;
    padding: 5px 0;
    position: relative;
    top: 0;
    shape-rendering: geometricprecision;
    transition: transform 300ms ease-in-out;
  }

  .linkContainer {
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    justify-content: space-evenly;
    vertical-align: middle;
    padding-right: 20px;

    a,
    span,
    button {
      display: flex;
      align-items: center;
      color: ${ThemeTextColor};
      text-decoration: none;
      opacity: 0.75;
      transition: opacity 100ms ease-in-out;
      margin-left: 30px;
      cursor: pointer;
      &:hover {
        opacity: 1;
      }
    }

    button {
      background-color: transparent;
      padding: 0;
      display: inherit;
      font-size: inherit;
      font-family: inherit;
      border: none;
      &:focus {
        outline: none;
      }
    }
  }

  i {
    padding-right: 5px;
  }

  @media (max-width: 900px) {
    padding-bottom: 55px;
    margin-top: 0px;
    .no-tablet {
      display: none !important;
    }
  }

  @media (max-width: 700px) {
    margin-top: 0px;
    padding-bottom: 50px;
    flex-direction: column-reverse;
    justify-content: space-evenly;

    .no-mobile {
      display: none !important;
    }

    .linkContainer {
      width: 100%;
      margin-bottom: 15px;
      padding-right: 0px;
      justify-content: center;

      a,
      span {
        margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      }

      button:first-child {
        margin-left: 0px;
      }
    }
  }
`;

export default Footer;
