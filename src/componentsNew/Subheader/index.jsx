import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Pagination from '../Pagination';
import LoggedInOnly from '../LoggedInOnly';
import { buttonHover, MobileMediaQuery, DesktopMediaQuery } from '../SharedStyles';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeTextColor,
} from '../../utils/ThemeNew';

const Subheader = ({
  returnToUrl,
  returnToText,
  totalPaginationItems,
  currentPage,
  pageSize,
  subforumId,
  hasRules,
}) => {
  const paginationPath = `/subforum/${subforumId}/`;
  const newThreadPath = `/thread/new/${subforumId}`;
  const rulesPath = `/subforumRules/${subforumId}`;

  return (
    <StyledSubHeader>
      <span className="back-and-title">
        <div className="left">
          {returnToUrl && returnToText && (
            <Link className="return-btn" to={returnToUrl}>
              <i className="fas fa-angle-left" />
              <span>{returnToText}</span>
            </Link>
          )}
        </div>
      </span>
      <div className="pagination-and-buttons">
        <div className="pagination">
          <Pagination
            showNext
            pagePath={paginationPath}
            totalPosts={totalPaginationItems}
            currentPage={currentPage}
            pageSize={pageSize}
          />
        </div>
        <div className="subheader-menu-wrapper">
          <i className="dropdown-arrow fa fa-angle-down" />
          <div className="subheader-menu-content">
            {hasRules && (
              <div className="subheader-menu-item rules-btn">
                <Link className="subheader-btn" to={rulesPath} title="View rules">
                  <i className="fas fa-atlas">&nbsp;</i>
                  <span>Rules</span>
                </Link>
              </div>
            )}
            <LoggedInOnly>
              <div className="subheader-menu-item">
                <Link
                  className="subheader-btn"
                  to={newThreadPath}
                  title="Create new thread (shortcut: ctrl+enter)"
                >
                  <i className="fas fa-dumpster-fire">&nbsp;</i>
                  <span>Create new thread</span>
                </Link>
              </div>
            </LoggedInOnly>
          </div>
        </div>
      </div>
    </StyledSubHeader>
  );
};

Subheader.propTypes = {
  returnToUrl: PropTypes.string.isRequired,
  returnToText: PropTypes.string.isRequired,
  totalPaginationItems: PropTypes.number,
  currentPage: PropTypes.number,
  pageSize: PropTypes.number,
  subforumId: PropTypes.number,
  hasRules: PropTypes.bool,
};

Subheader.defaultProps = {
  totalPaginationItems: 0,
  currentPage: 1,
  pageSize: 40,
  subforumId: 1,
  hasRules: false,
};

export default Subheader;

const StyledSubHeader = styled.nav`
  max-width: 100vw;
  padding-top: ${ThemeVerticalPadding};
  padding-bottom: ${ThemeVerticalPadding};
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  font-size: ${ThemeFontSizeSmall};
  color: ${ThemeTextColor};

  .return-btn {
    display: block;
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background: ${ThemeBackgroundLighter};

    span {
      margin-left: calc(${ThemeHorizontalPadding} / 2);
      ${MobileMediaQuery} {
        display: none;
      }
    }
  }

  .back-and-title {
    display: flex;
    height: 30px;
    overflow: hidden;
    align-items: stretch;
    margin-right: ${ThemeHorizontalPadding};
    text-decoration: none;
  }

  .pagination-and-buttons {
    display: flex;
    flex-grow: 1;

    .pagination {
      display: flex;
      flex-grow: 1;
      justify-content: flex-end;
    }

    .subheader-menu-wrapper {
      color: ${ThemeTextColor};
      font-size: ${ThemeFontSizeSmall};
      border: none;
      position: relative;
      width: 30px;
      height: 30px;
      margin-left: ${ThemeHorizontalPadding};
      overflow: hidden;
      z-index: 999;

      &:active,
      &:focus,
      &:hover {
        overflow: visible;
      }

      ${DesktopMediaQuery} {
        width: auto;
        overflow: visible !important;
      }

      .dropdown-arrow {
        position: relative;
        display: block;
        width: 100%;
        height: 100%;
        line-height: 27px;
        text-align: center;
        background: ${ThemeBackgroundLighter};
        cursor: pointer;
        z-index: 41;

        ${DesktopMediaQuery} {
          display: none;
        }
      }

      .subheader-menu-content {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        margin-top: 27px;
        box-sizing: border-box;
        box-shadow: 0 0 0 2000px rgba(0, 0, 0, 0.5);
        z-index: 40;
        background: ${ThemeBackgroundLighter};

        ${DesktopMediaQuery} {
          position: relative;
          width: auto;
          padding: 0;
          margin-top: 0;
          background: transparent;
          box-shadow: none;
          display: flex;
          flex-direction: row;
        }

        .rules-btn {
          margin-right: ${ThemeHorizontalPadding};
        }
        .subheader-menu-item {
          position: relative;
          padding: 0;
          background: ${ThemeBackgroundLighter};

          span {
            margin-top: calc(${ThemeHorizontalPadding} / 2);
          }

          ${DesktopMediaQuery} {
            span {
              margin: 0;
            }
          }
        }
      }
    }
  }

  .subheader-btn {
    position: relative;
    display: block;
    margin: 0;
    padding: 0 ${ThemeHorizontalPadding};
    text-align: left;
    line-height: 30px;
    white-space: nowrap;
    border: none;
    height: 100%;

    i {
      padding-left: calc(${ThemeHorizontalPadding} / 2);
      padding-right: calc(${ThemeHorizontalPadding} / 2);
      text-align: center;
    }

    span {
      height: 30px;
    }

    ${buttonHover}
  }
`;
