/* stylelint-disable property-no-vendor-prefix */
import { css } from 'styled-components';
import {
  ThemeBannedUserColor,
  ThemeGoldMemberColor,
  ThemeGoldMemberGlow,
  ThemeModeratorColor,
  ThemeModeratorInTrainingColor,
} from '../../utils/ThemeNew';

export default {
  0: {
    name: 'Banned User',
    color: ThemeBannedUserColor,
    extraStyle: null,
  },
  1: {
    name: 'Member',
    color: '#3facff',
  },
  2: {
    name: 'Gold Member',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      text-shadow: unset;
      -webkit-background-clip: text;
    `,
  },
  3: {
    name: 'Moderator',
    color: ThemeModeratorColor,
    extraStyle: null,
  },
  4: {
    name: 'Admin',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
  5: {
    name: 'Staff',
    color: '#fcbe20',
    extraStyle: css`
      color: transparent;
      filter: ${ThemeGoldMemberGlow};
      background: ${ThemeGoldMemberColor};
      background-clip: text;
      -webkit-background-clip: text;
    `,
  },
  6: {
    name: 'Moderator in Training',
    color: ThemeModeratorInTrainingColor,
    extraStyle: null,
  },
};
