import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

import userRoles from './userRoles';
import { ThemeBannedUserColor } from '../../utils/ThemeNew';

/**
 * @param {{ user: { usergroup: number, isBanned: boolean }, children: React.DOMElement[] }} props
 */
const UserRoleWrapper = (props) => {
  const userGroupId = (props.user && props.user.usergroup) || 1;
  const userRole = userRoles[userGroupId] ? userRoles[userGroupId] : userRoles[1];
  const isBanned = (props.user && props.user.isBanned) || false;
  const title = `${props.user.username} - ${isBanned ? `Banned ${userRole.name}` : userRole.name}`;

  return (
    <StyledUserRoleWrapper
      className={`user-role-wrapper-component ${props.className}`}
      userRole={userRole}
      title={title}
      isBanned={isBanned}
      username={props.user.username}
    >
      {props.children}
    </StyledUserRoleWrapper>
  );
};

export const StyledUserRoleWrapper = styled.span`
  color: ${(props) => props.userRole.color};

  ${(props) => props.userRole.extraStyle};

  ${(props) => props.isBanned && `color: ${ThemeBannedUserColor(props)};`}

  ${(props) => props.username === 'DELETED_USER' && 'color: #999999;'}

  overflow: hidden;
  text-overflow: ellipsis;
`;

UserRoleWrapper.propTypes = {
  user: PropTypes.shape({
    usergroup: PropTypes.number.isRequired,
    username: PropTypes.string.isRequired,
    isBanned: PropTypes.bool,
  }),
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
};
UserRoleWrapper.defaultProps = {
  user: { isBanned: false },
  className: '',
};

export default UserRoleWrapper;
