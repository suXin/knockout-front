import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ReactModal from 'react-modal';
import { SubHeaderButton, Button, TextButton } from '../Buttons';
import {
  ThemeBackgroundDarker,
  ThemeTextColor,
  ThemeFontFamily,
  ThemeFontSizeHuge,
  ThemeKnockoutRed,
} from '../../utils/ThemeNew';
import { TextField } from '../FormControls';

const DURATION = 300;

const Modal = ({
  title,
  cancelText,
  submitText,
  iconUrl,
  submitFn,
  cancelFn,
  children,
  isOpen,
  className,
  modalClassName,
  hideButtons,
  disableSubmit,
  ...props
}) => (
  <ReactModal
    isOpen={isOpen}
    onRequestClose={cancelFn}
    className={modalClassName}
    portalClassName={className}
    bodyOpenClassName="portalOpen"
    closeTimeoutMS={DURATION}
    // eslint-disable-next-line react/jsx-props-no-spreading
    {...props}
  >
    <div className="modal-section header">
      {iconUrl && <img className="modal-icon" src={iconUrl} alt="Background icon" />}
      <h2 className="modal-title">{title}</h2>
    </div>
    <div className="modal-content">{children}</div>
    {!hideButtons && (
      <div className="modal-section footer">
        <TextButton type="button" onClick={cancelFn}>
          {cancelText}
        </TextButton>
        <Button type="button" disabled={disableSubmit} onClick={submitFn}>
          {submitText}
        </Button>
      </div>
    )}
  </ReactModal>
);

Modal.propTypes = {
  title: PropTypes.string.isRequired,
  cancelText: PropTypes.string,
  submitText: PropTypes.string,
  submitFn: PropTypes.func.isRequired,
  cancelFn: PropTypes.func.isRequired,
  iconUrl: PropTypes.string,
  children: PropTypes.node,
  isOpen: PropTypes.bool,
  className: PropTypes.string,
  modalClassName: PropTypes.string,
  hideButtons: PropTypes.bool,
  disableSubmit: PropTypes.bool,
};

Modal.defaultProps = {
  cancelText: 'Cancel',
  submitText: 'Submit',
  iconUrl: undefined,
  children: undefined,
  isOpen: true,
  className: '',
  modalClassName: '',
  hideButtons: false,
  disableSubmit: false,
};

export default styled(Modal).attrs({
  overlayClassName: 'overlay',
  modalClassName: 'modal',
})`
  & .modal {
    position: fixed;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    max-width: 100vw;
    width: 500px;

    display: flex;
    flex-direction: column;
    justify-content: space-between;

    background: ${ThemeBackgroundDarker};
    color: ${ThemeTextColor};
    overflow: hidden;
    opacity: 0;
    transition: opacity ${DURATION / 1000}s;

    z-index: 100;
  }

  & .overlay {
    position: fixed;
    background: rgba(0, 0, 0, 0.8);
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 50;
    opacity: 0;
    transition: opacity ${DURATION / 1000}s;
  }

  & .ReactModal__Overlay--after-open,
  .ReactModal__Content--after-open {
    opacity: 1;
  }

  & .ReactModal__Content--before-close,
  .ReactModal__Overlay--before-close {
    opacity: 0;
  }

  .modal-section {
    display: flex;
    background: ${ThemeBackgroundDarker};
    padding: 15px;
    box-sizing: border-box;
    position: relative;
    align-items: center;

    ${SubHeaderButton} {
      &:first-child {
        margin-left: 0;
      }
    }
  }

  ${TextField}:last-child {
    margin-bottom: 0px;
  }

  .footer {
    display: flex;
    justify-content: flex-end;
  }

  .modal-title {
    font-size: ${ThemeFontSizeHuge};
    color: ${ThemeTextColor};
    margin: 10px 0px;
  }

  .modal-content {
    padding: 15px;
    max-height: 60vh;
    overflow: hidden;
    overflow-y: auto;

    input[type='text'] {
      color: ${ThemeTextColor};
      padding: 5px;
      background: transparent;
      font-size: 16px;
      border-radius: 5px;
      font-family: ${ThemeFontFamily};
      box-sizing: border-box;
      outline: none;

      &:focus {
        border: 1px solid transparent;
        box-shadow: 0 0 0px 2px ${ThemeKnockoutRed};
      }
    }

    a {
      color: #3facff;
    }
  }

  .modal-icon {
    max-height: 35px;
    max-width: 35px;
    margin-right: 5px;
  }
`;
