import config from '../../config';
import { getEventHeaderLogo } from '../utils/eventDates';

// ### Punchy Labs ###
export const loadPunchyLabsFromStorage = () => {
  const punchyLabs = localStorage.getItem('punchyLabs');
  return punchyLabs || 'false';
};
export const setPunchyLabsToStorage = (bool) => {
  localStorage.setItem('punchyLabs', bool);
};
export const loadPunchyLabsFromStorageBoolean = () => loadPunchyLabsFromStorage() === 'true';

// ### Popular Thread Mode ###
export const loadLatestThreadModeFromStorage = () => {
  const mode = localStorage.getItem('show-latest-threads');
  if (!mode) {
    return true;
  }
  return mode === '1';
};
export const setLatestThreadModeToStorage = (show) => {
  localStorage.setItem('show-latest-threads', show ? '1' : '0');
};

// ### Theme ###
export const loadThemeFromStorage = () => {
  const theme = localStorage.getItem('theme');
  if (localStorage.getItem('customColors')) {
    return 'custom';
  }
  if (theme === 'device') {
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light';
  }
  return theme || 'dark';
};

export const setThemeToStorage = (theme) => {
  localStorage.setItem('theme', theme);
};

// ### Custom Theme ###
export const loadCustomThemeFromStorage = () => {
  const customTheme = localStorage.getItem('customTheme');

  return customTheme;
};
export const setCustomThemeToStorage = (customTheme) => {
  localStorage.setItem('customTheme', customTheme);
};
export const deleteCustomThemeFromStorage = () => {
  localStorage.removeItem('customTheme');
};

// ### Scale ###
export const loadScaleFromStorage = () => {
  const scale = localStorage.getItem('scale');
  return scale || 'medium';
};
export const setScaleToStorage = (scale) => {
  localStorage.setItem('scale', scale);
};

// ### Width ###
export const loadWidthFromStorage = () => {
  const width = localStorage.getItem('width');
  return width || 'wide';
};
export const setWidthToStorage = (width) => {
  localStorage.setItem('width', width);
};

// ### AutoSubscribe ###
export const loadAutoSubscribeFromStorage = () => {
  const autoSubscribe = localStorage.getItem('autoSubscribe');
  return autoSubscribe || 'true';
};
export const setAutoSubscribeToStorage = (autoSubscribe) => {
  localStorage.setItem('autoSubscribe', autoSubscribe);
};
export const loadAutoSubscribeFromStorageBoolean = () => loadAutoSubscribeFromStorage() === 'true';

// ### Ratings Xray ###
export const loadRatingsXrayFromStorage = () => {
  const ratingsXray = localStorage.getItem('ratingsXray');
  return ratingsXray || 'false';
};
export const setRatingsXrayToStorage = (ratingsXray) => {
  localStorage.setItem('ratingsXray', ratingsXray);
};
export const loadRatingsXrayFromStorageBoolean = () => loadRatingsXrayFromStorage() === 'true';

// ### Ratings Xray ###
export const loadStickyHeaderFromStorage = () => {
  const stickyHeader = localStorage.getItem('stickyHeader');
  return stickyHeader || 'true';
};
export const setStickyHeaderToStorage = (stickyHeader) => {
  localStorage.setItem('stickyHeader', stickyHeader);
};
export const loadStickyHeaderFromStorageBoolean = () => loadStickyHeaderFromStorage() === 'true';

// ### Thread Ad Display ###
export const loadThreadAdsFromStorage = () => {
  const threadAds = localStorage.getItem('threadAds');
  return threadAds || 'true';
};
export const setThreadAdsToStorage = (threadAds) => {
  localStorage.setItem('threadAds', threadAds);
};
export const loadThreadAdsFromStorageBoolean = () => loadThreadAdsFromStorage() === 'true';

// ### Ratings Display ###
export const loadHideRatingsFromStorage = () => {
  const hideRatings = localStorage.getItem('hideRatings');
  return hideRatings || 'false';
};
export const setHideRatingsToStorage = (hideRatings) => {
  localStorage.setItem('hideRatings', hideRatings);
};
export const loadHideRatingsFromStorageBoolean = () => loadHideRatingsFromStorage() === 'true';

// ### Misc
export const getLogoPath = () => {
  const eventLogo = getEventHeaderLogo();

  if (eventLogo) {
    return eventLogo;
  }

  if (config.qa) {
    return '/static/logo_qa.png';
  }
  const theme = loadThemeFromStorage();

  const logo = {
    light: '/static/logo_dark.svg',
    dark: '/static/logo_new_dark.svg ',
  };
  let logoPath = logo[theme];
  if (logoPath == null) {
    logoPath = logo.dark;
  }

  return logoPath;
};
