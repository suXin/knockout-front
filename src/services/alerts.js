import { authPost, authDelete, authGet } from './common';

export const createAlertRequest = async ({
  threadId,
  lastPostNumber,
  previousLastPostNumber,
  lastSeen,
  previousLastSeen,
}) => {
  if (!threadId || !lastPostNumber) throw Error('Invalid alert');
  if (lastPostNumber > previousLastPostNumber || new Date(lastSeen) > new Date(previousLastSeen)) {
    const requestBody = {
      lastPostNumber,
      lastSeen,
      threadId,
    };

    const response = await authPost({ url: '/alerts', data: requestBody });

    return response;
  }

  return null;
};

export const deleteAlertRequest = async ({ threadId }) => {
  if (!threadId) return { error: 'Invalid thread id' };

  const requestBody = {
    threadId,
  };

  const response = await authDelete({ url: '/alerts', data: requestBody });

  return response;
};

export const getAlerts = async (hideNsfw, page = 1) => {
  const user = localStorage.getItem('currentUser');

  if (!user) {
    return { alerts: [] };
  }
  try {
    const results = await authGet({ url: `/alerts/${page}${hideNsfw ? '?hideNsfw=1' : ''}` });
    return results.data;
  } catch (err) {
    return [];
  }
};
