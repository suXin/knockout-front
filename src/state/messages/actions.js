export const MESSAGES_SET_STATE = 'MESSAGES_SET_STATE';

export function setMessages(value) {
  return {
    type: MESSAGES_SET_STATE,
    value,
  };
}
