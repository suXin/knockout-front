import { MESSAGES_SET_STATE } from './actions';

const initialState = {
  messages: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case MESSAGES_SET_STATE:
      return action.value;
    default:
      break;
  }
  return state;
}
