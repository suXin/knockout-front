import { USER_UPDATE, USER_LOGOUT } from './actions';
import { loadUserFromStorage, setupUserFlags } from '../../services/user';

const localUser = loadUserFromStorage();
let initialState = localUser || {
  username: null,
  avatarUrl: null,
  id: null,
  usergroup: null,
  createdAt: null,
};

initialState = setupUserFlags(initialState);

export default function (state = initialState, action) {
  switch (action.type) {
    case USER_UPDATE:
      return { ...state, ...action.payload };
    case USER_LOGOUT:
      return { loggedOut: true };
    default:
      break;
  }
  return state;
}
