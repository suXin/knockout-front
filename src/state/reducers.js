import { combineReducers } from 'redux';

import { userReducer } from './user';
import { backgroundReducer } from './background';
import { mentionsReducer } from './mentions';
import { subscriptionsReducer } from './subscriptions';
import { styleReducer } from './style';
import { messageReducer } from './messages';

export default combineReducers({
  user: userReducer,
  background: backgroundReducer,
  mentions: mentionsReducer,
  subscriptions: subscriptionsReducer,
  style: styleReducer,
  messages: messageReducer,
});
