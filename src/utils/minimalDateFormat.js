export default (date, useSpaces = true) => {
  if (date === 'a few seconds') {
    return 'now';
  }

  if (date === 'a few seconds ago') {
    return 'just now';
  }

  const dateArray = date.split(' ');
  const termMap = {
    minute: 'm',
    minutes: 'm',
    hour: 'h',
    hours: 'h',
    day: 'd',
    days: 'd',
    month: 'mo',
    months: 'mo',
    year: 'y',
    years: 'y',
    a: '1',
    an: '1',
  };
  const output = dateArray.map((word) => {
    if (word in termMap) {
      return termMap[word];
    }
    return word;
  });
  const hasSuffix = output[output.length - 1] === 'ago';
  if (hasSuffix) output.pop();
  return output.join(useSpaces ? ' ' : '') + (hasSuffix ? ' ago' : '');
};
