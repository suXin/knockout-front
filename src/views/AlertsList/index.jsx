import React, { useState, useEffect } from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet';
import { getAlerts, deleteAlertRequest } from '../../services/alerts';
import AlertsListThreadItem from './components/AlertsListThreadItem';
import BlankSlate from '../../componentsNew/BlankSlate';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeFontSizeHuge,
  ThemeTextColor,
} from '../../utils/ThemeNew';
import { isLoggedIn } from '../../componentsNew/LoggedInOnly';
import { pushNotification } from '../../utils/notification';
import updateSubscriptions from '../../utils/subscriptions';
import ThreadItemPlaceholder from '../../componentsNew/ThreadItemPlaceholder';
import Pagination from '../../componentsNew/Pagination';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../utils/postOptionsStorage';

const AlertsList = () => {
  const {
    params: { page = 1 },
  } = useRouteMatch();
  const [alerts, setAlerts] = useState([]);
  const [totalAlerts, setTotalAlerts] = useState(0);
  const [alertsLoaded, setAlertsLoaded] = useState(false);
  const dispatch = useDispatch();
  const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

  useEffect(() => {
    const getAlertsList = async () => {
      const results = await getAlerts(nsfwFilterEnabled, page);
      setAlerts(results.alerts);
      setTotalAlerts(results.totalAlerts);
      setAlertsLoaded(true);
    };

    getAlertsList();
  }, [page]);

  useEffect(() => {
    updateSubscriptions(dispatch, alerts);
  }, [alerts, dispatch]);

  if (!isLoggedIn()) {
    window.location.replace('/login');
    return null;
  }

  const markUnreadAction = async (id) => {
    try {
      await deleteAlertRequest({ threadId: id });
      const results = await getAlerts(nsfwFilterEnabled, page);
      setAlerts(results.alerts);
      setTotalAlerts(results.totalAlerts);
      pushNotification({ message: 'Successfully unsubscribed to thread.' });
    } catch (err) {
      console.error(err);
      pushNotification({ message: 'Could not unsubscribe to thread.', type: 'error' });
    }
  };

  let alertsContent;

  if (alertsLoaded && totalAlerts === 0) {
    alertsContent = <BlankSlate resourceNamePlural="subscriptions" />;
  } else if (!alertsLoaded) {
    alertsContent = Array(16)
      .fill(1)
      // eslint-disable-next-line react/no-array-index-key
      .map((item, index) => <ThreadItemPlaceholder key={`p${index}`} />);
  } else {
    alertsContent = alerts.map((thread) => {
      const user = {
        avatarUrl: thread.threadUserAvatarUrl,
        username: thread.threadUsername,
        usergroup: thread.threadUserUsergroup,
        id: thread.threadUser,
      };
      return (
        <AlertsListThreadItem
          key={thread.threadId}
          id={thread.threadId}
          createdAt={thread.threadCreatedAt}
          deleted={thread.deleted}
          iconId={thread.threadIcon}
          lastPost={thread.lastPost}
          locked={!!thread.threadLocked}
          postCount={thread.threadPostCount}
          unreadPostCount={thread.unreadPosts}
          title={thread.threadTitle}
          firstUnreadId={thread.firstUnreadId}
          user={user}
          backgroundUrl={thread.threadBackgroundUrl}
          backgroundType={thread.backgroundType}
          markUnreadAction={() => markUnreadAction(thread.threadId)}
        />
      );
    });
  }

  return (
    <StyledAlertsList>
      <Helmet>
        <title>Subscriptions - Knockout!</title>
      </Helmet>
      <h2>Subscriptions</h2>

      <nav className="subHeader">
        <span className="back-and-title">
          <div className="left">
            <Link className="return-btn" to="/">
              <i className="fas fa-angle-left" />
              <span>Home</span>
            </Link>
          </div>
        </span>
        <Pagination
          showNext
          pagePath="/alerts/list/"
          totalPosts={totalAlerts}
          currentPage={page}
          pageSize={20}
        />
      </nav>

      {alertsContent}
    </StyledAlertsList>
  );
};

export default AlertsList;

const StyledAlertsList = styled.div`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h2 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  nav.subHeader {
    max-width: 100vw;
    padding-top: ${ThemeVerticalPadding};
    padding-bottom: ${ThemeVerticalPadding};
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    font-size: ${ThemeFontSizeSmall};
    color: ${ThemeTextColor};

    .return-btn {
      display: block;
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      background: ${ThemeBackgroundLighter};

      span {
        margin-left: calc(${ThemeHorizontalPadding} / 2);
      }
    }

    .back-and-title {
      display: flex;
      height: 30px;
      overflow: hidden;
      align-items: stretch;
      margin-right: ${ThemeHorizontalPadding};
      text-decoration: none;
    }
  }
`;
