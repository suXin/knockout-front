import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { getSubforumWithThreads } from '../../services/subforums';
import { getSubforumRules } from '../../services/rules';
import Rules from '../../componentsNew/Rules';
import StyleWrapper from '../../componentsNew/Rules/components/StyleWrapper';

const SubforumRulesPage = (props) => {
  const {
    match: {
      params: { id: subforumId },
    },
  } = props;

  const [subforumName, setSubforumName] = useState('');

  useEffect(() => {
    const getSubforumName = async () => {
      const subforum = await getSubforumWithThreads(subforumId);
      setSubforumName(subforum.name);
    };

    getSubforumName();
  }, [subforumId]);

  return (
    <StyleWrapper>
      <Rules resource={subforumName} getRules={async () => getSubforumRules(subforumId)} />
    </StyleWrapper>
  );
};

SubforumRulesPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.string.isRequired }).isRequired,
  }).isRequired,
};

export default SubforumRulesPage;
