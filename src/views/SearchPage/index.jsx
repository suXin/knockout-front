import React, { useEffect } from 'react';
import styled from 'styled-components';

import { Helmet } from 'react-helmet';
import {
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeTextColor,
} from '../../utils/ThemeNew';

const SearchPage = () => {
  useEffect(() => {
    const script = document.createElement('script');
    script.src = 'https://cse.google.com/cse.js?cx=005675489929610452028:k9jkao3p-fy';
    script.async = true;

    document.body.appendChild(script);

    return () => {
      document.body.removeChild(script);
    };
  }, []);

  return (
    <StyledSearchPage>
      <Helmet>
        <title>Search - Knockout!</title>
      </Helmet>
      <div className="gcse-search" data-sort_by="date" />
    </StyledSearchPage>
  );
};

export default SearchPage;

const StyledSearchPage = styled.div`
  .gsc-results {
    background-color: ${ThemeBackgroundDarker};
  }

  .gsc-results .gsc-cursor > .gsc-cursor-current-page,
  .gsc-results .gsc-cursor > .gsc-cursor-page {
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    background-color: ${ThemeBackgroundLighter};
    color: ${ThemeTextColor};
  }
  .gsc-cursor-box.gs-bidi-start-align {
    margin-left: 0;
  }

  div.gsc-thumbnail-inside > div {
    line-height: 1.3;
  }

  .gsc-control-cse.gsc-control-cse-en {
    background: transparent;
    border: none;
  }
  .gsc-above-wrapper-area {
    border-bottom: none;
  }

  .gsc-webResult.gsc-result {
    margin-bottom: ${ThemeVerticalPadding};
  }
`;
