import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Helmet } from 'react-helmet';
import { getSubforumRules } from '../../services/rules';
import { getSubforumWithThreads } from '../../services/subforums';
import SubforumThreadItem from './components/SubforumThreadItem';
import BlankSlate from '../../componentsNew/BlankSlate';
import Subheader from '../../componentsNew/Subheader';
import {
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
  ThemeFontSizeHuge,
  ThemeFontSizeSmall,
} from '../../utils/ThemeNew';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../utils/postOptionsStorage';
import Pagination from '../../componentsNew/Pagination';
import ThreadItemPlaceholder from '../../componentsNew/ThreadItemPlaceholder';
import Placeholder from '../../componentsNew/Placeholder';

const Subforum = (props) => {
  const {
    match: {
      params: { id: subforumId, page = 1 },
    },
  } = props;

  const [subforum, setSubforum] = useState({ threads: [] });
  const [hasRules, setHasRules] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    const getData = async () => {
      const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();
      const subforumResult = await getSubforumWithThreads(subforumId, page, nsfwFilterEnabled);

      setSubforum(subforumResult);

      const rules = await getSubforumRules(subforumId);
      setHasRules(rules && rules.length > 0);
      setLoading(false);
    };

    if (subforumId) {
      setLoading(true);
      getData();
    }
  }, [subforumId, page]);

  const { id, threads, currentPage, totalThreads, name } = subforum;

  const handleNewThreadShortcut = (e) => {
    if (e.ctrlKey && e.key === 'Enter') {
      window.location.replace(`/thread/new/${id}`);
    }
  };

  useEffect(() => {
    window.addEventListener('keydown', handleNewThreadShortcut);
    return () => window.removeEventListener('keydown', handleNewThreadShortcut);
  }, [id]);

  let subforumContent;
  if (totalThreads === 0) {
    subforumContent = <BlankSlate resourceNamePlural="threads" />;
  } else if (loading) {
    subforumContent = Array(16)
      .fill(1)
      // eslint-disable-next-line react/no-array-index-key
      .map((item, index) => <ThreadItemPlaceholder key={`p${index}`} />);
  } else {
    subforumContent = threads.map((thread) => {
      return (
        <SubforumThreadItem
          key={thread.id}
          id={thread.id}
          createdAt={thread.createdAt}
          deleted={thread.deleted}
          iconId={thread.iconId}
          lastPost={thread.lastPost}
          locked={thread.locked}
          pinned={thread.pinned}
          postCount={thread.postCount}
          read={thread.read}
          readThreadUnreadPosts={thread.readThreadUnreadPosts}
          title={thread.title}
          unreadPostCount={thread.unreadPostCount}
          user={thread.user}
          firstPostTopRating={thread.firstPostTopRating}
          firstUnreadId={thread.firstUnreadId}
          tags={thread.tags}
          backgroundUrl={thread.backgroundUrl}
          backgroundType={thread.backgroundType}
          viewers={thread.viewers}
          subforumName={name}
        />
      );
    });
  }

  return (
    <StyledSubforumPage>
      {loading ? <Placeholder width={250} textSize="huge" /> : <h1>{name}</h1>}

      <Helmet>
        <title>{name}</title>
      </Helmet>

      {!loading && (
        <div>
          <Subheader
            returnToUrl="/"
            returnToText="Home"
            totalPaginationItems={totalThreads}
            currentPage={currentPage}
            subforumId={id}
            hasRules={hasRules}
            pageSize={40}
          />
        </div>
      )}

      {subforumContent}

      {!loading && (
        <div className="pagination-and-buttons">
          <div className="pagination">
            <Pagination
              showNext
              pagePath={`/subforum/${id}/`}
              totalPosts={totalThreads}
              currentPage={currentPage}
              pageSize={40}
            />
          </div>
        </div>
      )}
    </StyledSubforumPage>
  );
};

Subforum.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.string.isRequired, page: PropTypes.string }).isRequired,
  }).isRequired,
};

export default Subforum;

const StyledSubforumPage = styled.section`
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  h1 {
    margin: 0 0 ${ThemeVerticalPadding} 0;
    font-size: ${ThemeFontSizeHuge};
  }

  .pagination-and-buttons {
    display: flex;
    flex-grow: 1;
    font-size: ${ThemeFontSizeSmall};

    .pagination {
      display: flex;
      flex-grow: 1;
      justify-content: flex-end;
    }
  }

  .rules-btn {
    margin: 0;
  }
`;
