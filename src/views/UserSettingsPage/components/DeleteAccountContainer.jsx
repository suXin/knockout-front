import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { scrollToTop } from '../../../utils/pageScroll';
import { Panel, PanelTitle } from '../../../componentsNew/Panel';

const DeleteAccountContainer = (props) => {
  return (
    <Panel>
      <PanelTitle title="Delete Account">Delete Account</PanelTitle>
      <StyledDeleteAccountButton
        background="#ad1a1a"
        onClick={() => {
          props.showDeleteAccount(true);
          scrollToTop();
        }}
        type="button"
      >
        <i className="fas fa-bomb" />
        Delete Account
      </StyledDeleteAccountButton>
    </Panel>
  );
};

DeleteAccountContainer.propTypes = {
  showDeleteAccount: PropTypes.func,
};

DeleteAccountContainer.defaultProps = {
  showDeleteAccount: null,
};

const StyledDeleteAccountButton = styled.button`
  display: block;
  position: relative;
  background: ${(props) => props.background || 'black'};
  color: ${(props) => props.color || 'white'};
  border: none;
  border-radius: 5px;
  margin: 9px auto;
  padding: 0 0 0 32px;
  font-size: 13px;
  text-align: center;
  width: 230px;
  height: 32px;
  line-height: 32px;
  cursor: pointer;
  overflow: hidden;

  &:active {
    opacity: 0.9;
  }

  i {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 32px;
    line-height: 32px;
    text-align: center;
    background: rgba(0, 0, 0, 0.1);
  }

  span {
    padding: 0 10px;
  }
`;

export default DeleteAccountContainer;
