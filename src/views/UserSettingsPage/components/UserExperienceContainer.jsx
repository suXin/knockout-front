/* eslint-disable jsx-a11y/label-has-for */
import React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import OptionsStorageInput, { StyledOptionsStorageInput } from './OptionsStorageInput';
import ServerSideInput from './ServerSideInput';

import { getProfileRatingsDisplay, updateProfileRatingsDisplay } from '../../../services/user';
import { updateHeader, updateWidth } from '../../../state/style';
import { Panel, PanelTitle } from '../../../componentsNew/Panel';
import { setWidthToStorage } from '../../../services/theme';
import { FieldLabel, FieldLabelSmall } from '../../../componentsNew/FormControls';

export const autoSubscribeKey = 'autoSubscribe';
export const ratingsXrayKey = 'ratingsXray';
export const stickyHeaderKey = 'stickyHeader';
export const punchyLabsKey = 'punchyLabs';
export const displayCountryInfoKey = 'displayCountryInfo';
export const nsfwFilterKey = 'nsfwFilter';
export const threadAdsKey = 'threadAds';
export const hideRatingsKey = 'hideRatings';

const ThemeInputContainer = (props) => {
  const { label, value, options, desc } = props;
  return (
    <StyledOptionsStorageInput>
      <div className="options-info">
        <FieldLabel>{label}</FieldLabel>
        <FieldLabelSmall>{desc}</FieldLabelSmall>
      </div>
      <div className="dropdown">
        <select onChange={(e) => props.update(e.target.value)} defaultValue={value}>
          {options.map((option) => {
            return (
              <option key={option.key} value={option.key}>
                {option.label}
              </option>
            );
          })}
        </select>
      </div>
    </StyledOptionsStorageInput>
  );
};

ThemeInputContainer.propTypes = {
  update: PropTypes.func,
  value: PropTypes.string,
  label: PropTypes.string,
  options: PropTypes.arrayOf(PropTypes.any),
  desc: PropTypes.string,
};

ThemeInputContainer.defaultProps = {
  update: null,
  value: '',
  label: '',
  options: null,
  desc: '',
};

const UserExperienceContainer = () => {
  const width = useSelector((state) => state.style.width);
  const dispatch = useDispatch();

  const setWidth = (widthValue) => {
    dispatch(updateWidth(widthValue));
    setWidthToStorage(widthValue);
  };

  return (
    <Panel>
      <PanelTitle title="Because you're important to us <3">User Experience</PanelTitle>
      <div className="options-wrapper">
        <OptionsStorageInput
          desc="Subscribe to threads on reply / on creation"
          label="AutoSub™"
          storageKey={autoSubscribeKey}
          defaultValue={false}
        />

        <OptionsStorageInput
          desc="Sticky header that's always with you as you scroll"
          label="StickyHeader™"
          storageKey={stickyHeaderKey}
          defaultValue
          onChange={(value) => dispatch(updateHeader(value))}
        />

        <OptionsStorageInput
          desc="Show which country you're posting from"
          label="FlagPunchy™"
          storageKey={displayCountryInfoKey}
          defaultValue
        />

        <OptionsStorageInput
          desc="Experimental and buggy settings"
          label="PunchyLabs™"
          storageKey={punchyLabsKey}
          defaultValue={false}
          onChange={() => window.location.reload()}
        />

        <OptionsStorageInput
          desc="Hides NSFW threads"
          label="WorkSafe™"
          storageKey={nsfwFilterKey}
          defaultValue
        />

        <OptionsStorageInput
          desc="See who left that *dumb* rating"
          label="Ratings X-ray™"
          storageKey={ratingsXrayKey}
          defaultValue
        />

        <ServerSideInput
          desc="Hide ratings displayed on your profile"
          label="BoxHide™"
          setValue={updateProfileRatingsDisplay}
          getValue={getProfileRatingsDisplay}
        />

        <OptionsStorageInput
          desc="Hide all ratings on the site"
          label="BoxHide Deluxe™"
          storageKey={hideRatingsKey}
          defaultValue={false}
        />

        <OptionsStorageInput
          desc="Show short GIF 'thread advertisements' on the front page"
          label="ThreadAds™"
          storageKey={threadAdsKey}
          defaultValue
        />

        <ThemeInputContainer
          label="Width"
          desc="Width of the middle section"
          update={setWidth}
          options={[
            { key: 'full', label: 'Full' },
            { key: 'verywide', label: 'Very Wide' },
            { key: 'wide', label: 'Wide' },
            { key: 'medium', label: 'Medium' },
            { key: 'narrow', label: 'Narrow' },
          ]}
          value={width}
        />
      </div>
    </Panel>
  );
};
export default UserExperienceContainer;
