import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { pushSmartNotification } from '../../../utils/notification';
import { deleteOwnAccount } from '../../../services/user';
import Modal from '../../../componentsNew/Modals/Modal';
import { TextField } from '../../../componentsNew/FormControls';

const DeleteAccountWarning = ({ modalOpen, setModalOpen }) => {
  const [username, setUsername] = useState('');
  const accountUsername = useSelector((state) => state.user.username);

  return (
    <Modal
      title="Delete account"
      iconUrl="static/icons/mushroom-cloud.png"
      submitText="Delete account"
      submitFn={async () => {
        const result = await deleteOwnAccount();
        pushSmartNotification(result);
        setModalOpen(false);
        window.location = '/logout';
      }}
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
      disableSubmit={accountUsername !== username}
    >
      <h2>You are about to delete your account. This action is irreversible.</h2>
      <p>
        Note that as per our Privacy Policy / Terms of Service, some data required for the proper
        functioning of this site will be retained. Feel free to review the&nbsp;
        <a href="/rules/privacy-policy" target="_blank">
          Privacy Policy / Terms of Service
        </a>
        &nbsp;you agreed to.
      </p>

      <p>
        You might also find yourself unable to make new accounts in the future. This is also
        required to prevent abuse of the account deletion system.
      </p>

      <p>
        Please also consider the negative effects account deletion has on the preservation of
        internet history. You can always log out and never log back in again. Only Redditors delete
        their accounts.
      </p>

      <p>
        In order to give you sufficient time to ponder this carefully and prevent any rash
        decisions, please type your username in the field below before pressing the button.
      </p>
      <TextField
        placeholder="Enter your username"
        value={username}
        onChange={(e) => setUsername(e.target.value)}
      />
    </Modal>
  );
};

DeleteAccountWarning.propTypes = {
  modalOpen: PropTypes.bool.isRequired,
  setModalOpen: PropTypes.func.isRequired,
};

export default DeleteAccountWarning;
