/* eslint-disable react/display-name */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* stylelint-disable property-no-vendor-prefix */
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { ThemeHorizontalPadding, ThemeVerticalPadding } from '../../../utils/ThemeNew';
import StyleableLogo from '../../../componentsNew/Header/components/StyleableLogo';

// eslint-disable-next-line no-unused-vars
const ThemePreview = ({ theme }) => {
  return (
    <div className="browser">
      <div className="tabs">
        <div className="tab active">
          <div className="inner">
            <div className="title">Knockout preview!</div>
            <i className="fas fa-times" />
          </div>
        </div>
        <div className="tab">
          <div className="inner">
            <div className="title">eBay - gas masks</div>
            <i className="fas fa-times" />
          </div>
        </div>
      </div>
      <div className="address-bar">
        <div className="inner">
          <span>https://not-real.knockout.chat/theme-preview</span>
        </div>
      </div>
      <div className="content">
        <StyledThemePreview themeValues={theme}>
          <div className="banner">
            <StyleableLogo className="header-logo" />
            <div className="title">knockout!</div>
          </div>
          <div className="thread">
            <div className="thread-header">
              <div className="thread-title">Here&apos;s a Headline</div>
              <button type="button" className="example-button">
                Example button
              </button>
              <a href="" className="example-button active small">
                1
                <i className="fas fa-caret-up" />
              </a>
              <a href="" className="example-button small">
                2
              </a>
              <a href="" className="example-button small">
                Next&nbsp;
                <i className="fas fa-angle-double-right" />
              </a>
            </div>
            <div className="post">
              <div className="user">
                <div className="username">VeryCoolUser</div>
                <div className="join-date">June 1992</div>
              </div>
              <div className="post-body">
                <div className="post-details">a month ago</div>
                <div className="post-content small">
                  Hey here is some post content and it is small text!
                </div>
              </div>
            </div>
            <div className="post">
              <div className="user gold">
                <div className="username">GoldUser</div>
                <div className="join-date">June 1557</div>
              </div>
              <div className="post-body">
                <div className="post-details">a month ago</div>
                <div className="post-content">
                  Kill time thread. This is also an example of the line height in the system,
                  assuming that your browser isn&apos;t displaying this super duper side. Why would
                  you do that anyway? Knockout definitely looks better as a single column in the
                  center of the screen.
                </div>
              </div>
            </div>
            <div className="post">
              <div className="user mod">
                <div className="username">BadMod</div>
                <div className="join-date">December 10 BC</div>
              </div>
              <div className="post-body">
                <div className="post-details">a month ago</div>
                <div className="post-content large">Pissing in a large sea of piss</div>
              </div>
            </div>
            <div className="post">
              <div className="user banned">
                <div className="username">BanMePlease</div>
                <div className="join-date">January 2025</div>
              </div>
              <div className="post-body">
                <div className="post-details">a month ago</div>
                <div className="post-content huge">Some huge text cause im BANNED</div>
              </div>
            </div>
            <div className="post-editor">
              <textarea>Here is some content which you probably should not post</textarea>
            </div>
          </div>
        </StyledThemePreview>
      </div>
    </div>
  );
};

ThemePreview.propTypes = {
  theme: PropTypes.shape({
    backgroundLighter: PropTypes.string.isRequired,
    backgroundDarker: PropTypes.string.isRequired,
    mainBackgroundColor: PropTypes.string.isRequired,
    bodyBackgroundColor: PropTypes.string.isRequired,
    knockoutBrandColor: PropTypes.string.isRequired,
    // knockoutLogo: PropTypes.string.isRequired,
    textColor: PropTypes.string.isRequired,
    themeFontSizeSmall: PropTypes.string.isRequired,
    themeFontSizeMedium: PropTypes.string.isRequired,
    themeFontSizeLarge: PropTypes.string.isRequired,
    themeFontSizeHuge: PropTypes.string.isRequired,
    themeFontFamily: PropTypes.string.isRequired,
    themeEditorFontFamily: PropTypes.string.isRequired,
    themeGoldMemberColor: PropTypes.string.isRequired,
    themeGoldMemberGlow: PropTypes.string.isRequired,
    themeModeratorColor: PropTypes.string.isRequired,
    themeBannedUserColor: PropTypes.string.isRequired,
  }).isRequired,
};

const StyledThemePreview = styled.div`
  background: ${(props) => props.themeValues.mainBackgroundColor};
  color: ${(props) => props.themeValues.textColor};
  padding: 0 1rem;
  * {
    font-family: ${(props) => props.themeValues.themeFontFamily};
  }
  .banner {
    height: 48px;
    display: flex;
    align-items: center;
    padding: 0 ${ThemeHorizontalPadding};
    background: ${(props) => `${props.themeValues.backgroundLighter}`};

    .header-logo {
      height: 36px;
      --svg-light: #ffffff;
      --svg-outline: ${(props) => props.themeValues.textColor};
      --svg-color: ${(props) => props.themeValues.knockoutBrandColor};
      --svg-base: #1f2c39;
    }

    .title {
      font-size: ${(props) => props.themeValues.themeFontSizeHuge};
      font-weight: 600;
      font-style: italic;
      opacity: 0.925;
      font-family: 'Arimo', sans-serif;
    }
  }

  .thread {
    background: ${(props) => props.themeValues.bodyBackgroundColor};

    .thread-title {
      font-size: ${(props) => props.themeValues.themeFontSizeHeadline};
      font-weight: bold;
      margin-bottom: 10px;
    }
    .thread-header {
      padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
      .example-button {
        border: none;
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        margin: 0 calc(${ThemeHorizontalPadding} / 2);
        background: ${(props) => props.themeValues.backgroundDarker};
        color: ${(props) => props.themeValues.textColor};

        &.small {
          font-size: ${(props) => props.themeValues.themeFontSizeSmall};
        }
        &.active {
          position: relative;
          background: ${(props) => props.themeValues.backgroundLighter};

          i {
            color: ${(props) => props.themeValues.knockoutBrandColor};
            position: absolute;
            left: 50%;
            bottom: -5px;
            transform: translateX(-50%);
          }
        }
      }
    }

    .post {
      display: grid;
      grid-template-columns: 200px 1fr;
      margin-bottom: ${ThemeVerticalPadding};
      &:last-of-type {
        margin-bottom: 0px;
      }
      .user {
        height: 100px;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;

        .username {
          color: rgb(63, 172, 255);
          font-size: ${(props) => props.themeValues.themeFontSizeMedium};
          font-weight: bold;
        }
        .join-date {
          font-size: ${(props) => props.themeValues.themeFontSizeSmall};
        }

        &.gold {
          .username {
            color: transparent;
            filter: ${(props) => props.themeValues.themeGoldMemberGlow};
            background: ${(props) => props.themeValues.themeGoldMemberColor};
            background-clip: text;
            -webkit-background-clip: text;
            text-shadow: unset;
          }
        }

        &.mod {
          .username {
            color: ${(props) => props.themeValues.themeModeratorColor};
          }
        }

        &.banned {
          .username {
            color: ${(props) => props.themeValues.themeBannedUserColor};
          }
        }
      }

      .post-body {
        .post-details {
          padding: 0 ${ThemeHorizontalPadding};
          line-height: 30px;
          font-size: ${(props) => props.themeValues.themeFontSizeMedium};
          background: ${(props) => props.themeValues.backgroundLighter};
        }
        .post-content {
          font-size: ${(props) => props.themeValues.themeFontSizeMedium};
          background: ${(props) => props.themeValues.backgroundDarker};
          padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
          min-height: 54px;
          line-height: ${(props) => props.themeValues.postLineHeight}%;

          &.small {
            font-size: ${(props) => props.themeValues.themeFontSizeSmall};
          }

          &.large {
            font-size: ${(props) => props.themeValues.themeFontSizeLarge};
          }

          &.huge {
            font-size: ${(props) => props.themeValues.themeFontSizeHuge};
          }
        }
      }
    }
    .post-editor {
      textarea {
        width: 100%;
        height: 120px;
        border: none;
        border-radius: 0;
        font-family: ${(props) => props.themeValues.themeEditorFontFamily};
        background: ${(props) => props.themeValues.backgroundDarker};
        color: ${(props) => props.themeValues.textColor};
        padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
        font-size: ${(props) => props.themeValues.themeFontSizeMedium};
        line-height: calc(${(props) => props.themeValues.themeFontSizeMedium} * 1.2);
        box-sizing: border-box;
        resize: none;

        &:focus {
          outline: 1px solid ${(props) => props.themeValues.backgroundLighter};
          outline-offset: -1px;
        }
      }
    }
  }
`;

export default ThemePreview;
