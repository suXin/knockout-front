import React from 'react';
import styled from 'styled-components';

const setPunchyLabsNoticeRead = () => {
  localStorage.setItem('punchyLabsNoticeRead', true);
  if (location && location.reload) {
    location.reload();
  }
};

const PassiveAggressiveness = () => (
  <StyledPassiveAggressiveness>
    <p className="title">
      You are currently using PunchyLabs, the experimental / work in progress branch of Knockout.
    </p>
    <p className="second">To continue using it, please click the button below:</p>

    <a
      target="_blank"
      rel="noopener"
      href="https://www.dictionary.com/browse/experiment"
      onClick={setPunchyLabsNoticeRead}
    >
      The Button
    </a>

    <p className="bye">
      If you wish to go back to the non-experimental branch, you may do so from your settings page.
    </p>

    <p className="bye">Thank you for your understanding and for supporting Knockout.</p>
    <img src="/static/logo.svg" alt="for the love of god stop" />
  </StyledPassiveAggressiveness>
);

export default PassiveAggressiveness;

const StyledPassiveAggressiveness = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: black;
  color: white;
  text-align: center;
  flex-direction: column;
  display: flex;
  justify-content: center;
  padding: 0 10px;

  z-index: 999999999;

  p.title {
    font-weight: bold;
  }

  a {
    width: 120px;
    height: 25px;
    padding: 10px 10px;
    background: #da2b2b;
    margin: 0 auto;
    border-radius: 5px;
    line-height: 25px;
    font-weight: bold;
    transition: filter 500ms ease-in-out, box-shadow 500ms ease-in-out;
    filter: brightness(1);
    box-shadow: 0px 0px black;

    &:hover {
      filter: brightness(1.5);
      box-shadow: 0px 0px 15px #ff4a4a;
    }
  }

  img {
    width: 50px;
    margin: 0 auto;
  }
`;
