import { lighten, transparentize } from 'polished';
import React, { useContext } from 'react';
import { Helmet } from 'react-helmet';
import { RedocStandalone } from 'redoc';
import styled, { ThemeContext } from 'styled-components';
import config from '../../../config';
import { MobileMediaQuery } from '../../componentsNew/SharedStyles';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontFamily,
  ThemeHighlightWeaker,
  ThemeTextColor,
} from '../../utils/ThemeNew';

const DocsPage = () => {
  const themeContext = useContext(ThemeContext);
  return (
    <StyledDocsPage>
      <Helmet>
        <title>API Docs - Knockout!</title>
      </Helmet>
      <RedocStandalone
        specUrl={`${config.apiHost}/schema`}
        options={{
          theme: {
            rightPanel: {
              backgroundColor: ThemeBackgroundLighter({ theme: themeContext }),
            },
            colors: {
              text: { primary: ThemeTextColor({ theme: themeContext }) },
              primary: { main: ThemeHighlightWeaker({ theme: themeContext }) },
              http: { get: '#44bd32', post: '#0097e6', put: '#8c7ae6' },
              success: { main: themeContext.mode === 'light' ? '#19ae37' : '#4caa5f' },
            },
            codeBlock: {
              backgroundColor: 'rgb(23, 30, 33)',
            },
            typography: {
              fontFamily: ThemeFontFamily({ theme: themeContext }),
              headings: { fontFamily: ThemeFontFamily({ theme: themeContext }) },
            },
          },
        }}
      />
    </StyledDocsPage>
  );
};

const StyledDocsPage = styled.div`
  background: ${ThemeBackgroundDarker};
  h2 {
    color: ${ThemeTextColor};
  }

  h5 {
    color: ${(props) => transparentize(0.5, ThemeTextColor(props))};
    border-bottom: 1px solid ${(props) => transparentize(0.7, ThemeTextColor(props))};

    span {
      color: ${ThemeTextColor};
    }
  }

  ul > li.react-tabs__tab--selected {
    background: ${(props) => lighten(0.1, ThemeBackgroundLighter(props))};
    border: none;
  }

  ul > li.react-tabs__tab {
    border: none;
  }

  /* Protocol and host in endpoint dropdown */
  .sc-bUrJUP span {
    color: #555;
  }

  h3 {
    color: ${ThemeTextColor};
  }

  /* Nested object definition */
  .sc-dIUggk.iYIfuN {
    background: transparent;
  }

  ${MobileMediaQuery} {
    .redoc-wrap div:nth-child(2) {
      bottom: 54px;
    }
  }
`;
export default DocsPage;
