/* eslint-disable react/forbid-prop-types */
import React, { useState, useRef } from 'react';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import ErrorBoundary from '../../componentsNew/ErrorBoundary';
import LoggedInOnly from '../../componentsNew/LoggedInOnly';
import Post from '../../componentsNew/Post';
import ThreadSubheader from './components/ThreadSubheader';
import PostEditor from '../../componentsNew/PostEditor';
import EditableTitle from './components/EditableTitle';
import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeFontSizeHeadline,
} from '../../utils/ThemeNew';
import Modal from '../../componentsNew/Modals/Modal';
import ModalSelect from '../../componentsNew/Modals/ModalSelect';
import TagModal from './components/TagModal';
import ReportModal from '../../componentsNew/ReportModal';
import { MODERATOR_GROUPS } from '../../utils/userGroups';
import UserGroupRestricted, { usergroupCheck } from '../../componentsNew/UserGroupRestricted';
import { StyledForumIcon } from '../../componentsNew/ForumIcon';
import { StyledPlaceholder } from '../../componentsNew/Placeholder';
import ViewerCount from './components/ViewerCount';
import ThreadViewerModal from './components/ThreadViewerModal';

const ThreadPage = ({
  posts,
  thread,
  params,
  showingMoveModal,
  moveModalOptions,
  currentPage,
  togglePinned,
  toggleLocked,
  toggleDeleted,
  showMoveModal,
  deleteAlert,
  createAlert,
  currentUserId,
  submitThreadMove,
  hideModal,
  refreshPosts,
  goToPost,
  linkedPostId,
}) => {
  const [modalMove, setModalMove] = useState();
  const [reportModalOpen, setReportModalOpen] = useState(false);
  const [viewerModalOpen, setViewerModalOpen] = useState(false);
  const [reportPostId, setReportPostId] = useState(undefined);
  const editorRef = useRef();
  const [tagModal, showTagModal] = useState(false);
  const showNewPostEditor = usergroupCheck(MODERATOR_GROUPS) || (!thread.locked && !thread.deleted);

  return (
    <>
      <Modal
        iconUrl="/static/icons/rearrange.png"
        title="Move thread"
        cancelFn={hideModal}
        submitFn={() => submitThreadMove(modalMove)}
        isOpen={showingMoveModal}
      >
        <ModalSelect
          defaultText="Choose a subforum..."
          options={moveModalOptions}
          onChange={(e) => setModalMove(e.target.value)}
        />
      </Modal>
      <TagModal thread={thread} isOpen={tagModal} openFn={showTagModal} />
      <ReportModal
        postId={reportPostId}
        isOpen={reportModalOpen}
        close={() => setReportModalOpen(false)}
        subforumId={thread.subforumId}
      />
      <StyledThreadPage>
        <Helmet defer={false}>
          <title>
            {thread.title ? `${thread.title} - Knockout!` : 'Knockout - Loading thread...'}
          </title>
        </Helmet>
        <h1 className="thread-title">
          <EditableTitle
            title={thread.title}
            byCurrentUser={currentUserId === thread.user?.id}
            threadId={thread.id}
            iconId={thread.iconId}
          />
        </h1>
        {thread.viewers && (
          <ViewerCount
            id={thread.id}
            viewers={thread.viewers}
            onClick={() => thread.viewers.users && setViewerModalOpen(true)}
          />
        )}
        {thread.viewers?.users?.length > 0 && (
          <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
            <ThreadViewerModal
              viewers={thread.viewers?.users}
              modalOpen={viewerModalOpen}
              setModalOpen={setViewerModalOpen}
            />
          </UserGroupRestricted>
        )}
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
        />
        <article className="thread-page-wrapper">
          <div className="thread-post-list">
            {posts.map((post, index) => {
              const isUnread =
                new Date(thread.subscriptionLastSeen) < new Date(post.createdAt) ||
                new Date(thread.readThreadLastSeen) < new Date(post.createdAt);
              const subforumName = thread.subforumName || 'Subforum';
              const isLinkedPost = linkedPostId && Number(post.id) === Number(linkedPostId);
              const byCurrentUser = currentUserId === post.user.id;

              return (
                <Post
                  key={`${post.id}-${post.updatedAt}`}
                  postDepth={index}
                  totalPosts={posts.length}
                  goToPost={goToPost}
                  username={post.user.username}
                  avatarUrl={post.user.avatarUrl}
                  userJoinDate={post.user.createdAt}
                  threadPage={currentPage}
                  threadId={thread.id}
                  threadLocked={thread.locked}
                  postId={post.id}
                  postBody={post.content}
                  postDate={post.createdAt}
                  postThreadPostNumber={post.threadPostNumber}
                  postPage={currentPage}
                  ratings={post.ratings}
                  user={post.user}
                  byCurrentUser={byCurrentUser}
                  bans={post.bans}
                  subforumName={subforumName}
                  isUnread={isUnread}
                  countryName={post.countryName}
                  countryCode={post.countryCode}
                  isLinkedPost={isLinkedPost}
                  subforumId={thread.subforumId}
                  handleReplyClick={(text) => editorRef.current.appendToContent(text)}
                  handleReportClick={() => {
                    setReportPostId(post.id);
                    setReportModalOpen(true);
                  }}
                  postSubmitFn={refreshPosts}
                />
              );
            })}
          </div>
          {showNewPostEditor && (
            <LoggedInOnly>
              <ErrorBoundary errorMessage="The editor has crashed. Your post was saved a few seconds ago (hopefully). Reload the page.">
                <div className="thread-new-post">
                  <PostEditor
                    threadId={parseInt(params.id, 10)}
                    ref={editorRef}
                    postSubmitFn={refreshPosts}
                  />
                </div>
              </ErrorBoundary>
            </LoggedInOnly>
          )}
        </article>
        <ThreadSubheader
          thread={thread}
          params={params}
          currentPage={currentPage}
          togglePinned={togglePinned}
          toggleDeleted={toggleDeleted}
          toggleLocked={toggleLocked}
          currentUserId={currentUserId}
          showMoveModal={showMoveModal}
          showTagModal={showTagModal}
          createAlert={createAlert}
          deleteAlert={deleteAlert}
        />
      </StyledThreadPage>
    </>
  );
};
ThreadPage.propTypes = {
  posts: PropTypes.array.isRequired,
  thread: PropTypes.object.isRequired,
  params: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  showingMoveModal: PropTypes.bool.isRequired,
  moveModalOptions: PropTypes.array.isRequired,
  currentPage: PropTypes.number.isRequired,
  togglePinned: PropTypes.func.isRequired,
  toggleLocked: PropTypes.func.isRequired,
  toggleDeleted: PropTypes.func.isRequired,
  showMoveModal: PropTypes.func.isRequired,
  deleteAlert: PropTypes.func.isRequired,
  createAlert: PropTypes.func.isRequired,
  currentUserId: PropTypes.number,
  submitThreadMove: PropTypes.func.isRequired,
  hideModal: PropTypes.func.isRequired,
  refreshPosts: PropTypes.func.isRequired,
  goToPost: PropTypes.func.isRequired,
  linkedPostId: PropTypes.number,
};

ThreadPage.defaultProps = {
  currentUserId: undefined,
  linkedPostId: undefined,
};

const StyledThreadPage = styled.div`
  .thread-title {
    margin: calc(${ThemeVerticalPadding} / 2) ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeHeadline};
    line-height: 1.1;
    overflow-wrap: break-word;
    vertical-align: middle;

    ${StyledForumIcon} {
      margin-right: 5px;
      max-height: 35px;
      vertical-align: middle;
    }

    ${StyledPlaceholder} {
      margin-right: 7px;
    }
  }

  .thread-page-wrapper {
    display: block;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    padding-top: 0;
    padding-bottom: ${ThemeVerticalPadding};
    padding-left: ${ThemeHorizontalPadding};
    padding-right: ${ThemeHorizontalPadding};
  }

  .thread-post-list {
    padding: 0;
    margin-top: calc(${ThemeVerticalPadding} / 2);
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
  }
`;
export default ThreadPage;
