import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LoggedInOnly from '../../../componentsNew/LoggedInOnly';
import EditableBackground from './EditableBackground';
import Tooltip from '../../../componentsNew/Tooltip';
import UserGroupRestricted from '../../../componentsNew/UserGroupRestricted';
import { scrollToTop } from '../../../utils/pageScroll';
import {
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
  ThemeFontSizeSmall,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
} from '../../../utils/ThemeNew';
import { DesktopMediaQuery } from '../../../componentsNew/SharedStyles';
import { SubHeaderButton, SubheaderLink } from '../../../componentsNew/Buttons';
import Pagination from '../../../componentsNew/Pagination';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';
import ModerationDropdown from './ModerationDropdown';
import { DropdownMenuItem } from '../../../componentsNew/Header/components/DropdownMenu';

const buttonKeyEvent = (event, func) => {
  if (event.key === 'Enter') {
    func();
  }
};

const ThreadSubheader = ({
  thread,
  params,
  currentPage,
  togglePinned,
  toggleDeleted,
  toggleLocked,
  currentUserId,
  showMoveModal,
  showTagModal,
  createAlert,
  deleteAlert,
}) => (
  <StyledThreadSubheader>
    <SubheaderLink to={`/subforum/${thread.subforumId}`} onClick={scrollToTop}>
      &#8249;
      <span>{(thread.subforum && thread.subforum.name) || 'Subforum'}</span>
    </SubheaderLink>
    <div className="subheader-buttons">
      <div className="subheader-pagination">
        <Pagination
          totalPosts={thread.totalPosts}
          pagePath={`/thread/${params.id}/`}
          currentPage={currentPage}
          showNext
        />
      </div>
      <LoggedInOnly>
        <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
          <ModerationDropdown>
            <DropdownMenuItem
              onClick={togglePinned}
              role="button"
              tabIndex="0"
              onKeyDown={(e) => buttonKeyEvent(e, togglePinned)}
            >
              {thread.pinned ? (
                <i className="fas fa-sticky-note" />
              ) : (
                <i className="far fa-sticky-note" />
              )}
              {thread.pinned ? 'Unpin Thread' : 'Pin Thread'}
            </DropdownMenuItem>
            <DropdownMenuItem
              onClick={toggleLocked}
              role="button"
              tabIndex="0"
              onKeyDown={(e) => buttonKeyEvent(e, toggleLocked)}
            >
              {thread.locked ? <i className="fas fa-lock-open" /> : <i className="fas fa-lock" />}
              {thread.locked ? 'Unlock Thread' : 'Lock Thread'}
            </DropdownMenuItem>
            <DropdownMenuItem
              onClick={toggleDeleted}
              role="button"
              tabIndex="0"
              onKeyDown={(e) => buttonKeyEvent(e, toggleDeleted)}
            >
              {thread.deleted ? (
                <i className="fas fa-trash-restore" />
              ) : (
                <i className="fas fa-trash" />
              )}
              {thread.deleted ? 'Restore Thread' : 'Delete Thread'}
            </DropdownMenuItem>
            <DropdownMenuItem
              onClick={showMoveModal}
              role="button"
              tabIndex="0"
              onKeyDown={(e) => buttonKeyEvent(e, showMoveModal)}
            >
              <i className="fas fa-people-carry" />
              Move thread
            </DropdownMenuItem>
            <DropdownMenuItem
              onClick={showTagModal}
              role="button"
              tabIndex="0"
              onKeyDown={(e) => buttonKeyEvent(e, showTagModal)}
            >
              <i className="fas fa-tag" />
              Edit tags
            </DropdownMenuItem>
          </ModerationDropdown>
        </UserGroupRestricted>
        <div className="subheader-dropdown">
          <i className="subheader-dropdown-arrow fa fa-angle-down" />
          <ul className="subheader-dropdown-list">
            <EditableBackground
              backgroundUrl={thread.threadBackgroundUrl}
              backgroundType={thread.threadBackgroundType}
              byCurrentUser={currentUserId === thread.userId}
              threadId={thread.id}
            />
            {thread.subscribed ? (
              <li className="subheader-dropdown-list-item">
                <Tooltip text="Unsubscribe">
                  <SubHeaderButton onClick={deleteAlert} title="Unsubscribe">
                    <i className="fas fa-eye-slash" />
                    <span>Unsubscribe</span>
                  </SubHeaderButton>
                </Tooltip>
              </li>
            ) : (
              <li className="subheader-dropdown-list-item">
                <Tooltip text="Subscribe">
                  <SubHeaderButton onClick={createAlert} title="Subscribe">
                    <i className="fas fa-eye" />
                    <span>Subscribe</span>
                  </SubHeaderButton>
                </Tooltip>
              </li>
            )}
          </ul>
        </div>
      </LoggedInOnly>
    </div>
  </StyledThreadSubheader>
);

ThreadSubheader.propTypes = {
  thread: PropTypes.shape({
    id: PropTypes.number,
    subforumId: PropTypes.number,
    subforum: PropTypes.shape({
      name: PropTypes.string,
    }),
    totalPosts: PropTypes.number,
    pinned: PropTypes.bool,
    deleted: PropTypes.bool,
    locked: PropTypes.bool,
    threadBackgroundUrl: PropTypes.string,
    threadBackgroundType: PropTypes.string,
    userId: PropTypes.number,
    subscribed: PropTypes.bool,
  }).isRequired,
  params: PropTypes.shape({
    id: PropTypes.string,
  }).isRequired,
  currentPage: PropTypes.number.isRequired,
  togglePinned: PropTypes.func.isRequired,
  toggleLocked: PropTypes.func.isRequired,
  toggleDeleted: PropTypes.func.isRequired,
  showMoveModal: PropTypes.func.isRequired,
  showTagModal: PropTypes.func.isRequired,
  deleteAlert: PropTypes.func.isRequired,
  createAlert: PropTypes.func.isRequired,
  currentUserId: PropTypes.number.isRequired,
};

export const StyledThreadSubheader = styled.header`
  box-sizing: border-box;
  max-width: 100vw;
  padding-top: calc(${ThemeVerticalPadding} / 2);
  padding-bottom: calc(${ThemeVerticalPadding} / 4);
  padding-left: ${ThemeHorizontalPadding};
  padding-right: ${ThemeHorizontalPadding};
  display: flex;
  flex-direction: row;

  .subheader-buttons {
    display: flex;
    flex-grow: 1;
  }

  .subheader-pagination {
    display: flex;
    justify-content: flex-end;
    flex-grow: 1;
    font-size: ${ThemeFontSizeSmall};
  }

  .subheader-dropdown {
    position: relative;
    width: 30px;
    height: 30px;
    margin-left: ${ThemeHorizontalPadding};
    overflow: hidden;

    &:active,
    &:focus,
    &:hover {
      overflow: visible;
    }

    ${DesktopMediaQuery} {
      width: auto;
      margin-left: 0;
      overflow: visible !important;
    }
  }

  .subheader-dropdown-arrow {
    position: relative;
    display: block;
    width: 100%;
    height: 30px;
    line-height: 30px;
    text-align: center;
    font-size: ${ThemeFontSizeMedium};
    color: ${ThemeTextColor};
    background: ${ThemeBackgroundLighter};
    cursor: pointer;
    z-index: 41;

    ${DesktopMediaQuery} {
      display: none;
    }
  }

  .subheader-dropdown-list {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    list-style: none;
    padding: 32px 0 0 0;
    background: ${ThemeBackgroundLighter};
    z-index: 40;

    ${DesktopMediaQuery} {
      position: relative;
      width: auto;
      padding: 0;
      background: transparent;
      box-shadow: none;
      display: flex;
      flex-direction: row;
    }
  }

  .subheader-dropdown-list-item {
    display: block;
    position: relative;
    list-style: none;
    padding: 0;
    margin: calc(${ThemeVerticalPadding} / 2) calc(${ThemeVerticalPadding} / 2);

    ${DesktopMediaQuery} {
      margin: 0;
      margin-left: ${ThemeHorizontalPadding};
    }
  }
`;

export default ThreadSubheader;
