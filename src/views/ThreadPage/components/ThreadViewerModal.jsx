import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import MiniUserInfo from '../../../componentsNew/MiniUserInfo';
import Modal from '../../../componentsNew/Modals/Modal';
import { UserResults } from '../../MessagesPage/components/UserSearchModal';

const ThreadViewerModal = ({ viewers, setModalOpen, modalOpen }) => (
  <Modal
    title="Reading now"
    submitFn={() => {
      setModalOpen(false);
    }}
    cancelFn={() => setModalOpen(false)}
    isOpen={modalOpen}
    hideButtons
  >
    <UserResults>
      {viewers.map((viewer) => (
        <Link
          key={viewer.id}
          to={`/user/${viewer.id}`}
          className="result"
          role="button"
          tabIndex="0"
        >
          <MiniUserInfo as="div" user={viewer} defaultAvatar />
        </Link>
      ))}
    </UserResults>
  </Modal>
);

ThreadViewerModal.propTypes = {
  viewers: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
    })
  ).isRequired,
  setModalOpen: PropTypes.func.isRequired,
  modalOpen: PropTypes.bool.isRequired,
};

export default ThreadViewerModal;
