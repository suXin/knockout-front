import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import LoggedInOnly from '../../../componentsNew/LoggedInOnly';
import { usergroupCheck } from '../../../componentsNew/UserGroupRestricted';
import { updateThread } from '../../../services/threads';
import { pushSmartNotification, pushNotification } from '../../../utils/notification';
import Tooltip from '../../../componentsNew/Tooltip';
import ForumIcon from '../../../componentsNew/ForumIcon';
import {
  ThemeTextColor,
  ThemeHorizontalPadding,
  ThemeBackgroundDarker,
  ThemeFontSizeHeadline,
  ThemeFontFamily,
} from '../../../utils/ThemeNew';
import { Button, SubHeaderButton, TextButton } from '../../../componentsNew/Buttons';
import { MODERATOR_GROUPS } from '../../../utils/userGroups';
import Placeholder from '../../../componentsNew/Placeholder';

const EditableTitle = ({ title, iconId, threadId, byCurrentUser }) => {
  const [currentTitle, setCurrentTitle] = useState('Thread');
  const [isEditing, setIsEditing] = useState(false);
  const [titleInput, setTitleInput] = useState('');

  const titleField = useRef();

  useEffect(() => {
    setCurrentTitle(title);
  }, [title]);

  useEffect(() => {
    if (isEditing && titleField.current) {
      titleField.current.focus();
    }
  }, [isEditing]);

  const edit = () => {
    setIsEditing((value) => !value);
    setTitleInput(currentTitle);
  };

  const save = async () => {
    try {
      if (titleInput.length > 140) {
        pushSmartNotification({ error: 'Title is too long. Must be 140 characters or less.' });
        throw new Error({ error: 'Title too long' });
      }

      await updateThread(threadId, { title: titleInput });

      pushNotification({ message: 'Thread title updated.' });
      setCurrentTitle(titleInput);
    } catch (err) {
      pushNotification({ message: 'An error occured.' });
    }
    setIsEditing(false);
  };

  let titleDisplay = <Placeholder textSize="headline" width={630} marginBottom={0} />;
  if (title) titleDisplay = <span>{currentTitle}</span>;

  return (
    <StyledEditableTitle>
      {title && <ForumIcon iconId={iconId} />}
      {isEditing ? (
        <input
          className="thread-page-title-input"
          title="Title edit input"
          ref={titleField}
          value={titleInput}
          onChange={(e) => setTitleInput(e.target.value)}
        />
      ) : (
        titleDisplay
      )}
      <LoggedInOnly>
        {(usergroupCheck(MODERATOR_GROUPS) || byCurrentUser) &&
          (isEditing ? (
            <span>
              <div className="thread-page-title-button">
                <TextButton onClick={() => edit()}>Cancel</TextButton>
              </div>
              <div className="thread-page-title-button">
                <Button onClick={() => save()}>Save</Button>
              </div>
            </span>
          ) : (
            <div className="thread-page-title-button">
              <Tooltip text="Edit title" top={false}>
                <SubHeaderButton
                  title="Edit title"
                  className="title-edit-button"
                  onClick={() => edit()}
                >
                  <i className="fas fa-pencil-alt" />
                  <span>Rename</span>
                </SubHeaderButton>
              </Tooltip>
            </div>
          ))}
      </LoggedInOnly>
    </StyledEditableTitle>
  );
};

EditableTitle.propTypes = {
  byCurrentUser: PropTypes.bool,
  threadId: PropTypes.number,
  title: PropTypes.string,
  iconId: PropTypes.number,
};
EditableTitle.defaultProps = {
  byCurrentUser: false,
  threadId: 0,
  iconId: 0,
  title: '',
};

const StyledEditableTitle = styled.div`
  display: flex;
  align-items: center;
  color: ${ThemeTextColor};
  overflow-wrap: anywhere;

  .thread-page-title-button {
    display: inline-block;
    font-weight: normal;
    margin-left: 9px;
  }

  .thread-page-title-input {
    padding: 1px ${ThemeHorizontalPadding};
    color: ${ThemeTextColor};
    font-size: ${ThemeFontSizeHeadline};
    font-family: ${ThemeFontFamily};
    font-weight: 700;
    line-height: 1.1;
    overflow-wrap: break-word;
    vertical-align: middle;
    border: none;
    background: ${ThemeBackgroundDarker};
    flex-grow: 1;
  }
`;

export default EditableTitle;
