import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { getTags, updateThreadTags } from '../../../services/tags';
import { pushSmartNotification } from '../../../utils/notification';
import Modal from '../../../componentsNew/Modals/Modal';
import ModalSelect from '../../../componentsNew/Modals/ModalSelect';
import { Tag } from '../../../componentsNew/Buttons';
import { FieldLabelSmall } from '../../../componentsNew/FormControls';

const TagModal = ({ thread, isOpen, openFn }) => {
  const [availableTags, setAvailableTags] = useState([]);
  const [selectedTags, setSelectedTags] = useState([]);
  const [tagsMap, setTagsMap] = useState({});

  const defaultText = 'Select a tag...';

  useEffect(() => {
    const retrieveTags = async () => {
      const newMap = {};
      const tags = await getTags();
      setAvailableTags(
        tags.map((tag) => ({
          value: tag.id,
          text: tag.name,
        }))
      );
      tags.forEach((tag) => {
        newMap[tag.id] = tag.name;
      });
      setTagsMap(newMap);
    };

    if (isOpen && availableTags.length === 0) {
      retrieveTags();
      setSelectedTags(thread.tags.map((item) => Object.keys(item)[0]));
    }
  }, [isOpen]);

  const handleTagAdd = (e) => {
    if (selectedTags.length === 3) {
      return;
    }
    const { value } = e.target;
    if (value !== defaultText) {
      setSelectedTags([...selectedTags, Number(value)]);
    }
  };

  const handleTagRemove = (index) => {
    const newTags = [...selectedTags];
    newTags.splice(index, 1);

    setSelectedTags(newTags);
  };

  const handleSubmit = async () => {
    try {
      await updateThreadTags(thread.id, {
        tag_ids: selectedTags,
      });
      pushSmartNotification({ message: 'Tags updated.' });
      openFn(false);
    } catch (error) {
      pushSmartNotification({ error: 'Could not update tags.' });
    }
  };

  return (
    <Modal
      iconUrl="/static/icons/tag.png"
      title="Edit tags"
      cancelFn={() => openFn(false)}
      submitFn={handleSubmit}
      isOpen={isOpen}
    >
      <ModalSelect
        defaultText={defaultText}
        options={availableTags}
        selectedOptions={selectedTags}
        onChange={handleTagAdd}
      />
      <FieldLabelSmall>Selected tags</FieldLabelSmall>
      <div className="selected-tags">
        {selectedTags.map((tag, index) => (
          <Tag
            key={tag}
            type="button"
            className="tag-item"
            data-testid="selected-tag"
            onClick={() => handleTagRemove(index)}
          >
            <i className="fas fa-times-circle" />
            &nbsp;
            {tagsMap[tag]}
          </Tag>
        ))}
      </div>
    </Modal>
  );
};

export default TagModal;

TagModal.propTypes = {
  thread: PropTypes.shape({
    id: PropTypes.number,
    tags: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  isOpen: PropTypes.bool.isRequired,
  openFn: PropTypes.func.isRequired,
};
