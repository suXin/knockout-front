import styled from 'styled-components';

import {
  ThemeVerticalPadding,
  ThemeHorizontalPadding,
  ThemeFontSizeMedium,
  ThemeBackgroundLighter,
  ThemeBackgroundDarker,
  ThemeTextColor,
  ThemeKnockoutRed,
  ThemeFontSizeHuge,
} from '../../utils/ThemeNew';

export default styled.article`
  width: 100%;
  margin: ${ThemeVerticalPadding} auto 0 auto;
  display: block;
  padding: 0 ${ThemeHorizontalPadding};
  box-sizing: border-box;

  h1 {
    margin: 0;
    font-size: ${ThemeFontSizeHuge};
  }

  .thread-creation-notice {
    margin-top: ${ThemeVerticalPadding};
    padding: ${ThemeVerticalPadding} 0 calc(${ThemeVerticalPadding} * 2) 0;
    text-align: center;
    background-color: ${ThemeKnockoutRed};
    color: ${ThemeTextColor};
    font-weight: bold;
    border: dashed;
    border-width: 5px;

    h2 {
      text-transform: uppercase;
    }

    p {
      max-width: 50%;
      margin: 0 auto;
      line-height: 150%;
    }

    p.lead {
      font-size: 1.2rem;
    }

    a {
      text-decoration: underline;
    }

    ul {
      padding: 0;
      list-style-type: none;

      li {
        margin-top: ${ThemeVerticalPadding};
      }
    }
  }

  .line-option-wrapper {
    display: flex;
    .input {
      flex: 1;

      &:first-child {
        margin-right: 5px;
      }
    }
    .input.tall {
      flex: 1;
      flex-shrink: 1;
    }

    @media (max-width: 960px) {
      display: initial;

      .input {
        &:first-child {
          margin-right: unset;
        }
      }
    }
  }

  .tooltip-content {
    display: flex;
  }

  .input {
    position: relative;
    display: flex;
    flex-direction: row;
    min-height: 32px;
    line-height: 32px;
    margin: ${ThemeVerticalPadding} 0 0;
    color: ${ThemeTextColor};
    background: ${ThemeBackgroundDarker};
  }

  .input.tall {
    @media (max-width: 700px) {
      flex-direction: column;
    }
  }

  .input .input-label {
    flex: 1;
    display: flex;
    span {
      padding: 0 10px;
      min-width: 75px;
      flex-grow: 0;
      text-align: left;
      font-size: ${ThemeFontSizeMedium};
      background: ${ThemeBackgroundLighter};
    }
  }

  .input input {
    flex-grow: 1;
    border: none;
    margin: 0 0 0 ${ThemeHorizontalPadding};
    padding: 0;
    height: 32px;
    line-height: 32px;
    outline: none;
    font-size: ${ThemeFontSizeMedium};
    background: transparent;
    color: ${ThemeTextColor};
  }

  .editor {
    textarea {
      min-height: 100px;
    }
  }

  .selected-tags .tag-item {
    padding: 2px 6px;
    margin: 0 5px;
    background: ${ThemeKnockoutRed};
    border: none;
    border-radius: 10px;
    color: white;
    cursor: pointer;

    &:hover {
      filter: brightness(1.25);
    }
  }

  .tags-list {
    select {
      width: 100%;
      border: none;
      margin: 0 0 0 10px;
      padding: 0;
      line-height: 1;
      outline: none;
      font-size: ${ThemeFontSizeMedium};
      color: inherit;
      border-radius: 0 0 5px 5px;
      background: ${ThemeBackgroundDarker};

      @media (max-width: 960px) {
        margin: 0;
        min-height: 32px;
        padding-left: 5px;
      }
    }
  }

  .background-type-input {
    display: inline-block;
    .background-type-input-inner {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      font-size: 16px;
    }
    label {
      margin-left: 15px;
      background: none;
      font-size: 18px;
      border-radius: unset;
      cursor: pointer;
      &.active {
        color: ${ThemeKnockoutRed};
      }
      &:hover&:not(.active) {
        color: lighten(${ThemeKnockoutRed}, 2%);
      }
    }
  }

  .editor {
    margin-top: ${ThemeVerticalPadding};
  }
`;
