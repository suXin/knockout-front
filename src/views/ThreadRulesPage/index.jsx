import React from 'react';
import PropTypes from 'prop-types';
import { getThreadRules } from '../../services/rules';
import Rules from '../../componentsNew/Rules';
import StyleWrapper from '../../componentsNew/Rules/components/StyleWrapper';

const ThreadRulesPage = (props) => {
  const {
    match: {
      params: { id: threadId },
    },
  } = props;

  return (
    <StyleWrapper>
      <Rules resource="Thread" getRules={async () => getThreadRules(threadId)} />
    </StyleWrapper>
  );
};

ThreadRulesPage.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({ id: PropTypes.string.isRequired }).isRequired,
  }).isRequired,
};

export default ThreadRulesPage;
