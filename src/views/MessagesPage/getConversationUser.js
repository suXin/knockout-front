export default (users, currentUser) => users.filter((item) => item.id !== currentUser.id)[0];
