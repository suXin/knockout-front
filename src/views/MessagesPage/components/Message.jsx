import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import UserAvatar from '../../../componentsNew/Avatar';
import UserRoleWrapper from '../../../componentsNew/UserRoleWrapper';
import minimalDateFormat from '../../../utils/minimalDateFormat';
import {
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import getLighterColor from '../getLighterColor';

dayjs.extend(relativeTime);

const Message = ({ user, createdAt, readAt, content, self, child }) => (
  <StyledMessage
    self={self}
    child={child}
    title={new Intl.DateTimeFormat(undefined, {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
    }).format(new Date(createdAt))}
  >
    {!child && (
      <div className="info">
        <UserRoleWrapper className="user" user={user}>
          {user.username}
        </UserRoleWrapper>
        <span className="time">{minimalDateFormat(dayjs(createdAt).fromNow(true), false)}</span>
      </div>
    )}
    <div className="body">
      {!child && (
        <UserAvatar className="avatar" src={user.avatarUrl} alt={`${user.username}'s Avatar`} />
      )}
      <div className="content">{content}</div>
    </div>
    {readAt && (
      <div className="read-time" data-testid="read-message">
        {`Seen ${minimalDateFormat(dayjs(readAt).fromNow(), false)}`}
      </div>
    )}
  </StyledMessage>
);

Message.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    avatarUrl: PropTypes.string.isRequired,
  }).isRequired,
  createdAt: PropTypes.string.isRequired,
  readAt: PropTypes.string,
  content: PropTypes.string.isRequired,
  self: PropTypes.bool,
  child: PropTypes.bool,
};

Message.defaultProps = {
  child: false,
  self: false,
  readAt: '',
};

const StyledMessage = styled.div`
  margin-bottom: 10px;
  ${(props) => !props.child && 'margin-top: 30px;'}

  .info {
    margin-bottom: 15px;
    padding: 0 53px;
    display: flex;
    ${(props) => props.self && 'flex-direction: row-reverse;'}
  }

  .user {
    font-weight: bold;
    margin-right: 5px;
  }

  .time {
    opacity: 0.66;
    margin: 0 5px;
  }

  .body,
  .read-time {
    display: flex;
    ${(props) => props.self && 'flex-direction: row-reverse;'}
  }

  .read-time {
    margin: 0 55px;
    font-size: ${ThemeFontSizeSmall};
    opacity: 0.66;
    margin-top: 10px;
  }

  .content {
    border: ${(props) => (props.self ? 'none' : `1px solid ${getLighterColor(props)}`)};
    background: ${(props) => (props.self ? ThemeBackgroundLighter : 'none')};
    padding: calc(${ThemeVerticalPadding} * 1.5);
    margin: 0 ${(props) => (props.child ? '55px' : '10px')};
    line-height: normal;
    max-width: 65%;
    white-space: pre-line;
    overflow-wrap: break-word;
  }

  .avatar {
    max-height: 45px;
    height: 45px;
  }
`;
export default Message;
