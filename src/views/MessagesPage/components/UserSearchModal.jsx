import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { TextField } from '../../../componentsNew/FormControls';
import MiniUserInfo from '../../../componentsNew/MiniUserInfo';
import Modal from '../../../componentsNew/Modals/Modal';
import { searchUsers } from '../../../services/user';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeHorizontalPadding,
} from '../../../utils/ThemeNew';

const UserSearchModal = ({ modalOpen, setModalOpen, selectUser }) => {
  const [query, setQuery] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const timer = useRef();
  const textField = useRef();

  useEffect(() => {
    clearTimeout(timer.current);
    if (query) {
      timer.current = setTimeout(async () => {
        const results = await searchUsers(query);
        setSearchResults(results);
      }, 600);
    } else {
      setSearchResults([]);
    }
  }, [query]);

  useEffect(() => {
    if (modalOpen) {
      setQuery('');
      setTimeout(() => {
        if (textField.current) {
          textField.current.focus();
        }
      }, 100);
    }
  }, [modalOpen]);

  return (
    <Modal
      title="New message"
      submitText="Create message"
      submitFn={() => {
        setModalOpen(false);
      }}
      cancelFn={() => setModalOpen(false)}
      isOpen={modalOpen}
      hideButtons
    >
      <TextField
        placeholder="Search for users"
        value={query}
        onChange={(e) => setQuery(e.target.value)}
        ref={textField}
      />
      <UserResults>
        {searchResults.map((result) => (
          <div
            key={result.id}
            className="result"
            role="button"
            tabIndex="0"
            onClick={() => {
              selectUser(result);
              setModalOpen(false);
            }}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                selectUser(result);
                setModalOpen(false);
              }
            }}
          >
            <MiniUserInfo as="div" user={result} defaultAvatar />
          </div>
        ))}
      </UserResults>
    </Modal>
  );
};

export const UserResults = styled.div`
  max-height: 200px;
  overflow: auto;

  .result {
    display: flex;
    align-items: center;
    height: 40px;
    box-sizing: border-box;
    padding: 0 calc(${ThemeHorizontalPadding} * 1.5);
    cursor: pointer;

    &:hover {
      background: ${ThemeBackgroundLighter};
    }
  }

  scrollbar-width: thin;
  scrollbar-color: ${ThemeBackgroundLighter} transparent;
  &::-webkit-scrollbar-track {
    background-color: ${ThemeBackgroundDarker};
  }

  &::-webkit-scrollbar {
    width: 6px;
    height: 10px;
    background-color: ${ThemeBackgroundLighter};
  }

  &::-webkit-scrollbar-thumb {
    opacity: 0.5;
    background-color: ${ThemeBackgroundLighter};
  }
`;

UserSearchModal.propTypes = {
  modalOpen: PropTypes.bool.isRequired,
  setModalOpen: PropTypes.func.isRequired,
  selectUser: PropTypes.func.isRequired,
};

export default UserSearchModal;
