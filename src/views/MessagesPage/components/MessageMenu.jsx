import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import { transparentize } from 'polished';
import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import UserAvatar from '../../../componentsNew/Avatar';
import { Button, TextButton } from '../../../componentsNew/Buttons';
import UserRoleWrapper from '../../../componentsNew/UserRoleWrapper';
import minimalDateFormat from '../../../utils/minimalDateFormat';
import {
  ThemeFontSizeMedium,
  ThemeHighlightWeaker,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import getConversationUser from '../getConversationUser';
import getLighterColor from '../getLighterColor';
import { getUser } from '../../../services/user';

dayjs.extend(relativeTime);

const MessageMenu = ({
  getData,
  setModalOpen,
  conversations,
  currentConversation,
  loaded,
  startConversation,
}) => {
  const currentUser = useSelector((state) => state.user);
  const history = useHistory();
  const match = useRouteMatch();

  const fetchAndCreateConversation = async (userId) => {
    const user = await getUser(userId);
    startConversation(user);
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    if (match.params.user && loaded) {
      fetchAndCreateConversation(match.params.user);
    }
  }, [loaded]);

  return (
    <StyledMessageMenu className="column">
      <div className="messages-header">
        <span className="messages-title">Messages</span>
        <TextButton className="header-button" onClick={() => setModalOpen(true)}>
          <i className="header-icon fas fa-plus" />
        </TextButton>
      </div>
      <div className="messages-body">
        {conversations.length === 0 && (
          <div className="empty-message">
            <div className="empty-title">No messages</div>
            <div className="empty-desc">
              Conversations between you and other users will appear here.
            </div>
            <Button onClick={() => setModalOpen(true)}>Start a conversation</Button>
          </div>
        )}
        {conversations.map((conversation) => {
          const user = getConversationUser(conversation.users, currentUser);
          const unread =
            conversation.messages?.[0]?.user.id !== currentUser.id &&
            conversation.messages?.[0]?.readAt === null;
          let message = conversation.messages?.[0]?.content;
          if (message && conversation.messages[0].user.id === currentUser.id) {
            message = `You: ${message}`;
          }

          const selectConversation = () =>
            history.push(
              `/${match.url.split('/')[1]}/${conversation.id < 0 ? 'new' : ''}${conversation.id}`
            );
          return (
            <ConversationItem
              key={conversation.id}
              selected={currentConversation?.id === conversation.id}
              onClick={selectConversation}
              onKeyDown={(e) => {
                if (e.key === 'Enter') {
                  selectConversation();
                }
              }}
              unread={unread}
              tabIndex="0"
            >
              {unread && <div className="unread-icon" title="Unread" />}
              <UserAvatar className="avatar" src={user.avatarUrl} />
              <div className="details">
                <div className="info">
                  <UserRoleWrapper user={user} className="user">
                    {user.username}
                  </UserRoleWrapper>
                  <div className="time">
                    {minimalDateFormat(dayjs(conversation.updatedAt).fromNow(true), false)}
                  </div>
                </div>
                <div className="content">
                  {message}
                  {message === undefined && <i className="new-message">New message</i>}
                </div>
              </div>
            </ConversationItem>
          );
        })}
      </div>
    </StyledMessageMenu>
  );
};

MessageMenu.propTypes = {
  getData: PropTypes.func.isRequired,
  setModalOpen: PropTypes.func.isRequired,
  conversations: PropTypes.arrayOf(PropTypes.object).isRequired,
  currentConversation: PropTypes.shape({
    id: PropTypes.number.isRequired,
  }),
  loaded: PropTypes.bool.isRequired,
  startConversation: PropTypes.func.isRequired,
};

MessageMenu.defaultProps = {
  currentConversation: undefined,
};

const StyledMessageMenu = styled.div`
  .messages-header {
    justify-content: space-between;
  }
`;

const ConversationItem = styled.div`
  display: flex;
  align-items: center;
  padding: calc(${ThemeVerticalPadding} * 2.5) calc(${ThemeHorizontalPadding} * 4);
  cursor: pointer;
  user-select: none;
  background: ${(props) => {
    if (!props.selected) return 'inherit';
    return getLighterColor(props);
  }};

  &:hover {
    ${(props) => !props.selected && `background:${transparentize(0.8, getLighterColor(props))};`}
  }

  .unread-icon {
    display: inline-block;
    width: 9px;
    height: 9px;
    background: ${ThemeHighlightWeaker};
    border-radius: 50%;
    margin-right: calc(${ThemeVerticalPadding} * 1.25);
    margin-left: calc(${ThemeVerticalPadding} * -2.5);
  }

  .details {
    flex-grow: 1;
    overflow: hidden;
  }

  .avatar {
    margin-right: 15px;
    width: 45px;
    max-height: unset;
    background: rgba(0, 0, 0, 0.1);
  }

  .info {
    margin-bottom: 7px;
    display: flex;
    justify-content: space-between;
  }

  .user {
    font-weight: bold;
  }

  .content {
    --max-lines: 3;
    --lh: 1.2rem;
    ${(props) => !props.unread && 'opacity: 0.66;'}
    ${(props) => props.unread && 'font-weight: bold;'}
    font-size: ${ThemeFontSizeMedium};
    line-height: var(--lh);
    position: relative;
    max-height: calc(var(--lh) * var(--max-lines));
    overflow: hidden;
    overflow-wrap: break-word;
    padding-right: 1rem;
  }

  .time {
    opacity: 0.66;
    font-size: ${ThemeFontSizeMedium};
  }
`;

export default MessageMenu;
