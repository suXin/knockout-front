import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React, { useEffect, useRef, useState } from 'react';
import { Helmet } from 'react-helmet';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';
import { getConversations } from '../../services/messages';
import updateMessages from '../../utils/messages';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeHuge,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../utils/ThemeNew';
import MessageConversation from './components/MessageConversation';
import MessageMenu from './components/MessageMenu';
import UserSearchModal from './components/UserSearchModal';
import getConversationUser from './getConversationUser';

dayjs.extend(relativeTime);

const MessagesPage = () => {
  const [conversations, setConversations] = useState([]);
  const [currentConversation, setCurrentConversation] = useState(undefined);
  const [modalOpen, setModalOpen] = useState(false);
  const [isMobile, setIsMobile] = useState(false);
  const [loaded, setLoaded] = useState(false);

  const placeholderId = useRef(-1);
  const conversationUserMap = useRef({});
  const conversationIdMap = useRef({});

  const currentUser = useSelector((state) => state.user);
  const dispatch = useDispatch();

  const match = useRouteMatch();
  const history = useHistory();

  const getData = async () => {
    const results = await getConversations();
    const userMap = {};
    const idMap = {};
    results.forEach((result) => {
      const user = getConversationUser(result.users, currentUser);
      userMap[user.id] = result;
      idMap[result.id] = result;
    });
    conversationUserMap.current = userMap;
    conversationIdMap.current = idMap;
    setConversations(results);
    updateMessages(currentUser.id, dispatch, results);
    setLoaded(true);
  };

  const startConversation = (user) => {
    let newConversation;
    if (user.id in conversationUserMap.current) {
      newConversation = conversationUserMap.current[user.id];
      let path = newConversation.id;
      if (newConversation.id < 0) path = `new${newConversation.id}`;
      history.push(`/${match.url.split('/')[1]}/${path}`);
    } else {
      newConversation = {
        id: placeholderId.current,
        users: [user],
        updatedAt: new Date().toISOString(),
      };
      placeholderId.current -= 1;
      conversationUserMap.current[user.id] = newConversation;
      conversationIdMap.current[`new${newConversation.id}`] = newConversation;
      setConversations((current) => [newConversation, ...current]);
      history.push(`/${match.url.split('/')[1]}/new${newConversation.id}`);
    }
  };

  useEffect(() => {
    const width =
      window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    setIsMobile(width <= 700);
  }, []);

  useEffect(() => {
    if (match.params.conversation in conversationIdMap.current) {
      setCurrentConversation(conversationIdMap.current[match.params.conversation]);
    } else if (match.params.conversation === undefined) {
      setCurrentConversation(undefined);
    }
  }, [match.params.conversation, conversations]);

  return (
    <StyledMessagePage isMobile={isMobile}>
      <Helmet>
        <title>Messages - Knockout!</title>
      </Helmet>
      <UserSearchModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        selectUser={startConversation}
      />
      {(currentConversation === undefined || !isMobile) && (
        <MessageMenu
          conversations={conversations}
          loaded={loaded}
          getData={getData}
          setModalOpen={setModalOpen}
          startConversation={startConversation}
          currentConversation={currentConversation}
        />
      )}
      {(currentConversation !== undefined || !isMobile) && (
        <MessageConversation
          getData={getData}
          setModalOpen={setModalOpen}
          currentConversation={currentConversation}
          backButton={isMobile}
        />
      )}
    </StyledMessagePage>
  );
};

const StyledMessagePage = styled.div`
  ${(props) =>
    !props.isMobile &&
    `display: grid;
  grid-template-columns: 30% 70%;`}

  .messages-header {
    display: flex;
    align-items: baseline;
    padding: calc(${ThemeVerticalPadding} * 2.1) calc(${ThemeHorizontalPadding} * 4);
    background: ${ThemeBackgroundLighter};

    .messages-title {
      font-weight: bold;
      font-size: ${ThemeFontSizeHuge};
      user-select: none;
    }

    .header-button {
      padding: 0;
    }

    .header-icon {
      font-size: 20px;
    }
  }

  .messages-body {
    background: ${ThemeBackgroundDarker};
    flex-grow: 1;
    overflow: auto;
  }

  .column {
    display: flex;
    flex-direction: column;
    height: ${(props) => (props.isMobile ? '85vh' : '90vh')};
  }

  .empty-message {
    text-align: center;
    margin-top: 40px;
    padding: 0 calc(${ThemeHorizontalPadding} * 4);
  }

  .empty-title {
    font-size: ${ThemeFontSizeHuge};
    font-weight: bold;
    margin-bottom: 10px;
  }

  .empty-desc {
    line-height: 1.3;
    margin-bottom: 20px;
  }
`;

export default MessagesPage;
