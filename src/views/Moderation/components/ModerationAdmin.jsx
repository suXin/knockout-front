import React, { useEffect, useState } from 'react';
import SettingsToggle from '../../../componentsNew/SettingsToggle';
import { getAdminSettings, setAdminSettings } from '../../../services/moderation';

const ModerationAdmin = () => {
  const [raidMode, setRaidMode] = useState(false);

  useEffect(() => {
    async function getSettings() {
      const adminSettings = await getAdminSettings();
      setRaidMode(adminSettings.raidMode !== null);
    }
    getSettings();
  }, []);

  const updateSettings = async () => {
    await setAdminSettings({ raidMode: !raidMode });
    setRaidMode(!raidMode);
  };

  return (
    <SettingsToggle
      label="Raid Mode"
      desc="Block account creation and restrict newer accounts (< 1 week old) from posting. Automatically expires after three
            days."
      value={raidMode}
      setValue={updateSettings}
    />
  );
};

export default ModerationAdmin;
