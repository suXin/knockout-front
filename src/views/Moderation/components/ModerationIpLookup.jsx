import React from 'react';
import styled from 'styled-components';
import { MobileMediaQuery } from '../../../componentsNew/SharedStyles';
import IpByUsernamePanel from './IpByUsernamePanel';
import IpLookupPanel from './IpLookupPanel';

const ModerationIpLookup = () => (
  <StyledModerationIpLookup>
    <IpLookupPanel />
    <IpByUsernamePanel />
  </StyledModerationIpLookup>
);

const StyledModerationIpLookup = styled.div`
  display: flex;
  flex-direction: row;

  ${MobileMediaQuery} {
    flex-direction: column;
  }
`;
export default ModerationIpLookup;
