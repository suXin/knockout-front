import styled from 'styled-components';
import { MobileMediaQuery } from '../../../componentsNew/SharedStyles';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeMedium,
  ThemeHorizontalPadding,
  ThemeTextColor,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';

export const Panel = styled.div`
  position: relative;
  background-color: ${ThemeBackgroundDarker};
  flex-basis: 50%;
  margin: calc(${ThemeVerticalPadding} / 2) calc(${ThemeHorizontalPadding} / 2);

  ${MobileMediaQuery} {
    width: auto;
    flex-basis: 2;
  }
`;

export const PanelHeader = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  background-color: ${ThemeBackgroundLighter};

  ${(props) =>
    props.centerWide &&
    `
    width: 100%;
    max-width: 800px;
    margin: 0 auto;
  `}
`;

export const PanelSearchField = styled.input`
  flex-grow: 1;
  background: transparent;
  border: none;
  outline: none;
  line-height: 22px;
  color: ${ThemeTextColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const PanelSearchButton = styled.button`
  background: transparent;
  border: none;
  outline: none;
  line-height: 22px;
  cursor: pointer;
  color: ${ThemeTextColor};
  padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
`;

export const PanelBody = styled.div`
  position: relative;
  min-height: 222px;
  padding: calc(${ThemeVerticalPadding} / 2) ${ThemeHorizontalPadding};
  table {
    width: 100%;
  }

  table th {
    color: ${ThemeTextColor};
    opacity: 0.7;
    padding: calc(${ThemeVerticalPadding} / 2) 0;
    text-align: left;
  }

  table td {
    padding: calc(${ThemeVerticalPadding} / 2) 0;
  }

  table td span {
    display: inline-block;
    border-radius: 50px;
    padding: calc(${ThemeVerticalPadding} / 2) ${ThemeHorizontalPadding};
    margin-right: calc(${ThemeHorizontalPadding} / 2);
    margin-bottom: calc(${ThemeVerticalPadding} / 2);
    color: ${ThemeTextColor};
  }
`;

export const TagItem = styled.div`
  font-size: ${ThemeFontSizeMedium};
  font-weight: 600;
  margin: 5px 0;
  padding: ${ThemeHorizontalPadding};

  .tag-icon {
    margin-right: 15px;
  }
`;

export const PanelBodyPlaceholder = styled.div`
  text-align: center;
  padding: 100px 0;
  line-height: 22px;
  color: ${ThemeTextColor};
  opacity: 0.7;
`;
