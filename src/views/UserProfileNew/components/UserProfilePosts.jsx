import React from 'react';
import PropTypes from 'prop-types';

import Post from '../../../componentsNew/Post';
import PostProfilePlaceholder from '../../../componentsNew/PostProfilePlaceholder';
import { getUserPosts } from '../../../services/user';
import useFetchedData from './useFetchedData';
import UserContentContainer from './UserContentContainer';

const UserProfilePosts = ({ posts, showRatings, match }) => {
  const [currentPosts, loading, fetchData] = useFetchedData(
    posts,
    posts.posts,
    match,
    getUserPosts
  );

  let postContent = Array(16)
    .fill(1)
    // eslint-disable-next-line react/no-array-index-key
    .map((item, index) => <PostProfilePlaceholder key={`p${index}`} />);
  if (!loading)
    postContent = currentPosts.posts.map((post) => (
      <Post
        key={post.id}
        hideUserWrapper
        hideControls
        byCurrentUser
        ratings={showRatings ? post.ratings : []}
        threadId={post.thread.id || post.thread}
        threadInfo={post.thread.id && post.thread}
        postId={post.id}
        postBody={post.content}
        postDate={post.createdAt}
        postPage={post.page}
        threadPage={1}
        profileView
      />
    ));

  return (
    <UserContentContainer
      fetchData={fetchData}
      total={currentPosts.totalPosts}
      currentPage={currentPosts.currentPage}
      content={postContent}
    />
  );
};

UserProfilePosts.propTypes = {
  posts: PropTypes.shape({
    posts: PropTypes.arrayOf(
      PropTypes.shape({
        content: PropTypes.string.isRequired,
        createdAt: PropTypes.string.isRequired,
        id: PropTypes.number.isRequired,
        page: PropTypes.number,
        thread: PropTypes.shape({
          id: PropTypes.number.isRequired,
          title: PropTypes.string.isRequired,
        }),
      })
    ),
    totalPosts: PropTypes.number,
    currentPage: PropTypes.number.isRequired,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
  showRatings: PropTypes.bool.isRequired,
};

export default UserProfilePosts;
