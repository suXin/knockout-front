import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { getUserBans } from '../../../services/user';
import UserProfileBansComponent from '../../UserProfile/components/UserProfileBans';

const UserProfileBans = ({ user, match }) => {
  const [bans, setBans] = useState([]);

  const getBans = async () => {
    setBans(await getUserBans(match.params.id));
  };

  useEffect(() => {
    getBans();
  }, [match.params.id]);

  return <UserProfileBansComponent bans={bans} user={user} />;
};

UserProfileBans.propTypes = {
  user: PropTypes.shape({
    username: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
};

export default UserProfileBans;
