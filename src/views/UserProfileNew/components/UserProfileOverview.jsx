import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Post from '../../../componentsNew/Post';
import { getUserPosts, getUserThreads } from '../../../services/user';
import { loadDisplayNsfwFilterSettingFromStorageBoolean } from '../../../utils/postOptionsStorage';
import ThreadItem from '../../../componentsNew/ThreadItem';
import { ThemeFontSizeLarge, ThemeFontSizeMedium } from '../../../utils/ThemeNew';

const UserProfileOverview = ({ match, posts, setPosts, threads, setThreads, showRatings }) => {
  const nsfwFilterEnabled = loadDisplayNsfwFilterSettingFromStorageBoolean();

  const fetchData = async () => {
    setPosts(await getUserPosts(match.params.id, 1, nsfwFilterEnabled));
    setThreads(await getUserThreads(match.params.id, 1, nsfwFilterEnabled));
  };

  useEffect(() => {
    fetchData();
  }, [match.params.id]);

  return (
    <StyledUserProfileOverview>
      {posts.posts?.length > 0 && (
        <div className="overview-header">
          <span className="overview-header-title">Latest Posts</span>
          <Link className="overview-header-link" to={`${match.url}/posts`}>
            See all
          </Link>
        </div>
      )}
      {posts.posts?.slice(0, 2).map((post) => (
        <Post
          key={post.id}
          hideUserWrapper
          hideControls
          byCurrentUser
          ratings={showRatings ? post.ratings : []}
          threadId={post.thread.id || post.thread}
          threadInfo={post.thread.id && post.thread}
          postId={post.id}
          postBody={post.content}
          postDate={post.createdAt}
          postPage={post.page}
          threadPage={1}
          profileView
        />
      ))}
      {threads.threads?.length > 0 && (
        <div className="overview-header">
          <span className="overview-header-title">Latest Threads</span>
          <Link className="overview-header-link" to={`${match.url}/threads`}>
            See all
          </Link>
        </div>
      )}
      {threads.threads?.slice(0, 3).map((thread) => (
        <ThreadItem
          key={thread.id}
          id={thread.id}
          createdAt={thread.createdAt}
          deleted={thread.deleted}
          iconId={thread.iconId}
          locked={thread.locked}
          pinned={thread.pinned}
          postCount={thread.postCount}
          title={thread.title}
          tags={thread.tags}
          backgroundUrl={thread.backgroundUrl}
          backgroundType={thread.backgroundType}
          subforumName={thread.subforumName}
          minimal
        />
      ))}
    </StyledUserProfileOverview>
  );
};

const StyledUserProfileOverview = styled.div`
  .overview-header {
    display: flex;
    justify-content: space-between;
    margin: 20px 0;
    margin-top: 40px;
  }

  .overview-header-title {
    font-size: ${ThemeFontSizeLarge};
  }

  .overview-header-link {
    font-size: ${ThemeFontSizeMedium};
    opacity: 60%;
  }
`;

UserProfileOverview.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
    url: PropTypes.string.isRequired,
  }).isRequired,
  posts: PropTypes.shape({
    posts: PropTypes.array.isRequired,
  }).isRequired,
  setPosts: PropTypes.func.isRequired,
  threads: PropTypes.shape({
    threads: PropTypes.array.isRequired,
  }).isRequired,
  setThreads: PropTypes.func.isRequired,
  showRatings: PropTypes.bool.isRequired,
};
export default UserProfileOverview;
