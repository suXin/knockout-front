import React from 'react';
import PropTypes from 'prop-types';
import ThreadItem from '../../../componentsNew/ThreadItem';
import ThreadItemPlaceholder from '../../../componentsNew/ThreadItemPlaceholder';
import { getUserThreads } from '../../../services/user';
import useFetchedData from './useFetchedData';
import UserContentContainer from './UserContentContainer';

const UserProfileThreads = ({ threads, match }) => {
  const [currentThreads, loading, fetchData] = useFetchedData(
    threads,
    threads.threads,
    match,
    getUserThreads
  );

  let threadContent = Array(16)
    .fill(1)
    // eslint-disable-next-line react/no-array-index-key
    .map((item, index) => <ThreadItemPlaceholder minimal key={`p${index}`} />);
  if (!loading)
    threadContent = currentThreads.threads.map((thread) => (
      <ThreadItem
        key={thread.id}
        id={thread.id}
        createdAt={thread.createdAt}
        deleted={thread.deleted}
        iconId={thread.iconId}
        locked={thread.locked}
        pinned={thread.pinned}
        postCount={thread.postCount}
        title={thread.title}
        tags={thread.tags}
        backgroundUrl={thread.backgroundUrl}
        backgroundType={thread.backgroundType}
        subforumName={thread.subforumName}
        minimal
      />
    ));
  return (
    <UserContentContainer
      fetchData={fetchData}
      total={currentThreads.totalPosts}
      currentPage={currentThreads.currentPage}
      content={threadContent}
    />
  );
};

UserProfileThreads.propTypes = {
  threads: PropTypes.shape({
    totalThreads: PropTypes.number,
    currentPage: PropTypes.number.isRequired,
    threads: PropTypes.arrayOf(PropTypes.object),
  }).isRequired,
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.number.isRequired,
    }).isRequired,
  }).isRequired,
};

export default UserProfileThreads;
