import React from 'react';
import PropTypes from 'prop-types';
import Pagination from '../../../componentsNew/Pagination';

const UserContentContainer = ({ fetchData, total, currentPage, content }) => (
  <div>
    <Pagination
      pageChangeFn={fetchData}
      totalPosts={total}
      currentPage={currentPage}
      pageSize={40}
      useButtons
      marginBottom
    />
    {content}
    <Pagination
      pageChangeFn={fetchData}
      totalPosts={total}
      currentPage={currentPage}
      pageSize={40}
      useButtons
    />
  </div>
);

UserContentContainer.propTypes = {
  fetchData: PropTypes.func.isRequired,
  total: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  content: PropTypes.arrayOf(PropTypes.object).isRequired,
};

export default UserContentContainer;
