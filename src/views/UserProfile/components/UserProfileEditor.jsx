import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { useSelector } from 'react-redux';
import styled from 'styled-components';
import { updateUserBackground, updateUserProfile } from '../../../services/user';

import Modal from '../../../componentsNew/Modals/Modal';
import ModalRadioButton from '../../../componentsNew/Modals/ModalRadioButton';
import { FieldLabelSmall, TextField, TextFieldLarge } from '../../../componentsNew/FormControls';
import { pushSmartNotification } from '../../../utils/notification';
import { checkBetweenLengths, checkValidURL, validate } from '../../../utils/forms';
import InputError, { StyledInputError } from '../../../componentsNew/InputError';

const UserProfileEditor = ({ closeFn, isOpen, profile, callback }) => {
  const [bio, setBio] = useState('');
  const [website, setWebsite] = useState('');
  const [steam, setSteam] = useState('');
  const [discord, setDiscord] = useState('');
  const [github, setGithub] = useState('');
  const [twitter, setTwitter] = useState('');
  const [twitch, setTwitch] = useState('');
  const [gitlab, setGitlab] = useState('');
  const [tumblr, setTumblr] = useState('');
  const [backgroundType, setBackgroundType] = useState('cover');
  const [backgroundImage, setBackgroundImage] = useState(null);
  const [errors, setErrors] = useState({});
  const { id: userId, usergroup } = useSelector((state) => state.user);

  const profileValidators = {
    website: [checkValidURL],
    steam: [(value) => checkValidURL(value, 'steamcommunity.com')],
    discord: [(value) => checkBetweenLengths(value, 0, 37)],
    github: [(value) => checkBetweenLengths(value, 0, 35)],
    twitter: [(value) => checkBetweenLengths(value, 0, 35)],
    twitch: [(value) => checkBetweenLengths(value, 0, 35)],
    gitlab: [(value) => checkBetweenLengths(value, 0, 35)],
    tumblr: [(value) => checkBetweenLengths(value, 0, 35)],
  };

  useEffect(() => {
    const results = validate(
      {
        website,
        steam,
        discord,
        github,
        twitter,
        twitch,
        gitlab,
        tumblr,
      },
      profileValidators
    );
    setErrors(results);
  }, [website, steam, discord, github, twitter, twitch, gitlab, tumblr]);

  useEffect(() => {
    setBio(profile.bio || '');
    setWebsite(profile.social?.website || '');
    setSteam(profile.social?.steam?.url);
    setDiscord(profile.social?.discord || '');
    setGithub(profile.social?.github || '');
    setTwitter(profile.social?.twitter || '');
    setTwitch(profile.social?.twitch || '');
    setGitlab(profile.social?.gitlab || '');
    setTumblr(profile.social?.tumblr || '');
    setBackgroundType(profile.background?.type || 'cover');
  }, [profile]);

  const handleSubmit = async () => {
    try {
      await updateUserProfile(userId, {
        bio,
        social: {
          website,
          steam: {
            url: steam,
          },
          discord,
          github,
          twitter,
          twitch,
          gitlab,
          tumblr,
        },
      });

      if (usergroup !== 1) {
        await updateUserBackground(userId, {
          image: backgroundImage,
          type: backgroundType,
        });
      }

      let steamUrl = steam;
      if (steamUrl?.slice(-1) === '/') {
        steamUrl = steamUrl.slice(0, -1);
      }
      callback({
        bio,
        social: {
          website,
          discord,
          steam: { name: steamUrl?.split('/').slice(-1)[0], url: steam },
          github,
          twitter,
          twitch,
          gitlab,
          tumblr,
        },
        background: {
          type: backgroundType,
          url: profile.background.url,
        },
      });
      closeFn();
    } catch (error) {
      console.log(error);
      pushSmartNotification({ error: 'Could not update profile.' });
    }
  };

  return (
    <Modal
      iconUrl="https://img.icons8.com/color/96/000000/paint-net.png"
      title="Edit your profile"
      cancelFn={closeFn}
      submitFn={handleSubmit}
      disableSubmit={Object.keys(errors).length > 0}
      isOpen={isOpen}
    >
      <TextFieldLarge
        value={bio}
        maxLength={160}
        placeholder="Add your bio"
        onChange={(e) => setBio(e.target.value)}
      />
      <LinkRow>
        <div className="link-input">
          <i className="fas fa-link link-icon" />
          <TextField
            value={website}
            placeholder="Website"
            onChange={(e) => setWebsite(e.target.value)}
          />
        </div>
        <InputError error={errors.website} />
      </LinkRow>

      <LinkRow>
        <div className="link-input">
          <i className="fab fa-steam link-icon" />
          <TextField
            value={steam}
            placeholder="Steam profile URL"
            onChange={(e) => setSteam(e.target.value)}
          />
        </div>
        <InputError error={errors.steam} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fab fa-discord link-icon" />
          <TextField
            value={discord}
            placeholder="Discord username"
            onChange={(e) => setDiscord(e.target.value)}
          />
        </div>
        <InputError error={errors.discord} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fab fa-github link-icon" />
          <TextField
            value={github}
            placeholder="Github username"
            onChange={(e) => setGithub(e.target.value)}
          />
        </div>
        <InputError error={errors.github} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fab fa-twitter link-icon" />
          <TextField
            value={twitter}
            placeholder="Twitter username"
            onChange={(e) => setTwitter(e.target.value)}
          />
        </div>
        <InputError error={errors.twitter} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fab fa-twitch link-icon" />
          <TextField
            value={twitch}
            placeholder="Twitch username"
            onChange={(e) => setTwitch(e.target.value)}
          />
        </div>
        <InputError error={errors.twitch} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fab fa-gitlab link-icon" />
          <TextField
            value={gitlab}
            placeholder="Gitlab username"
            onChange={(e) => setGitlab(e.target.value)}
          />
        </div>
        <InputError error={errors.gitlab} />
      </LinkRow>
      <LinkRow>
        <div className="link-input">
          <i className="fab fa-tumblr-square link-icon" />
          <TextField
            value={tumblr}
            placeholder="Tumblr username"
            onChange={(e) => setTumblr(e.target.value)}
          />
        </div>
        <InputError error={errors.tumblr} />
      </LinkRow>
      {usergroup !== 1 && (
        <>
          <FieldLabelSmall>Background type</FieldLabelSmall>
          <ModalRadioButton
            name="backgroundType"
            property={backgroundType}
            values={['cover', 'tiled']}
            onChange={(e) => setBackgroundType(e.target.value)}
          />
          <FieldLabelSmall>Background image (max size ~2MB)</FieldLabelSmall>
          <input
            name="backgroundImage"
            type="file"
            onChange={(e) => setBackgroundImage(e.target.files[0])}
          />
        </>
      )}
    </Modal>
  );
};

const LinkRow = styled.div`
  margin-bottom: 20px;

  .link-input {
    display: flex;
    align-items: center;
    margin-bottom: 10px;
  }

  .link-icon {
    opacity: 0.6;
    margin-right: 12px;
    font-size: 20px;
    width: 20px;
    text-align: center;
  }

  ${StyledInputError} {
    margin-left: 32px;
  }
`;

UserProfileEditor.propTypes = {
  closeFn: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired,
  profile: PropTypes.shape({
    bio: PropTypes.string,
    social: PropTypes.object,
    background: PropTypes.shape({
      type: PropTypes.string,
      url: PropTypes.string,
    }),
  }).isRequired,
  callback: PropTypes.func.isRequired,
};
export default UserProfileEditor;
