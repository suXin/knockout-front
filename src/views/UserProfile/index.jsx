import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useRouteMatch, Redirect } from 'react-router-dom';
import styled from 'styled-components';

import { Helmet } from 'react-helmet';
import config from '../../../config';
import { getUser, getUserBans, getUserProfile, getUserTopRatings } from '../../services/user';
import { updateBackgroundRequest } from '../../state/background';

import UserProfileHeading from './components/UserProfileHeading';
import UserProfileEditor from './components/UserProfileEditor';
import { OutlineButton, Button } from '../../componentsNew/Buttons';
import UserInfo from '../../componentsNew/Post/components/UserInfo';
import { removeUserImage, removeUserProfile } from '../../services/moderation';
import { loadHideRatingsFromStorageBoolean } from '../../services/theme';
import UserProfileSwitch from './components/UserProfileSwitch';
import { ThemeVerticalPadding, ThemeHorizontalPadding } from '../../utils/ThemeNew';
import { MODERATOR_GROUPS } from '../../utils/userGroups';
import UserGroupRestricted from '../../componentsNew/UserGroupRestricted';
import BanModal from '../../componentsNew/BanModal';

const UserProfile = () => {
  const match = useRouteMatch();
  const dispatch = useDispatch();

  const [user, setUser] = useState({});
  const [bans, setBans] = useState(null);
  const [topRatings, setTopRatings] = useState([]);
  const [userProfile, setUserProfile] = useState({});
  const [profileEdit, setProfileEdit] = useState(false);
  const [userId, setUserId] = useState(match.params.id);
  const [loaded, setLoaded] = useState(false);
  const [canEditProfile, setCanEditProfile] = useState(true);
  const [banModalOpen, setBanModalOpen] = useState(false);

  const currentUser = useSelector((state) => state.user);

  const fetchData = async () => {
    try {
      if (!match.params.id) {
        setLoaded(true);
        return;
      }

      setUser(await getUser(match.params.id));
      setBans(await getUserBans(match.params.id));
      if (!loadHideRatingsFromStorageBoolean()) {
        setTopRatings(await getUserTopRatings(match.params.id));
      }
      setUserProfile(await getUserProfile(match.params.id));
    } catch (err) {
      console.error(err);
    }
    setLoaded(true);
  };

  useEffect(() => {
    fetchData();
    return () => {
      dispatch(updateBackgroundRequest(null));
    };
  }, []);

  useEffect(() => {
    if (userProfile.backgroundUrl) {
      const url = `${config.cdnHost}/image/${userProfile.backgroundUrl}`;
      dispatch(updateBackgroundRequest(url, userProfile.backgroundType));
    } else {
      dispatch(updateBackgroundRequest(null));
    }
  }, [userProfile]);

  useEffect(() => {
    if (match.params.id !== userId) {
      fetchData();
      setUserId(match.params.id);
    }
  }, [match.params.id]);

  useEffect(() => {
    setCanEditProfile(user.id === currentUser.id && currentUser.usergroup !== 1);
  }, [user, currentUser]);

  if (loaded && user.id === undefined) {
    return <Redirect to="/" />;
  }

  return (
    <StyledProfileWrapper>
      <Helmet>
        <title>{user.username ? `${user.username}'s Profile - Knockout!` : 'Knockout!'}</title>
      </Helmet>
      <div className="user-profile-info">
        <UserInfo user={user} topRatings={topRatings} profileView />
        <UserGroupRestricted userGroupIds={MODERATOR_GROUPS}>
          <OutlineButton
            style={{ marginRight: 'unset' }}
            onClick={() => removeUserImage({ userId: user.id, avatar: true })}
          >
            Remove Avatar
          </OutlineButton>
          <OutlineButton
            style={{ marginRight: 'unset' }}
            onClick={() => removeUserImage({ userId: user.id, background: true })}
          >
            Remove Background
          </OutlineButton>
          <OutlineButton
            style={{ marginRight: 'unset' }}
            onClick={() => {
              removeUserProfile(user.id);
              setUserProfile({});
            }}
          >
            Remove profile customizations
          </OutlineButton>
          <Button style={{ marginRight: 'unset' }} alert onClick={() => setBanModalOpen(true)}>
            Ban User
          </Button>
          {loaded && (
            <BanModal
              userId={user.id}
              isOpen={banModalOpen}
              submitFn={() => setBanModalOpen(false)}
              cancelFn={() => setBanModalOpen(false)}
            />
          )}
        </UserGroupRestricted>
      </div>
      <div className="profile-content">
        <UserProfileHeading
          headingText={userProfile.headingText}
          personalSite={userProfile.personalSite}
        />

        {canEditProfile && <Button onClick={() => setProfileEdit(true)}>Edit profile</Button>}

        <UserProfileEditor
          closeFn={() => setProfileEdit(false)}
          isOpen={profileEdit}
          profile={userProfile}
          callback={setUserProfile}
        />
        {loaded && (
          <UserProfileSwitch userId={userId} bans={bans} user={user} ratings={topRatings} />
        )}
      </div>
    </StyledProfileWrapper>
  );
};

export default UserProfile;

export const StyledProfileWrapper = styled.div`
  display: grid;
  grid-template-columns: 230px 1fr;
  margin: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};

  @media (max-width: 960px) {
    display: flex;
    flex-direction: column;
  }

  .profile-content {
    flex: 1;
    padding-left: ${ThemeHorizontalPadding};

    @media (max-width: 960px) {
      padding-top: ${ThemeVerticalPadding};
      padding-left: 0;
    }
  }

  .user-profile-info {
    display: flex;
    flex-direction: column;
  }
`;
