import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import PropTypes from 'prop-types';
import {
  ThemeBackgroundDarker,
  ThemeBackgroundLighter,
  ThemeFontSizeSmall,
  ThemeHorizontalPadding,
  ThemeVerticalPadding,
} from '../../../utils/ThemeNew';
import UserRoleWrapper from '../../../componentsNew/UserRoleWrapper';
import Placeholder from '../../../componentsNew/Placeholder';

dayjs.extend(relativeTime);

const defaultColors = [
  ['#ff9e39', '#f16d0f'], // general
  ['#1e5fb3', '#a7bdd9'], // fast threads
  ['#80d8ff', '#b53fb1'], // videos
  ['#19d0a6', '#2599a5'], // film tv
  ['#1bffb1', '#2dbbff'], // gaming
  ['#18caff', '#1aa0ff'], // game generals
  ['#3cff18', '#ffce1a'], // progress report
  ['#da46ff', '#da2361'], // creativity
  ['#fff94a', '#669529'], // h&s
  ['#4cafe8', '#3b5592'], // developers
  ['#ff7c55', '#f15a2a'], // news
  ['#af5c2d', '#753b24'], // politics
  ['#946622', '#deb67c'], // source
  ['#fb2e24', '#ae1c1c'], // meta
  ['#ab7977', '#bd4045'], // facepunch
];

const defaultColor = (index) => defaultColors[index % defaultColors.length];

const buildLastPost = (lastPost, threadTitle) => {
  return (
    <Link
      to={`/thread/${lastPost.thread.id}/${lastPost.thread.lastPost?.page || '1'}#post-${
        lastPost.id
      }`}
    >
      <p className="thread-title">{threadTitle}</p>
      <p className="second-row">
        <div className="row-child dim">
          <span>Last post by&nbsp;</span>
        </div>
        <UserRoleWrapper className="username shrink-item" user={lastPost.user}>
          {lastPost.user.username}
        </UserRoleWrapper>
        <div className="row-child shrink-item dim">
          <span>
            &nbsp;
            {dayjs(lastPost.createdAt).fromNow()}
          </span>
        </div>
        <div className="row-child last-page dim">
          <span>
            Go to page
            <span> </span>
            {lastPost.thread.lastPost?.page || 1}
            <span> ➤</span>
          </span>
        </div>
      </p>
    </Link>
  );
};

const SubforumItem = ({
  index,
  id,
  name,
  description,
  icon,
  lastPost,
  totalPosts,
  totalThreads,
  isPlaceholder,
}) => {
  const threadTitle = lastPost?.thread.title || 'No thread';

  const totalPostsAriaLabel = `Total posts in ${name}`;
  const totalThreadsAriaLabel = `Total threads in ${name}`;

  const totalPostsLocalised = Number(totalPosts).toLocaleString();
  const totalThreadsLocalised = Number(totalThreads).toLocaleString();

  if (isPlaceholder) {
    return (
      <SubforumItemWrapper icon={icon} gradient={defaultColor(index)} isPlaceholder>
        <div className="title-stats" title={description}>
          <Placeholder width={150} marginTop={10} marginBottom={6} />
          <Placeholder width={120} marginBottom={10} />
        </div>
        <div className="thread-info">
          <Placeholder width={330} marginTop={10} marginBottom={6} />
          <Placeholder width={180} textSize="small" />
        </div>
      </SubforumItemWrapper>
    );
  }

  return (
    <SubforumItemWrapper icon={icon} gradient={defaultColor(index)}>
      <Link to={`/subforum/${id}`} className="title-stats" title={description}>
        <p className="sf-name">{name}</p>
        <div className="stats">
          <span aria-label={totalThreadsAriaLabel} title="Total threads">
            <i className="fas fa-comment-dots" />
            <span> </span>
            {totalThreadsLocalised}
          </span>
          <span aria-label={totalPostsAriaLabel} title="Total posts">
            <i className="fas fa-reply-all" />
            <span> </span>
            {totalPostsLocalised}
          </span>
        </div>
      </Link>
      <div className="thread-info">{lastPost && buildLastPost(lastPost, threadTitle)}</div>
    </SubforumItemWrapper>
  );
};

SubforumItem.propTypes = {
  description: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  index: PropTypes.number.isRequired,
  lastPost: PropTypes.objectOf(PropTypes.any).isRequired,
  name: PropTypes.string.isRequired,
  totalPosts: PropTypes.number,
  totalThreads: PropTypes.number,
  isPlaceholder: PropTypes.bool,
};

SubforumItem.defaultProps = {
  isPlaceholder: false,
  totalPosts: 0,
  totalThreads: 0,
};

export default SubforumItem;

const SubforumItemWrapper = styled.div`
  display: grid;
  grid-template-rows: auto auto;

  height: 112px;
  background: ${ThemeBackgroundLighter};

  .title-stats p.sf-name,
  .thread-info p.thread-title,
  .thread-info span.second-row {
    margin: calc(${ThemeVerticalPadding} / 2) 0 calc(${ThemeVerticalPadding} / 2) 0;
  }

  .title-stats {
    align-items: flex-start;
    display: flex;
    flex-direction: column;
    justify-content: center;
    position: relative;
    padding-top: 0;
    padding-left: ${ThemeVerticalPadding};
    font-weight: bold;

    background-image: url(${(props) => props.icon});
    background-repeat: no-repeat;
    background-position: calc(100% - ${ThemeHorizontalPadding}) center;
    background-size: auto 40px;

    &:after {
      content: '';
      display: block;
      position: absolute;
      left: 0;
      right: 0;
      bottom: 0;
      width: 100%;
      height: 1px;
      background: ${(props) => `
        rgba(0, 0, 0, 0) linear-gradient(270deg, ${props.gradient[0]} 0%, ${props.gradient[1]} 100%) repeat scroll 0% 0%
      `};
    }

    p {
      padding: 0 0 2px 0;
    }

    p,
    a,
    .stats {
      width: calc(100% - 60px);
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }

    &:hover {
      ${(props) => !props.isPlaceholder && 'cursor: pointer;'}
      filter: brightness(1.05);

      p {
        text-decoration: underline;
      }
    }

    .stats {
      font-size: ${ThemeFontSizeSmall};
      font-weight: normal;
      opacity: 0.5;
      overflow: hidden;
      padding-top: calc(${ThemeVerticalPadding / 2});
      padding-bottom: calc(${ThemeVerticalPadding / 2});

      > span:not(:first-child) {
        padding-left: ${ThemeHorizontalPadding};
      }
    }
  }

  .thread-info {
    background: ${ThemeBackgroundDarker};
    padding: ${ThemeVerticalPadding} ${ThemeHorizontalPadding};
    position: relative;
    display: grid;

    a {
      overflow: hidden;
    }

    p {
      margin: 0;
      width: 100%;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;

      &.thread-title {
        line-height: 1.3em; /* Fixes "overflow: hidden" cutting the top and bottoms of certain tall characters */
      }

      &:last-child {
        font-size: ${ThemeFontSizeSmall};
      }
    }

    .dim {
      opacity: 0.5;
    }

    p.second-row {
      display: flex;
      position: absolute;
      left: 0;
      height: calc(${ThemeFontSizeSmall} + 2px);
      padding-left: ${ThemeHorizontalPadding};
      padding-right: ${ThemeHorizontalPadding};
      margin-top: 0;
      box-sizing: border-box;

      .username {
        display: inline;
        opacity: 1;
      }

      .shrink-item {
        overflow: hidden;
        text-overflow: ellipsis;
      }

      .last-page {
        overflow: visible;
        margin-left: auto;
      }

      .row-child {
        white-space: nowrap;
      }
    }
    .go-to-page {
      position: absolute;
      right: ${ThemeHorizontalPadding};
      opacity: 0.5;
    }

    &:hover {
      filter: brightness(1.05);
      text-decoration: underline;
      ${(props) => !props.isPlaceholder && 'cursor: pointer;'}
    }
  }
`;
