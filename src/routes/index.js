import { loadPunchyLabsFromStorageBoolean } from '../services/theme';
import * as Routes from './routes';

const labsEnabled = loadPunchyLabsFromStorageBoolean();

const routes = [
  {
    path: '/',
    name: 'HomePage',
    component: Routes.HomePage,
    exact: true,
  },
  // user "control panel":
  {
    path: '/usersetup',
    name: 'UserSetup',
    component: Routes.UserSetup,
    exact: true,
  },
  // login page:
  {
    path: '/login',
    name: 'LoginPage',
    component: Routes.LoginPage,
    exact: true,
  },
  // user profile edit
  {
    path: '/usersettings',
    name: 'UserSettings',
    component: Routes.UserSettingsPage,
    exact: true,
  },
  // user profile
  {
    path: '/user/:id',
    name: 'UserProfile',
    component: labsEnabled ? Routes.UserProfileNew : Routes.UserProfile,
    exact: !labsEnabled,
  },
  // alerts list
  {
    path: '/alerts/list/:page?',
    name: 'AlertsList',
    component: Routes.AlertsList,
    exact: true,
  },
  // logout
  {
    path: '/logout',
    name: 'Logout',
    component: Routes.Logout,
    exact: true,
  },
  // rules page:
  {
    path: '/rules',
    name: 'Rules',
    component: Routes.Rules,
    exact: false,
  },
  // static KnockoutBB syntax page:
  {
    path: '/knockoutbb',
    name: 'KnockoutBBSyntaxPage',
    component: Routes.KnockoutBBSyntaxPage,
    exact: true,
  },
  // subforum page:
  {
    path: '/subforum/:id/:page?',
    name: 'SubforumPage',
    component: Routes.SubforumPage,
    exact: true,
  },
  // subforum rules page:
  {
    path: '/subforumRules/:id',
    name: 'SubforumRulesPage',
    component: Routes.SubforumRulesPage,
    exact: true,
  },
  // thread creation:
  {
    path: '/thread/new/:id',
    name: 'ThreadCreationPage',
    component: Routes.ThreadCreationPage,
    exact: true,
  },
  // thread page:
  {
    path: '/thread/:id/:page?',
    name: 'ThreadPage',
    component: Routes.ThreadPage,
    exact: true,
  },
  // thread rules page:
  {
    path: '/threadRules/:id',
    name: 'ThreadRulesPage',
    component: Routes.ThreadRulesPage,
    exact: true,
  },
  // moderation page:
  {
    path: '/moderate',
    name: 'ModerateDashboard',
    component: Routes.Moderate,
    exact: false,
  },
  // events page:
  {
    path: '/events',
    name: 'EventsPage',
    component: Routes.EventsPage,
    exact: true,
  },
  {
    path: '/search',
    name: 'SearchPage',
    component: Routes.SearchPage,
    exact: true,
  },
  {
    path: '/messages/new/:user',
    name: 'MessagesPage',
    component: Routes.MessagesPage,
    exact: true,
  },
  {
    path: '/messages/:conversation?',
    name: 'MessagesPage',
    component: Routes.MessagesPage,
    exact: false,
  },
  {
    path: '/docs',
    name: 'DocsPage',
    component: Routes.DocsPage,
    exact: true,
  },
];

export default routes;
