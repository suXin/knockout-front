/* eslint-disable no-underscore-dangle */
import React from 'react';
import { customRender, fireEvent } from '../../custom_renderer';

import EditorBB from '../../../src/componentsNew/EditorBB';

import * as EditorBBHelprs from '../../../src/componentsNew/EditorBB/helpers';
import * as optionsStorage from '../../../src/utils/postOptionsStorage';

describe('EditorBB component', () => {
  let contentMock;
  let setContentMock;

  beforeEach(() => {
    contentMock = '';
    setContentMock = jest.fn();
    localStorage.clear();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('Entering content', () => {
    test('I can enter content into the text area, and the content gets updated', () => {
      const { getByLabelText } = customRender(
        <EditorBB content={contentMock} setContent={setContentMock} />
      );
      const textArea = getByLabelText('Content');

      fireEvent.change(textArea, { target: { value: 'Editor content' } });
      expect(setContentMock).toBeCalledWith('Editor content');
    });
  });

  describe('Hotkeys', () => {
    describe('With extra scripting activated', () => {
      beforeEach(() => {
        localStorage.setItem('extraScripting', true);
      });

      test('I can press any key, and the currently pressed combination will get checked for a valid hotkey combination', () => {
        const checkForHotkeysMock = jest.spyOn(EditorBBHelprs, 'checkForHotkeys');

        const { getByLabelText } = customRender(
          <EditorBB content={contentMock} setContent={setContentMock} />
        );

        const textArea = getByLabelText('Content');
        fireEvent.keyDown(textArea, { key: 'B', code: 66 });
        expect(checkForHotkeysMock).toBeCalled();
      });
    });

    describe('With extra scripting deactivated', () => {
      beforeEach(() => {
        localStorage.setItem('extraScripting', false);
      });

      test('It does not check the key combination for any hotkeys', () => {
        const checkForHotkeysMock = jest.spyOn(EditorBBHelprs, 'checkForHotkeys');

        const { getByLabelText } = customRender(
          <EditorBB content={contentMock} setContent={setContentMock} />
        );

        const textArea = getByLabelText('Content');
        fireEvent.keyDown(textArea, { key: 'B', code: 66 });
        expect(checkForHotkeysMock).not.toBeCalled();
      });
    });
  });

  describe('Editor toolbar', () => {
    describe('With extra scripting activated', () => {
      beforeEach(() => {
        localStorage.setItem('extraScripting', true);
      });

      test('I can click on a formatting helper icon, and the formatter gets called with the associated information', () => {
        const insertTagMock = jest.spyOn(EditorBBHelprs, 'insertTag').mockImplementation(() => {});

        const { getByLabelText } = customRender(
          <EditorBB content={contentMock} setContent={setContentMock} />
        );

        const boldHelper = getByLabelText('Bold');
        fireEvent.click(boldHelper);
        expect(insertTagMock).toBeCalled();
      });
    });

    describe('With extra scripting deactivated', () => {
      beforeEach(() => {
        localStorage.setItem('extraScripting', false);
      });

      test('It does not display the formatting helpers', () => {
        const { getByText } = customRender(
          <EditorBB content={contentMock} setContent={setContentMock} />
        );

        expect(getByText('Extra scripting disabled!')).toBeDefined();
      });
    });

    test('I can change settings relating to usage of the editor', () => {
      const saveExtraScriptingInfoSettingToStorageMock = jest.spyOn(
        optionsStorage,
        'saveExtraScriptingInfoSettingToStorage'
      );

      const { getByText, getByLabelText } = customRender(
        <EditorBB content={contentMock} setContent={setContentMock} />
      );

      const settingsButton = getByText('Settings');
      fireEvent.click(settingsButton);

      const extraScriptingInput = getByLabelText(
        'Allow for extra scripting (disable if you have issues on mobile):'
      );

      fireEvent.click(extraScriptingInput);
      expect(saveExtraScriptingInfoSettingToStorageMock).toHaveBeenCalled();
    });
  });

  describe('With children passed to it', () => {
    test('Buttons sent as children are rendered in the toolbar as actions', () => {
      const customActionMock = jest.fn();

      const { getByText } = customRender(
        <EditorBB content={contentMock} setContent={setContentMock}>
          <>
            <button type="button" onClick={customActionMock}>
              I am a custom button!
            </button>
          </>
        </EditorBB>
      );

      const customButton = getByText('I am a custom button!');

      expect(customButton).toBeDefined();
      fireEvent.click(customButton);

      expect(customActionMock).toBeCalled();
    });
  });
});
