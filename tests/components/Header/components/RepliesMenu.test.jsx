import React from 'react';
import { waitFor } from '@testing-library/react';
import { customRender, fireEvent } from '../../../custom_renderer';
import '@testing-library/jest-dom/extend-expect';
import RepliesMenu from '../../../../src/componentsNew/Header/components/RepliesMenu';

describe('RepliesMenu component', () => {
  const initialStateWithMention = {
    mentions: {
      mentions: [
        {
          mentionId: 123,
          threadId: 456,
          postId: 789,
          threadTitle: 'Thread',
          mentionedBy: { username: 'Bob', usergroup: 1, isBanned: false },
          content: '["A mention!"]',
        },
      ],
    },
    messages: {
      messages: [
        {
          id: 12,
          conversationId: 1,
          user: {
            id: 3,
            username: 'Joe',
            usergroup: 1,
            avatarUrl: '',
          },
          content: 'stuff',
          readAt: null,
        },
      ],
    },
  };

  it('displays the menu when the icon is clicked', () => {
    const { getByTitle, queryByText } = customRender(<RepliesMenu />, {
      initialState: initialStateWithMention,
    });
    fireEvent.click(getByTitle('Messages'));
    expect(queryByText('Messages')).not.toBeNull();
  });

  it('displays unread replies', () => {
    const { getByTitle, queryByText, queryByTitle } = customRender(<RepliesMenu />, {
      initialState: initialStateWithMention,
    });
    fireEvent.click(getByTitle('Messages'));
    expect(queryByText('Thread.')).not.toBeNull();
    expect(queryByText('Bob')).not.toBeNull();
    expect(queryByText('Joe')).not.toBeNull();
    expect(queryByTitle('Messages').querySelector('.link-notification')).toHaveTextContent('2');
  });

  it('marks unread replies as read', async () => {
    const state = {
      mentions: {
        mentions: [
          {
            mentionId: 123,
            threadId: 456,
            postId: 789,
            threadTitle: 'Thread',
            mentionedBy: { username: 'Bob', usergroup: 1, isBanned: false },
            content: '["A mention!"]',
          },
          {
            mentionId: 2,
            threadId: 4,
            postId: 7,
            threadTitle: 'Another Thread',
            mentionedBy: { username: 'Joe', usergroup: 1, isBanned: false },
            content: '["A mention!"]',
          },
        ],
      },
    };
    const { getByTitle, getByText, getAllByTitle, queryByText, queryByTitle } = customRender(
      <RepliesMenu />,
      {
        initialState: state,
      }
    );
    fireEvent.click(getByTitle('Messages'));
    expect(queryByText('Thread.')).not.toBeNull();
    expect(queryByText('Bob')).not.toBeNull();
    expect(queryByText('Another Thread.')).not.toBeNull();
    expect(queryByText('Joe')).not.toBeNull();
    expect(queryByTitle('Messages').querySelector('.link-notification')).toHaveTextContent('2');

    fireEvent.click(getAllByTitle('Dismiss')[0]);
    expect(queryByText('Thread.')).toBeNull();
    expect(queryByText('Bob')).toBeNull();
    await waitFor(() => getByText('1'));
  });

  it('displays the old mention format', () => {
    const state = {
      mentions: {
        mentions: [
          {
            mentionId: 123,
            threadId: 456,
            postId: 789,
            content: '["A mention!"]',
          },
        ],
      },
    };
    const { getByTitle, queryByText, queryByTitle } = customRender(<RepliesMenu />, {
      initialState: state,
    });
    fireEvent.click(getByTitle('Messages'));
    expect(queryByText('A mention!')).not.toBeNull();
    expect(queryByTitle('Messages').querySelector('.link-notification')).toHaveTextContent('1');
  });
});
