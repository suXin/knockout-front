/* eslint-disable no-underscore-dangle */
import React from 'react';
import { waitFor, fireEvent } from '@testing-library/react';
import * as CustomRenderer from '../../custom_renderer';

import * as TagsService from '../../../src/services/tags';
import * as SubforumsService from '../../../src/services/subforums';
import * as Helpers from '../../../src/views/ThreadCreationPage/helpers';
import ThreadCreationPage from '../../../src/views/ThreadCreationPage';

import store from '../../../src/state/configureStore';

describe('ThreadCreationPage view', () => {
  let submitThreadMock;

  beforeEach(() => {
    jest.spyOn(TagsService, 'getTags').mockImplementation(() => {
      return [];
    });

    jest.spyOn(SubforumsService, 'getSubforumWithThreads').mockImplementation(() => {
      return [];
    });

    submitThreadMock = jest.spyOn(Helpers, 'submitThread').mockImplementation(() => {
      return true;
    });
    localStorage.clear();
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  describe('As a logged in regular user', () => {
    const loggedInState = { user: { loggedIn: true, username: 'TestUser', usergroup: 1 } };
    const userLocalStorageDetails = { id: 123, username: 'TestUser', usergroup: 1 };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
      jest.spyOn(store, 'getState').mockReturnValue(loggedInState);
    });

    test('I can fill out the thread form, and then submit a new thread', async () => {
      const {
        findByLabelText,
        findByTitle,
        findByText,
        queryByText,
      } = CustomRenderer.customRender(<ThreadCreationPage />, { initialState: loggedInState });

      const icon = await findByTitle('Drama-1');
      const titleInput = await findByLabelText('Title');
      const contentInput = await findByLabelText('Content');
      const submitButton = await findByText('Submit');
      expect(queryByText('Tags')).toBeNull();
      expect(queryByText('Background URL (optional)')).toBeNull();
      expect(queryByText('Background type')).toBeNull();

      fireEvent.click(icon);
      fireEvent.change(titleInput, { target: { value: 'Thread title' } });
      fireEvent.change(contentInput, { target: { value: 'Thread content!' } });
      fireEvent.click(submitButton);

      await waitFor(() => expect(submitThreadMock).toBeCalled());
    });

    test('I can see form errors for inputs which require filling out', async () => {
      const { findAllByText, findByText } = CustomRenderer.customRender(<ThreadCreationPage />, {
        initialState: loggedInState,
      });

      const submitButton = await findByText('Submit');
      fireEvent.click(submitButton);
      await waitFor(() => expect(submitThreadMock).not.toBeCalled());
      const errors = await findAllByText('Must have a value');
      expect(errors).toHaveLength(3);
    });

    describe('and the thread is able to have tags', () => {
      beforeEach(() => {
        jest.spyOn(TagsService, 'getTags').mockImplementation(() => {
          return [
            { id: 1, name: 'A_TAG' },
            { id: 2, name: 'OTHER' },
          ];
        });
      });

      test('I can choose multiple tags to add to the thread', async () => {
        const {
          findByLabelText,
          findByTitle,
          findByText,
          queryByText,
        } = CustomRenderer.customRender(<ThreadCreationPage />, { initialState: loggedInState });

        const icon = await findByTitle('Drama-1');
        const titleInput = await findByLabelText('Title');
        const contentInput = await findByLabelText('Content');
        const submitButton = await findByText('Submit');
        const tagToSelect = await findByText('A_TAG');
        expect(queryByText('Tags')).toBeDefined();

        fireEvent.click(icon);
        fireEvent.change(titleInput, { target: { value: 'Thread title' } });
        fireEvent.change(contentInput, { target: { value: 'Thread content!' } });
        fireEvent.click(tagToSelect);
        fireEvent.click(submitButton);

        await waitFor(() => expect(submitThreadMock).toBeCalled());
      });
    });
  });

  describe('As a logged in gold member', () => {
    const loggedInState = { user: { loggedIn: true, username: 'TestUser', usergroup: 2 } };
    const userLocalStorageDetails = { id: 123, username: 'TestUser', usergroup: 2 };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
      jest.spyOn(store, 'getState').mockReturnValue(loggedInState);
    });

    test('I can fill out the thread form, including background image, and then submit a new thread', async () => {
      const {
        findByLabelText,
        findByTitle,
        findByText,
        queryByText,
      } = CustomRenderer.customRender(<ThreadCreationPage />, { initialState: loggedInState });

      const icon = await findByTitle('Drama-1');
      const titleInput = await findByLabelText('Title');
      const contentInput = await findByLabelText('Content');
      const submitButton = await findByText('Submit');
      const threadBackgroundInput = await findByLabelText('Background URL (optional)');
      expect(queryByText('Background type')).toBeDefined();
      expect(queryByText('Tags')).toBeNull();

      fireEvent.click(icon);
      fireEvent.change(titleInput, { target: { value: 'Thread title' } });
      fireEvent.change(contentInput, { target: { value: 'Thread content!' } });
      fireEvent.change(threadBackgroundInput, {
        target: { value: 'https://bbc.co.uk/example.png' },
      });
      fireEvent.click(submitButton);

      await waitFor(() =>
        expect(submitThreadMock).toBeCalledWith(
          'Thread title',
          'Thread content!',
          1,
          'https://bbc.co.uk/example.png',
          'cover',
          [],
          NaN,
          expect.anything()
        )
      );
    });

    test('I can see form errors for the background image if it is not an image', async () => {
      const {
        findByLabelText,
        findAllByText,
        findByText,
      } = CustomRenderer.customRender(<ThreadCreationPage />, { initialState: loggedInState });

      const submitButton = await findByText('Submit');
      const threadBackgroundInput = await findByLabelText('Background URL (optional)');

      fireEvent.change(threadBackgroundInput, {
        target: { value: 'https://bbc.co.uk/NOTANIMAGE' },
      });
      fireEvent.click(submitButton);

      await waitFor(() => expect(submitThreadMock).not.toBeCalled());

      const errors = await findAllByText('Must have a value');
      expect(errors).toHaveLength(3);
      const imageError = await findByText('Must be an image');
      expect(imageError).toBeDefined();
    });
  });

  describe('As a logged in moderator', () => {
    const loggedInState = { user: { loggedIn: true, username: 'TestUser', usergroup: 3 } };
    const userLocalStorageDetails = { id: 123, username: 'TestUser', usergroup: 3 };

    beforeEach(() => {
      localStorage.__STORE__.currentUser = JSON.stringify(userLocalStorageDetails);
      jest.spyOn(store, 'getState').mockReturnValue(loggedInState);
    });

    test('I can fill out the thread form, including background image, and then submit a new thread', async () => {
      const {
        findByLabelText,
        findByTitle,
        findByText,
        queryByText,
      } = CustomRenderer.customRender(<ThreadCreationPage />, { initialState: loggedInState });

      const icon = await findByTitle('Drama-1');
      const titleInput = await findByLabelText('Title');
      const contentInput = await findByLabelText('Content');
      const submitButton = await findByText('Submit');
      const threadBackgroundInput = await findByLabelText('Background URL (optional)');
      expect(queryByText('Background type')).toBeDefined();
      expect(queryByText('Tags')).toBeNull();

      fireEvent.click(icon);
      fireEvent.change(titleInput, { target: { value: 'Thread title' } });
      fireEvent.change(contentInput, { target: { value: 'Thread content!' } });
      fireEvent.change(threadBackgroundInput, {
        target: { value: 'https://bbc.co.uk/example.png' },
      });
      fireEvent.click(submitButton);

      await waitFor(() =>
        expect(submitThreadMock).toBeCalledWith(
          'Thread title',
          'Thread content!',
          1,
          'https://bbc.co.uk/example.png',
          'cover',
          [],
          NaN,
          expect.anything()
        )
      );
    });

    test('I can see form errors for the inputs and background image', async () => {
      const {
        findByLabelText,
        findAllByText,
        findByText,
      } = CustomRenderer.customRender(<ThreadCreationPage />, { initialState: loggedInState });

      const submitButton = await findByText('Submit');
      const threadBackgroundInput = await findByLabelText('Background URL (optional)');

      fireEvent.change(threadBackgroundInput, {
        target: { value: 'https://bbc.co.uk/NOTANIMAGE' },
      });
      fireEvent.click(submitButton);

      await waitFor(() => expect(submitThreadMock).not.toBeCalled());

      const errors = await findAllByText('Must have a value');
      expect(errors).toHaveLength(3);
      const imageError = await findByText('Must be an image');
      expect(imageError).toBeDefined();
    });
  });
});
