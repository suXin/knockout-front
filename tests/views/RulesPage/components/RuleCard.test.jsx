import React from 'react';
import { customRender } from '../../../custom_renderer';
import RuleCard from '../../../../src/componentsNew/Rules/components/RuleCard';
import '@testing-library/jest-dom/extend-expect';

describe('Rule Card components', () => {
  const data = {
    title: "Don't Be Scum",
    desc: "It's Important to not be scum",
    id: 'id',
  };

  it("Shows the rule's title and description", () => {
    const { queryByText } = customRender(
      <RuleCard title={data.title} identifier={data.id}>
        {data.desc}
      </RuleCard>
    );
    expect(queryByText(data.title)).toBeInTheDocument();
    expect(queryByText(data.desc)).toBeInTheDocument();
  });
});
