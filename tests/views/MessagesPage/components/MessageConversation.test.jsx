import axios from 'axios';
import React from 'react';
import dayjs from 'dayjs';
import { customRender } from '../../../custom_renderer';
import MessageConversation from '../../../../src/views/MessagesPage/components/MessageConversation';

jest.mock('axios');
describe('MessageConversation component', () => {
  const getData = jest.fn();
  const setCurrentConversation = jest.fn();

  const conversation = {
    id: 1,
    messages: [
      {
        id: 12,
        conversationId: 1,
        user: {
          id: 3,
          username: 'Joe',
          usergroup: 1,
          avatarUrl: '',
        },
        content: 'stuff',
        readAt: dayjs().subtract(1, 'minute').toISOString(),
      },
    ],
    users: [
      {
        id: 4,
        username: 'Rick',
        usergroup: 3,
        avatarUrl: '',
      },
      {
        id: 3,
        username: 'Joe',
        usergroup: 1,
        avatarUrl: '',
      },
    ],
    updatedAt: dayjs().subtract(3, 'hour').toISOString(),
  };

  const messages = [
    {
      id: 12,
      conversationId: 1,
      user: {
        id: 3,
        username: 'Joe',
        usergroup: 1,
        avatarUrl: '',
      },
      content: 'stuff',
      createdAt: dayjs().subtract(1, 'minute').toISOString(),
    },
    {
      id: 11,
      conversationId: 1,
      user: {
        id: 3,
        username: 'Joe',
        usergroup: 1,
        avatarUrl: '',
      },
      content: 'yes',
      createdAt: dayjs().subtract(5, 'minute').toISOString(),
    },
    {
      id: 10,
      conversationId: 1,
      user: {
        id: 2,
        username: 'Rick',
        usergroup: 3,
        avatarUrl: '',
      },
      content: 'yoooo',
      createdAt: dayjs().subtract(7, 'minute').toISOString(),
    },
    {
      id: 9,
      conversationId: 1,
      user: {
        id: 2,
        username: 'Rick',
        usergroup: 3,
        avatarUrl: '',
      },
      content: 'testing',
      createdAt: dayjs().subtract(20, 'minute').toISOString(),
    },
    {
      id: 8,
      conversationId: 1,
      user: {
        id: 3,
        username: 'Joe',
        usergroup: 1,
        avatarUrl: '',
      },
      content: 'hey',
      createdAt: dayjs().subtract(25, 'minute').toISOString(),
    },
  ];

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('displays the conversation', async () => {
    axios.get.mockResolvedValue({ data: { messages } });
    const component = (
      <MessageConversation
        getData={getData}
        setCurrentConversation={setCurrentConversation}
        currentConversation={conversation}
        setModalOpen={jest.fn()}
      />
    );

    const { findByText } = customRender(component, { initialState: { user: { id: 3 } } });
    await findByText('testing');
  });

  it('displays an empty conversation', async () => {
    const { queryByText } = customRender(
      <MessageConversation
        getData={getData}
        setCurrentConversation={setCurrentConversation}
        currentConversation={undefined}
        setModalOpen={jest.fn()}
      />,
      { initialState: { user: { id: 3 } } }
    );
    expect(queryByText('No message selected')).not.toBeNull();
  });

  it('displays the back button', async () => {
    axios.get.mockResolvedValue({ data: { messages } });
    const { queryByTitle, findByText } = customRender(
      <MessageConversation
        getData={getData}
        setCurrentConversation={setCurrentConversation}
        currentConversation={conversation}
        setModalOpen={jest.fn()}
        backButton
      />,
      { initialState: { user: { id: 3 } } }
    );
    await findByText('stuff');
    expect(queryByTitle('Back')).not.toBeNull();
  });

  it('marks messages as read', async () => {
    axios.get.mockResolvedValue({ data: { messages } });
    axios.put.mockResolvedValue({ data: { ...messages[0], readAt: dayjs().toISOString() } });
    const { findByText } = customRender(
      <MessageConversation
        getData={getData}
        setCurrentConversation={setCurrentConversation}
        currentConversation={conversation}
        setModalOpen={jest.fn()}
        backButton
      />,
      { initialState: { user: { id: 4 } } }
    );
    await findByText('stuff');
    expect(getData).toBeCalled();
  });
});
