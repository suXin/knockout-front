const axios = require('axios');
const icons = require('../config/icons');

const { apiHost, appUrl } = require('../appConfig');

const userAgents = [
  'Googlebot',
  'Twitterbot',
  'Discordbot',
  'Telegrambot',
  'Facebook',
  'Valve',
  'Steam',
  'WhatsApp',
  'Slackbot',
  'redditbot',
  'DuckDuckBot',
  'KnockoutBot',
  'https://github.com/sindresorhus/got',
];

const userAgentCheck = (req) =>
  userAgents.some((substring) =>
    req.headers['user-agent'].toLowerCase().includes(substring.toLowerCase())
  );

const threadUrlCheck = (req) =>
  req.originalUrl && /\/thread\/([0-9]+)\/?([0-9]*)/gm.exec(req.originalUrl);

const userProfileUrlCheck = (req) => req.originalUrl && /\/user\/([0-9]+)/gm.exec(req.originalUrl);

const getThreadInformation = async (req) => {
  const threadUrlTest = threadUrlCheck(req);

  if (!threadUrlTest || !threadUrlTest[1]) {
    return null;
  }

  const threadUrl = `${apiHost}/thread/${threadUrlTest[1]}`;

  const thread = await axios.get(threadUrl);
  thread.data.posts = [];

  return thread.data;
};

const getUserInformation = async (req) => {
  const userUrlTest = userProfileUrlCheck(req);

  if (!userUrlTest || !userUrlTest[1]) {
    return null;
  }

  const userUrl = `${apiHost}/user/${userUrlTest[1]}`;

  const user = await axios.get(userUrl);

  return user.data;
};

const metaTagBuilder = async (req) => {
  if (
    !req.headers['user-agent'] ||
    !userAgentCheck(req) ||
    (!threadUrlCheck(req) && !userProfileUrlCheck(req))
  ) {
    return null;
  }

  const threadInfo = await getThreadInformation(req);
  const userInfo = await getUserInformation(req);

  const hasThreadInfo = threadInfo && threadInfo.title && !threadInfo.deleted;
  const hasUserInfo = userInfo && userInfo.username && userInfo.id !== 8;

  if (!hasThreadInfo && !hasUserInfo) {
    return null;
  }

  if (hasThreadInfo) {
    const threadIcon = icons.find((el) => el.id === threadInfo.iconId).url;
    const threadTitle = `${threadInfo.title} - Knockout Forums`;
    const threadSubforum = threadInfo.subforum.name;
    const threadDate = threadInfo.updatedAt;
    const threadCreationDate = threadInfo.createdAt;
    const threadUser = threadInfo.user.username;

    const metaTags = `
      <meta name="theme-color" content="#ec3737">
      <meta property="og:title" content="${threadTitle.replace(/"/g, '&quot;')}" />
      <meta property="og:description" content="A thread by ${threadUser} in ${threadSubforum}" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="${appUrl}${req.originalUrl}" />
      <meta property="og:image" content="${appUrl}/${threadIcon}" />
      <meta property="og:article:author" content="${threadUser}" />
      <meta name="date" content="${threadDate}">
      <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "Article",
        "mainEntityOfPage": {
          "@type": "WebPage",
          "@id": "${appUrl}${req.originalUrl}"
        },
        "headline": "${threadTitle}",
        "image": "${appUrl}/${threadIcon}",
        "datePublished": "${threadCreationDate}",
        "dateModified": "${threadDate}",
        "author": {
          "@type": "Person",
          "name": "${threadUser}"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Knockout Forums",
          "logo": {
            "@type": "ImageObject",
            "url": "https://i.imgur.com/oafvbr2.png"
          }
        }
      }
      </script>
  `;

    return metaTags;
  }

  if (hasUserInfo) {
    const userAvatar = `https://cdn.knockout.chat/image/${userInfo.avatarUrl}`;
    const userName = userInfo.username;
    const userPosts = userInfo.posts;
    const userThreads = userInfo.threads;
    const userCreationDate = new Date(userInfo.createdAt);
    const userBanned = userInfo.isBanned;
    const userBanMessage = userBanned
      ? ' This user is currently banned for doing something dumb. Shoulda read the rules.'
      : '';
    const userGoldMessage = userInfo.usergroup === 2 ? ' This user is also a gold member! ' : '';

    const joinYear = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(userCreationDate);
    const joinMonth = new Intl.DateTimeFormat('en', { month: 'short' }).format(userCreationDate);

    const metaTags = `
      <meta name="theme-color" content="#ec3737">
      <meta property="og:title" content="${userName}" />
      <meta property="og:description" content="${userName}'s profile on Knockout. They have made ${userPosts} posts and ${userThreads} threads. Huh. They've also been around since ${joinMonth} ${joinYear}.${userGoldMessage}${userBanMessage}" />
      <meta property="og:type" content="article" />
      <meta property="og:url" content="${appUrl}${req.originalUrl}" />
      <meta property="og:image" content="${userAvatar}" />
      <meta name="twitter:card" content="summary_large_image">
      <meta property="og:article:author" content="${userName}" />
      <meta name="date" content="${new Date()}">
      <script type="application/ld+json">
      {
        "@context": "https://schema.org",
        "@type": "Article",
        "mainEntityOfPage": {
          "@type": "WebPage",
          "@id": "${appUrl}${req.originalUrl}"
        },
        "headline": "${userName}",
        "image": "${userAvatar}",
        "datePublished": "${userCreationDate}",
        "dateModified": "${new Date()}",
        "author": {
          "@type": "Person",
          "name": "${userName}"
        },
        "publisher": {
          "@type": "Organization",
          "name": "Knockout Forums",
          "logo": {
            "@type": "ImageObject",
            "url": "https://i.imgur.com/oafvbr2.png"
          }
        }
      }
      </script>
  `;

    return metaTags;
  }

  return null;
};

module.exports = metaTagBuilder;
